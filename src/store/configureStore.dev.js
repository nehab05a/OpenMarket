import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from '../reducers';
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'

const history = createHistory()

const routerware = routerMiddleware(history)

const enhancer = composeWithDevTools(
  applyMiddleware(thunk
    ,routerware
    ,logger()
  )
);

export default function configureStore (initialState={}) {

  return createStore(
    rootReducer,
    undefined,
    enhancer
  );
}
