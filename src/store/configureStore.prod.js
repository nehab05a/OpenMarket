import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'
import rootReducer from '../reducers';

const history = createHistory()
const routerware = routerMiddleware(history)

const enhancer = applyMiddleware(thunk, routerware);

export default function configureStore (initialState) {
  return createStore(
    rootReducer,
    initialState,
    enhancer
  );
}
