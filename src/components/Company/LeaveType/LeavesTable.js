import React from 'react'
import PropTypes from 'prop-types'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import _ from 'lodash'

class LeavesTable extends React.PureComponent {
  render () {
    const {data, onEdit, onDelete} = this.props,
    _styles = {
      icons: {
        fontSize : '12px',
      },
      tableHeader: {
        textAlign: 'left',
        color: '#7F8FA4',
        fontWeight: '600',
        padding: '15px'
      },
      tableRow: {
        alignItems: 'center',
      },
      tableCell: {
        padding: '8px 15px'
      }
    },
    columns = [{
      Header: 'Code',
      accessor: 'shortCode',
      Cell: (props) => {
        const {shortCode, colorCode} = props.original;
        return (
          <div className='d-flex align-items-center justify-content-center'
            style={{
              fontSize:'18px',
              width:'35px',
              height:'35px',
              borderRadius:'50%',
              background: colorCode,
              color:'white'}}>
            {shortCode}
          </div>
        )},
    }, {
      Header: 'Name',
      accessor: 'type',
    }, {
      Cell: (props) => {
        const {original} = props;
        return (
          <button className='btn btn-primary' style={{width: '80px'}} type='button' onClick={() => onEdit(original)}>
            Edit
          </button>
        )},
    }, {
      Cell: (props) => {
        const {original} = props;
        return (
          !original.isUsed && <button className='btn btn-secondary' aria-hidden='true' style={{width: '110px'}} type='button' onClick={() => onDelete(original.id)}>
            Delete &nbsp;
            <i className='icon-trash' style={{color: 'crimson', fontSize: '15px'}}/>
          </button>
        )},
    } ];

    return (
      <div>
        <div className='table-wrap'>
          <ReactTable
            className='-striped -highlight'
            data={data}
            columns={columns}
            defaultPageSize={10}
            getTheadThProps={() => {return {style: _styles.tableHeader}}}
            getTdProps={() => {return {style: _styles.tableCell}}}
            getTrProps={() => {return {style: _styles.tableRow}}}
          />
        </div>
      </div>
    )
  }
}

LeavesTable.propTypes = {
  data: PropTypes.array,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
}

export default LeavesTable
