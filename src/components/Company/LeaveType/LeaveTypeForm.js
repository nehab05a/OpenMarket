import React from 'react'
import PropTypes from 'prop-types'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../Commons/FormGroup'
import ColorPicker from '../../Commons/ColorPicker'
import SwitchFormRight from '../../Commons/SwitchFormRight'
import {required} from '../../Commons/Validators'
import {isEmpty} from 'lodash'

const LeavesTypesForm =({
  handleSubmit,
  submitting,
  invalid,
  color,
  onColorSelect,
  getShortCode,
  shortCode,
  initialValues,
  onCancel,
}) => {
    return (
        <form onSubmit={handleSubmit} className='form-pb'>
          <h5 className='mb-2'>Enter Details for Leave Types</h5>
          <div className='row'>
            <Field className='col-md-6' name='type' type='text' component={FormGroup} label='Leave Name' validate={[required]}/>
            <Field className='col-md-6' name='shortCode' type='text' onAnswered={getShortCode} component={FormGroup} label='Short Code' />
        </div>
          <div className='row form-group' style={{padding:'0px 15px'}}>
            <label className='form-control-label'>Description</label>
            <Field className='col-md-12 form-control' name='description' type='textarea' rows='6' placeholder='Description' component='textarea' label='Requirements'/>
          </div>
          <div className='row'>
            <Field className='col-md-6' name='colorCode' type='text' component={ColorPicker} onAnswered={onColorSelect} label='Code' />
            <Field className='col-md-6' name='nonDeductible' checked={false} component={SwitchFormRight} message='Non-Deductible' />
          </div>
          <div className='row form-group' style={{padding:'0px 15px'}}>
            <label className='form-control-label'>Preview</label>
            <div className='col-md-12  d-flex justify-content-center'>
              <div className='d-flex align-items-center justify-content-center'
                style={{
                  fontSize:'40px',
                  width:'90px',
                  height:'90px',
                  borderRadius:'50%',
                  background: color ? `rgba(${color.r }, ${ color.g }, ${ color.b }, ${ color.a })` : initialValues && initialValues.colorCode,
                  color:'white'}}>
                {shortCode || (initialValues && initialValues.shortCode)}
              </div>
            </div>
          </div>
          <div  className='row d-flex justify-content-center'>
            <button className='col-md-3 btn btn-secondary' style={{margin: '10px'}} type='button' onClick={onCancel}>
              Cancel
            </button>
            <button className='col-md-3 btn btn-primary' style={{margin: '10px'}} type='submit' disabled={submitting || invalid}>
              {isEmpty(initialValues) ?  `Save` : `Update`}
            </button>
          </div>
        </form>
    )
}

LeavesTypesForm.propTypes ={
  color: PropTypes.object,
  onColorSelect: PropTypes.func,
  getShortCode: PropTypes.func,
  shortCode: PropTypes.string,
  initialValues: PropTypes.object,
  onCancel: PropTypes.func,
}

export default reduxForm({
  form: 'leaveTypeForm',
  enableReinitialize: true
})(LeavesTypesForm)
