import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import LeaveTypesActions from './LeavesActions'
import LeaveTypesTable from './LeavesTable'
import LeaveTypeForm from './LeaveTypeForm'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
import * as actionsLeaveTypes from '../../../actions/LeaveTypes'
import {createFilter} from 'react-search-input'
import {filter} from 'lodash'
import {change} from 'redux-form'
import autoBind from 'react-autobind'
import {withRouter} from 'react-router-dom'
import {isEmpty, isEqual} from 'lodash'

class LeaveTypes extends Component {
  constructor(props){
    super(props)
    this.state = {
      isOpen: false,
      searchTerm : '',
      leaveTypes: this.props.leaveTypes,
      selectedColor : {},
      initialValues : {},
      shortCode : '',
    }
    autoBind(this)
  }

  componentWillMount() {
    const {fetchLeaveTypes} = this.props.actions;
    fetchLeaveTypes()
  }

  componentWillReceiveProps(nextProps){
    const { leaveTypes } = this.props
    !isEqual(nextProps.leaveTypes, leaveTypes) && this.setState({leaveTypes: nextProps.leaveTypes})
  }

  _onSubmit(values) {
    const {updateLeaveTypes}= this.props.actions;
    updateLeaveTypes(values, this._onSubmitSuccess)
  }

  _onEditRecord(initialValues) {
    this.setState({initialValues},
      this._toggleModal
    )
  }

  _onDeleteRecord(id) {
    const { actions: {deleteLeaveType} } = this.props;
    deleteLeaveType(id, this._onSubmitSuccess)
  }

  _onSubmitSuccess(){
    const {fetchLeaveTypes} = this.props.actions;
    this.setState({isOpen: false},
      () =>   fetchLeaveTypes()
    )
  }

  render() {
    const
    { history } = this.props,
    _styles = {
      button:{
        margin: '20px 10px'
      }
    },
    { isOpen, selectedColor, leaveTypes, shortCode, initialValues } = this.state,
    filteredItems = filter(leaveTypes, createFilter(this.state.searchTerm, ['type']));

    return (
      <div className='container-fluid mt-2'>
        <div className='animated fadeIn'>
          <div className='card card-default'>
            <LeaveTypesActions toggleModal={this._toggleModal} searchUpdated={this._searchUpdated}/>
            <LeaveTypesTable data={filteredItems} onEdit={this._onEditRecord} onDelete={this._onDeleteRecord}/>
            <div className='row mt-2'>
                <div className='col-sm-12'>
                  <div className='float-right'>
                    <button className='btn btn-secondary' type='button' style={_styles.button}
                      onClick={() => history.push('/company/expense')}>
                        <i className='fa fa-arrow-left' aria-hidden='true'></i>
                        &nbsp; Back
                    </button>
                    {' '}
                    <button className='btn btn-primary' type='button' style={_styles.button}
                      onClick={() => history.push('/company/locations')}>
                        Next &nbsp;
                        <i className='fa fa-arrow-right' aria-hidden='true'></i>
                    </button>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <Modal isOpen={isOpen}>
         <ModalHeader toggle={this._toggleModal}>{isEmpty(initialValues) ? `Add` : `Update`} Leaves Type</ModalHeader>
         <ModalBody>
           <LeaveTypeForm
            initialValues={initialValues}
            color={selectedColor.rgb}
            onColorSelect={this._onColorSelection}
            getShortCode={this._getShortCode}
            shortCode={shortCode}
            onSubmit={this._onSubmit}
            onCancel={this._toggleModal}
           />
         </ModalBody>
        </Modal>
      </div>
    );
  }

  _toggleModal(clear) {
    if(clear) this.setState({
      initialValues: undefined,
      shortCode: '',
      selectedColor: {}
    })
    this.setState({isOpen: !this.state.isOpen})
  }

  _searchUpdated (term) {
    this.setState({searchTerm: term})
  }

  _onColorSelection(selectedColor) {
    this.setState({selectedColor}, () => this.props.actions.changeFieldValue('colorCode', selectedColor.hex))
  }

  _getShortCode(shortCode) {
    this.setState({shortCode})
  }
}

LeaveTypes.propTypes = {
  leaveTypes: PropTypes.array,
  actions: PropTypes.object,
  history: PropTypes.object
}

const
  mapStateToProps = (state) => ({
      leaveTypes: state.companySettings.leaveTypes
    }
  ),

  mapDispatchToProps = (dispatch) => {
    const changeFieldValue = (field, value) => change('leaveTypeForm', field, value)
    return {
      actions : bindActionCreators({
        ...actionsLeaveTypes,
        changeFieldValue
      }, dispatch),
    }
  }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LeaveTypes));
