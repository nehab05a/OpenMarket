import React from 'react';
import {Field, reduxForm} from 'redux-form'
import TeamSize from '../../../config/data/TeamSize'
import {Currencies,CurrenciesSymbols} from '../../../config/data/Currencies'
import TimeZones from '../../../config/data/TimeZones'
import NumberFormats from '../../../config/data/NumberFormats'
import {Months,Days,FormatDates} from '../../../config/data/Date'
import Languages from '../../../config/data/Languages'
import SelectRedux from '../../Commons/SelectRedux'
import SwitchFormRight from '../../Commons/SwitchFormRight'
import {required} from '../../Commons/Validators'
import Logo from './Logo'

const SettingsForm = (props) => {
    const {handleSubmit, submitting, invalid, initialValues, onBack, uploadLogo } = props
    return (
        <form className='form-pb' onSubmit={handleSubmit}>
            <div className='row'>
                <div className='offset-sm-2 col-lg-8'>

                    <h5 className='mb-2'>General Settings</h5>
                    <div className='row'>
                        <Field className='col-md-6' name='fiscalYear' type='text' options={Months} component={SelectRedux} label='Fiscal Year Starts' />
                        <Field className='col-md-6' name='holidayYear' type='text' options={Months} component={SelectRedux} label='Holiday Year Starts' />
                    </div>
                    <div className='row'>
                        <Field className='col-md-6' name='defaultDateFormat' type='text' options={FormatDates}
                          component={SelectRedux} label='Default Date Format' validate={required} required/>
                        <Field className='col-md-6' name='defaultLanguage' type='text' options={Languages} component={SelectRedux} label='Default Language'/>
                    </div>
                    <div className='row'>
                        <Field className='col-md-6' name='timeZone' type='text' options={TimeZones} component={SelectRedux} label='Default Time Zone' />
                        <Field className='col-md-6' name='defaultNumberFormat' options={NumberFormats}
                          type='text' component={SelectRedux} label='Default Number Format' validate={required} required/>
                    </div>
                    <div className='row'>
                        <Field className='col-md-6' name='startWeekOn' type='text' options={Days} component={SelectRedux} label='Start Week On' />
                        <Field className='col-md-6' name='currency' type='text' options={Currencies} component={SelectRedux}
                          label='Currency' validate={required} required/>
                    </div>
                    <div className='row'>
                        <Field className='col-md-6' name='teamSize' options={TeamSize} label='Team Size' component={SelectRedux} />
                        <Field className='col-md-6' name='symbol' type='text' options={CurrenciesSymbols} component={SelectRedux}
                          label='Currency Symbol' validate={required} required/>
                    </div>
                    <div className='row'>
                      <Field component={SwitchFormRight} label='Currencies Code Suffix' name='currencyCodeSuffix' message='Include currency code when account has multiple currencies'/>
                      <Field component={SwitchFormRight} label='Currency Symbol' name='currencySymbolPlacement' message='Place before the amount'/>
                    </div>
                    <div className='row'>
                      <Field component={SwitchFormRight} label='Include Non-working Days As Leaves' name='isWeekEndCount' message='Yes' />
                    </div>

                    <Logo logo={initialValues.companyLogo} uploadLogo={uploadLogo}/>

                    <div className='row mt-2'>
                        <div className='col-sm-12'>
                          <div className='float-right'>
                            <button className='btn btn-secondary' type='button' onClick={onBack} style={{margin: '20px 10px'}}>
                                <i className='fa fa-arrow-left' aria-hidden='true'></i>
                                &nbsp; Back
                            </button>
                            {' '}
                            <button className='btn btn-primary' type='submit' style={{margin: '20px 10px'}} disabled={submitting || invalid}>
                                Next &nbsp;
                                <i className='fa fa-arrow-right' aria-hidden='true'></i>
                            </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    )
}
export default reduxForm({
    form: 'settingsForm', // a unique identifier for this form
    enableReinitialize: true
})(SettingsForm)
