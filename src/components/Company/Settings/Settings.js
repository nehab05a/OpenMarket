import React, { Component } from 'react'
import SettingsForm from './SettingsForm'
import * as actions from '../../../actions/AccountDetails'
import autobind from 'react-autobind'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {isEqual} from 'lodash'

class Settings extends Component{
  constructor(props) {
    super(props)
    this.state = {
      accountDetails: this.props.accountDetails
    }
    autobind(this)
  }

  componentWillMount(){
    const { fetchAccountDetails } = this.props;
    fetchAccountDetails()
  }

  componentWillReceiveProps(nextProps){
    const {accountDetails} = this.props
    !isEqual(nextProps.accountDetails, accountDetails) && this.setState({accountDetails: nextProps.accountDetails})
  }

  onNext(values) {
    const
      { updateAppSettings, logo } = this.props,
      payload = {
        fiscalYear : values.fiscalYear,
        holidayYear : values.holidayYear,
        defaultDateFormat : values.defaultDateFormat,
        defaultLanguage : values.defaultLanguage,
        timeZone : values.timeZone,
        startWeekOn : values.startWeekOn,
        currency : values.currency,
        teamSize : values.teamSize,
        symbol : values.symbol,
        currencyCodeSuffix : values.currencyCodeSuffix || false,
        currencySymbolPlacement : values.currencySymbolPlacement || false,
        isWeekEndCount : values.isWeekEndCount || false,
        defaultNumberFormat : values.defaultNumberFormat,
        companyLogo: (logo && logo[0].preview) || null
      }
    updateAppSettings(payload, this.onSubmitSuccess, this.onSubmitFailure)
  }

  onBack(values) {
    const { history } = this.props;
    history.push('/company/details')
  }

  onSubmitSuccess() {
    const { history } = this.props;
    history.push('/company/staff-types')
  }

  onSubmitFailure(errorMessage) {

  }

  uploadLogo(file) {
    this.setState({file})
  }

  render(){
    const { accountDetails } = this.state;
    return(
      <div className='container-fluid mt-2'>
        <div className='animated fadeIn'>
            <div className='card card-default'>
                <div className='card-header main-header'>
                    Settings
                </div>

                <div className='card-block'>
                    <SettingsForm onSubmit={this.onNext} onBack={this.onBack} initialValues={accountDetails} uploadLogo={this.uploadLogo}/>
                </div>
            </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    accountDetails: state.companySettings.accountDetails,
    logo: state.files.logo,
  }
};

export default withRouter(connect(mapStateToProps, actions)(Settings))
