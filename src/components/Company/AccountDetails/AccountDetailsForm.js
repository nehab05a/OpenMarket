import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../Commons/FormGroup'
import {required, isNumber} from '../../Commons/Validators'
import SelectRedux from '../../Commons/SelectRedux'
import Countries from '../../../config/data/Countries'

class AccountDetailsForm extends Component {
    render() {
        const {toggleModal, handleSubmit, submitting, invalid, onBack, setValue, handleFindAddress} = this.props
        return (
            <form className='form-pb' onSubmit={handleSubmit}>
                <div className='row'>
                    <div className='offset-sm-2 col-lg-8'>

                        <h5 className='mb-2'>Account Holder Details</h5>
                        <div className='row'>
                            <Field className='col-md-6' name='firstName' type='text' component={FormGroup} label='First Name' validate={required}/>
                            <Field className='col-md-6' name='lastName' type='text' component={FormGroup} label='Last Name'/>
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='contact' type='text' component={FormGroup} label='Contact Number' validate={isNumber}/>
                            <Field className='col-md-6' name='email' type='email' component={FormGroup} disabled={true} label='Email Address' validate={required}/>
                        </div>
                        <h5 className='mb-2'>Company Details</h5>
                        <div className='row'>
                            <Field className='col-md-6' name='companyName' type='text' component={FormGroup} label='Company Name' validate={required}/>
                            <Field className='col-md-6' name='registeredName' type='text' component={FormGroup} label='Registered Name'/>
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='webAddress' type='text' component={FormGroup} label='Web address'/>
                            <Field className='col-md-6' name='telephone' type='text' component={FormGroup} label='Phone Number'/>
                        </div>
                        <h5 className='mb-2'>Company Address</h5>
                        <div className='row'>
                          <Field
                            className='col-md-6'
                            name='postalCode'
                            type='text'
                            component={FormGroup}
                            label='Post Code/Zip Code'
                            onBlur={(e) => setValue(e.target.value)}
                          />
                          <div className='col-md-6 mt-2'>
                            <button className='btn btn-primary btn-block' type='button' onClick={handleFindAddress}>
                            Find Address
                           </button>
                        </div>
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='addressLine1' type='text' component={FormGroup} label='1st Line of the Address'/>
                            <Field className='col-md-6' name='addressLine2' type='text' component={FormGroup} label='2st Line of the Address'/>
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='cityOrTown' type='text' component={FormGroup} label='City/Town'/>
                            <Field className='col-md-6' name='countyOrState' type='text' component={FormGroup} label='County/State'/>
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='postalCode' type='text' component={FormGroup} label='Post Code/Zip Code'/>
                            <Field className='col-md-6' name='country' type='text' options={Countries} component={SelectRedux} label='Country'/>
                        </div>
                        <div className='row'>
                            <div className='col-sm-6'>
                                <button className='btn btn-outline-primary btn-block' type='button' onClick={toggleModal}>
                                    <i className='fa fa-key'></i>
                                    Change Password
                                </button>
                            </div>
                        </div>

                        <div className='row mt-2'>
                            <div className='col-sm-12'>
                              <div className='float-right'>
                                <button className='btn btn-secondary' type='button' onClick={onBack} style={{margin: '20px 10px'}}>
                                    <i className='fa fa-arrow-left' aria-hidden='true'></i>
                                    &nbsp; Back
                                </button>
                                {' '}
                                <button className='btn btn-primary' type='submit' disabled={submitting || invalid} style={{margin: '20px 10px'}}>
                                    Next &nbsp;
                                    <i className='fa fa-arrow-right' aria-hidden='true'></i>
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

AccountDetailsForm.propTypes = {
  toggleModal: PropTypes.func,
  onBack: PropTypes.func,
}

export default reduxForm({form: 'accountDetailsForm',enableReinitialize: true})(AccountDetailsForm)
