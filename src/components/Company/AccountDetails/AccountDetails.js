import React, {Component} from 'react'
import PropTypes from 'prop-types'
import AccountDetailsForm from './AccountDetailsForm'
import * as actions from '../../../actions/AccountDetails'
import * as actionsLocations from '../../../actions/Locations'
import ModalChangePwdForm from '../../Commons/ModalChangePwdForm'
import { changePassword } from '../../../actions'
import {Modal, ModalHeader} from 'reactstrap'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import autobind from 'react-autobind'
import {change} from 'redux-form'
import { bindActionCreators } from 'redux'
import _ from 'lodash'

class AccountDetails extends Component {
    constructor(props) {
      super(props)
      this.state = {
        accountDetails: this.props.accountDetails,
        isOpen: false
      }
      autobind(this)
    }

    componentWillMount(){
      const { fetchAccountDetails } = this.props.actions;
      fetchAccountDetails()
    }

    componentWillReceiveProps(nextProps){
      const { accountDetails } = this.props
      !_.isEqual(nextProps.accountDetails, accountDetails)  && this.setState({accountDetails: nextProps.accountDetails})
    }

    _setPostalCode(postalOrZipCode) {
      this.setState({postalOrZipCode})
    }

    _handleFindAddress() {
      const
        { postalOrZipCode } = this.state,
        { findAddress, changeFieldValue } = this.props.actions;
      findAddress(postalOrZipCode, address => this.setState({
        address
      }, () => Object.keys(address).forEach( key => changeFieldValue(key, address[key])) ))
    }

    onNext(values) {
      const
        payload = {
          firstName: values.firstName,
          lastName: values.lastName,
          contact: values.contact,
          companyName: values.companyName,
          webAddress: values.webAddress,
          telephone: values.telephone,
          addressLine1: values.addressLine1,
          addressLine2: values.addressLine2,
          cityOrTown: values.cityOrTown,
          countyOrState: values.countyOrState,
          postalCode: values.postalCode,
          country: values.country,
          registeredName: values.registeredName
        },
        { updateAccountDetails } = this.props.actions;
      updateAccountDetails(payload, this.onSubmitSuccess, this.onSubmitFailure)
    }

    onBack(values) {
      const { history } = this.props;
      history.push('/company/getstart')
    }

    onSubmitSuccess() {
      const { history } = this.props;
      history.push('/company/settings')
    }

    onSubmitFailure(errorMessage) {

    }

    changePassword(values)  {
      const { oldPassword, newPassword, confirmPassword} = values
      if(newPassword !== confirmPassword) this.setState({errorMessage: 'Password does not match'})
      else this.props.actions.changePassword({oldPassword, newPassword}, this.handleChangePassword, this.handleFailure)
    }

    handleFailure(submitMessage) {
      this.setState({submitMessage})
    }

    handleChangePassword() {
      this.setState({submitMessage: undefined}, () => this.toggleChangePwd())
    }

    toggleChangePwd() {
      this.setState({isOpen: !this.state.isOpen})
    }

    render() {
        const
          {accountDetails, isOpen} = this.state;
        return (
          <div className='container-fluid mt-2'>
            <div className="animated fadeIn">
                <div className="card card-default">
                    <div className="card-header main-header">
                        Account Details
                    </div>

                    <div className="card-block">
                        <AccountDetailsForm onSubmit={this.onNext}
                        toggleModal={this.toggleChangePwd}
                        initialValues={accountDetails}
                        setValue={this._setPostalCode}
                        handleFindAddress={this._handleFindAddress}
                        onBack={this.onBack}/>
                    </div>
                </div>
            </div>
            <Modal isOpen={isOpen} toggle={this.toggleChangePwd}>
              <ModalHeader toggle={this.toggleChangePwd}>Change Password</ModalHeader>
                <ModalChangePwdForm
                  onSubmit={this.changePassword} toggle={this.toggleChangePwd}  {...this.state}/>
            </Modal>
          </div>
        )
    }
}

AccountDetails.propTypes = {
  accountDetails: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    accountDetails: state.companySettings.accountDetails,
  }
},

mapDispatchToProps = (dispatch) => {
  const changeFieldValue = (field, value) => change('accountDetailsForm', field, value)
  return {
    actions : bindActionCreators({
      ...actions,
      ...actionsLocations,
      changePassword,
      changeFieldValue
    }, dispatch)
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AccountDetails))
