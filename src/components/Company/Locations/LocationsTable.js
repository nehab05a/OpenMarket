import React from 'react'
import PropTypes from 'prop-types'
import ReactTable from 'react-table'
import 'react-table/react-table.css'

class LocationsTable extends React.PureComponent {
  render () {
    const { data, onView } = this.props,
    _styles = {
      icons: {
        fontSize : '12px',
      },
      tableHeader: {
        textAlign: 'left',
        color: '#7F8FA4',
        fontWeight: '600',
        padding: '15px'
      },
      tableRow: {
        alignItems: 'center',
      },
      tableCell: {
        padding: '8px 15px'
      }
    },
    columns = [{
      Header: 'Code',
      accessor: 'branch_code',
      style: {
        color: '#7F8FA4'
      },
    }, {
      Header: 'Name',
      accessor: 'branch_name',
    }, {
      Header: 'Location',
      Cell: (props) => {
        const {branch_location, addressLine1, addressLine2, city, country, cityOrState} = props.original;
        return branch_location || `${addressLine1}, ${addressLine2}, ${city}, ${country}, ${cityOrState}`
      },
      style: {
        color: '#7F8FA4'
      },
    }, {
      Cell: (props) => {
        const {original} = props;
        return (
          <button className='btn btn-primary' style={{width: '80px'}} type='button' onClick={() => onView(original)}>
            View
          </button>
        )},
    }];

    return (
      <div>
        <div className='table-wrap'>
          <ReactTable
            className='-striped -highlight'
            data={data}
            columns={columns}
            defaultPageSize={5}
            getTheadThProps={() => {return {style: _styles.tableHeader}}}
            getTdProps={() => {return {style: _styles.tableCell}}}
            getTrProps={() => {return {style: _styles.tableRow}}}
          />
        </div>
      </div>
    )
  }
}

LocationsTable.propTypes = {
  data: PropTypes.array,
  onView: PropTypes.func,
}

export default LocationsTable
