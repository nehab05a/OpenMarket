import React from 'react'
import PropTypes from 'prop-types'
import SearchInput from 'react-search-input'

const LocationActions = (props) => (
  <div className='card-header main-header'>
    Locations
    <div className='card-actions d-flex align-items-center'>
      {/* <div className='input-group'>
        <input type='text' className='form-control' placeholder='Search' />
        <span className='input-group-addon'>
         <i className='fa fa-search'></i>
        </span>
      </div>
       <button className='btn-minimize' type='button'>
           <i className='fa fa-filter'></i>
       </button> */}
       <SearchInput className='list-search' onChange={props.searchUpdated} />
       <span className='partial-border-left' style={{marginLeft: '20px'}} ></span>
       <button className='btn-minimize' onClick={() => props.toggleModal(true)} type='button'>
           <i className='icon-plus'></i>
       </button>
   </div>
  </div>

);

LocationActions.propTypes = {
  toggleModal: PropTypes.func.isRequired
};

export default LocationActions
