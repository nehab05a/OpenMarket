import React, {Component} from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import LocationActions from './LocationActions'
import LocationsTable from './LocationsTable'
import AddLocationForm from './AddLocationForm'
import * as actionsLocations from '../../../actions/Locations'
import * as actionsDepartments from '../../../actions/Departments'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
import autoBind from 'react-autobind'
import {filter, isEmpty, isEqual} from 'lodash'
import {withRouter} from 'react-router-dom'
import {createFilter} from 'react-search-input'
import DetailsView from './DetailsView'
import {change} from 'redux-form'

class Locations extends Component {
  constructor(props){
    super(props)
    this.state = {
      isOpen: false,
      searchTerm : '',
      initialValues: {},
      locations: this.props.locations,
      viewDetails : false,
    }
    autoBind(this)
  }

  componentWillMount() {
      const { fetchLocations, fetchDepartments } = this.props.actions
      fetchLocations()
      fetchDepartments()
  }

  componentWillReceiveProps(nextProps){
    const { locations } = this.props
    !isEqual(nextProps.locations, locations) && this.setState({locations: nextProps.locations})
  }

  _onSubmit(values) {
    const
      { address } = this.state,
      { isShiftLocation } = values,
      { updateLocation } = this.props.actions,
      data = {
        ...values,
        isShiftLocation: isShiftLocation || false,
        ...address
      }
      updateLocation(data, this._onSubmitSuccess)
  }

  _onSubmitSuccess(){
    const { fetchLocations } = this.props.actions;
    this.setState({ isOpen: false },
      () => fetchLocations()
    )
  }

  _setPostalCode(postalOrZipCode) {
    this.setState({postalOrZipCode})
  }

  _handleFindAddress() {
    const
      { postalOrZipCode } = this.state,
      { findAddress, changeFieldValue } = this.props.actions;
    findAddress(postalOrZipCode, address => this.setState({
      address
    }, () => Object.keys(address).forEach( key => changeFieldValue(key, address[key])) ))
  }

  _onView(initialValues) {
    this.setState({initialValues, viewDetails: true},
      this._toggleModal
    )
  }

  _onEdit() {
    this.setState({viewDetails: false})
  }

  render() {
    const { locations, history } = this.props,
    _styles = {
      button:{
        margin: '20px 10px'
      }
    },
    { isOpen, searchTerm, initialValues, viewDetails } = this.state,
    filteredItems = filter(locations, createFilter(searchTerm, ['name']));
    return (
      <div className='container-fluid mt-2'>
        <div className='animated fadeIn'>
          <div className='card card-default'>
            <LocationActions
              addLocation={() => {}}
              toggleModal={this._toggleModal}
              searchUpdated={this._searchUpdated}

            />
            <LocationsTable
              data={filteredItems}
              onView={this._onView}
            />
            <div className='row mt-2'>
                <div className='col-sm-12'>
                  <div className='float-right'>
                    <button className='btn btn-secondary' type='button' style={_styles.button}
                      onClick={() => history.push('/company/leave')}>
                        <i className='fa fa-arrow-left' aria-hidden='true'></i>
                        &nbsp; Back
                    </button>
                    {' '}
                    <button className='btn btn-primary' type='button' style={_styles.button}
                      onClick={() => history.push('/company/departments')}>
                        Next &nbsp;
                        <i className='fa fa-arrow-right' aria-hidden='true'></i>
                    </button>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <Modal isOpen={isOpen}>
         <ModalHeader toggle={this._toggleModal}>{isEmpty(initialValues) ? `Add` : `Update`} Location</ModalHeader>
         {viewDetails ?
          <DetailsView
            data={initialValues || {}}
            onEdit={this._onEdit}
            onCancel={this._toggleModal}
            departments={[]}
          />
         :<ModalBody>
           <AddLocationForm
            initialValues={initialValues}
            onSubmit={this._onSubmit}
            onCancel={this._toggleModal}
            setValue={this._setPostalCode}
            handleFindAddress={this._handleFindAddress}
           />
         </ModalBody>
        }
        </Modal>
      </div>
    );
  }

  _toggleModal(clear) {
    if(clear) this.setState({
      initialValues: undefined,
      address: undefined,
      viewDetails: false
    })
    this.setState({isOpen: !this.state.isOpen})
  }

  _searchUpdated (term) {
    this.setState({searchTerm: term})
  }

}
const  mapStateToProps = (state) => (
  {
    locations: state.companySettings.locations
  }
)

const mapDispatchToProps = (dispatch) => {
  const changeFieldValue = (field, value) => change('addLocationForm', field, value)
  return {
    actions : bindActionCreators({
      ...actionsLocations,
      ...actionsDepartments,
      changeFieldValue
    }, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Locations));
