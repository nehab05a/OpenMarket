import React from 'react'
import PropTypes from 'prop-types'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../Commons/FormGroup'
import {required,emailValid} from '../../Commons/Validators'
import SelectRedux from '../../Commons/SelectRedux'
import SwitchFormRight from '../../Commons/SwitchFormRight'
import Countries from '../../../config/data/Countries'
import LocationPhoto from './LocationsPhoto'

const AddLocationForm =({
  handleSubmit,
  submitting,
  invalid,
  onCancel,
  initialValues,
  handleFindAddress,
  setValue
}) => {
    return (
        <form onSubmit={handleSubmit} className='form-pb'>
          <h5 className='mb-2'>Enter Details for Location</h5>
          <div className='row'>
            <Field className='col-md-6' name='branch_name' type='text' component={FormGroup} label='Location Name'/>
            <Field className='col-md-6' name='branch_code' type='text' component={FormGroup} label='Location Code '/>
          </div>
          <div className='row'>
            <Field className='col-md-6' name='telephone' type='text' component={FormGroup} label='Telephone'/>
            <Field className='col-md-6' name='fax' type='text' component={FormGroup} label='Fax'/>
          </div>
          <div className='row'>
            <Field className='col-md-6' name='email' type='text' component={FormGroup} label='Email Address' validate={[required,emailValid]}/>
            <Field name='isShiftLocation' component={SwitchFormRight}  message='This is a Shift Location'/>
          </div>
          <div className='row'>
            <Field
              className='col-md-6'
              name='postalCode'
              type='text'
              component={FormGroup}
              label='Post Code/Zip Code'
              onBlur={(e) => setValue(e.target.value)}
            />
            <div className='col-md-6 mt-2'>
              <button className='btn btn-primary btn-block' type='button' onClick={handleFindAddress}>
              Find Address
             </button>
          </div>
          </div>
          <div className='row'>
              <Field className='col-md-6' name='addressLine1' type='text' component={FormGroup} label='1st Line of the Address'/>
              <Field className='col-md-6' name='addressLine2' type='text' component={FormGroup} label='2st Line of the Address'/>
          </div>
          <div className='row'>
              <Field className='col-md-6' name='cityOrTown' type='text' component={FormGroup} label='City/Town'/>
              <Field className='col-md-6' name='countyOrState' type='text' component={FormGroup} label='County/State'/>
          </div>
          <div className='row'>
              <Field className='col-md-6' name='postalCode' type='text' component={FormGroup} label='Post Code/Zip Code'/>
              <Field className='col-md-6' name='country' type='text' options={Countries} component={SelectRedux} label='Country'/>
          </div>
          <LocationPhoto />
          <div  className='row d-flex justify-content-center'>
            <button className='col-md-3 btn btn-secondary' style={{margin: '10px'}} type='button' onClick={onCancel}>
              Cancel
            </button>
            <button className='col-md-3 btn btn-primary' style={{margin: '10px'}} type='submit'>
              {initialValues ? `Update` : `Save`}
            </button>
          </div>
        </form>
    )
}

AddLocationForm.propTypes = {
  onCancel: PropTypes.func,
  initialValues: PropTypes.object,
  handleFindAddress: PropTypes.func,
  setValue: PropTypes.func
}

export default reduxForm({form: 'addLocationForm', enableReinitialize: true})(AddLocationForm)
