import React from 'react';
import {Field, reduxForm} from 'redux-form'
import classnames from 'classnames'
import {required, emailValid} from '../../Commons/Validators'
import ListSelectorGroup from '../../Commons/ListSelectorGroup'
import SelectRedux from '../../Commons/SelectRedux'

const FormGroupInline = (props)=> {
    const {input, label, type,  meta: {  touched,  error  }} = props
    return(
      <div className={classnames('col-md-2', { 'has-danger': touched && error })}>
        <input {...input} placeholder={label} type={type} className='form-control mb-2 mr-sm-2 mb-sm-0'/>
        {touched && ((error &&   <div className='form-control-feedback'>{error}</div>))}
      </div>
    )
}

const AddUserForm = (props) => {
    const {
      handleSubmit,
      submitting,
      invalid,
      departmentList,
      handleDepartments,
      initialValues,
      selectedDepartments,
      errorMessage,
      managers,
      leaveManagers,
      expenseManagers,
      staffTypes
    } = props
    return (
        <div>
        <form className='form-users' onSubmit={handleSubmit}>
          <div className='row mb-sm-2'>
            <Field name='firstName' type='text' component={FormGroupInline} label='First Name' validate={required}/>
            <Field name='lastName' type='text' component={FormGroupInline} label='Last Name' validate={required}/>
            <Field name='email' type='text' component={FormGroupInline} label='Email' validate={[required, emailValid]}/>
            <Field name='designation' type='text' component={FormGroupInline} label='Position' validate={required}/>
            <Field className='col-md-2 mb-sm-0' name='departments' type='text' component={ListSelectorGroup}
              list={departmentList} placeholder='Departments' multiSelect={true}
              nonSelectedIconClass='fa fa-circle-thin'
              selectedIconClass='fa fa-check-circle'
              showIcons onAnswered={handleDepartments} values={initialValues && selectedDepartments}/>
          </div>
          <div className='row' >
            <Field className='col-md-2' classLabel='sr-only' name='lineManager' options={managers} component={SelectRedux} label='Line Manager'/>
            <Field className='col-md-2' classLabel='sr-only' name='leaveManager' options={leaveManagers} component={SelectRedux} label='Leaves Report To'/>
            <Field className='col-md-2' classLabel='sr-only' name='expenseManager' options={expenseManagers} component={SelectRedux} label='Expense Report To'/>
            <Field className='col-md-2' classLabel='sr-only' name='staffType' options={staffTypes} component={SelectRedux} label='Staff Type'/>
            <div className='col-md-2'>
              <button className='btn btn-primary btn-inline' style={{width: '100%'}} type='submit'
                disabled={submitting || invalid}>
                Add
              </button>
            </div>
          </div>
        </form>
        <div className='has-danger'>
          {errorMessage && <div className='form-control-feedback'>
            {errorMessage || `Unable to add member. Please ensure the entered email.`}
          </div>}
        </div>
        </div>
    )
}
export default reduxForm({form: 'addUserForm'})(AddUserForm)
