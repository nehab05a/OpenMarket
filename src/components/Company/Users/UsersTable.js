import React from 'react';
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import UserStatus from './UserStatus'
import UserAvatar from './UserAvatar'
import { withRouter } from 'react-router-dom'
import _ from 'lodash'

class UsersTable extends React.PureComponent {
  render () {
    const { data, departments, history, updateStatus } = this.props,
    getMappedRecords = (records) => {
      const
        mappedRecords = _.filter(departments, department => _.includes(records, department.departmentId))
      return mappedRecords;
    },
    _styles = {
      icons: {
        fontSize : '12px',
      },
      tableHeader: {
        textAlign: 'left',
        color: '#7F8FA4',
        fontWeight: '600',
        padding: '15px'
      },
      tableRow: {
        alignItems: 'center',
      },
      tableCell: {
        padding: '8px 15px',
      }
    },
    columns = [{
      Header: 'Name',
      accessor: 'userFullName',
      Cell: (props) => {
        const { userFullName, userPhotoThumbnail } = props.original
        return (
          <UserAvatar avatarUrl={userPhotoThumbnail}>
            {userFullName}
          </UserAvatar>
        )
      },
    }, {
      Header: 'Position',
      accessor: 'position',
    }, {
      Header: 'Departments',
      accessor: 'departments',
      Cell: (props) => {
        const {departments} = props.original,
          selectedDepartments = getMappedRecords(departments)
        return (selectedDepartments[0] && _.capitalize(selectedDepartments[0].departmentName)) || ''
      },
    }, {
      Header: 'Status',
      accessor: 'status',
      Cell: (props) => {
        const {status, id} = props.original
        return (
          <UserStatus updateStatus={updateStatus} userId={id}>{status && status.name}</UserStatus>
        )},
    }, {
      Cell: (props) => {
        const {id, basicSettingsCompleted} = props.original,
        editIconStyle = {
          color: basicSettingsCompleted ? '#20a8d8' : 'red',
          fontSize: '30px',
          margin: '0px 10px'
        }
        return (
            <i className='fa fa-edit' style={editIconStyle} onClick={() => history.push(`/setup/user/${id}`)}/>
        )},
    } ];

    return (
      <div>
        <div className='table-wrap'>
          <ReactTable
            className='-striped -highlight'
            data={data}
            columns={columns}
            defaultPageSize={5}
            getTheadThProps={() => {return {style: _styles.tableHeader}}}
            getTdProps={() => {return {style: _styles.tableCell}}}
            getTrProps={() => {return {style: _styles.tableRow}}}
          />
        </div>
      </div>
    )
  }
}

export default withRouter(UsersTable)
