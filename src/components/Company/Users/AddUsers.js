import React, { Component } from 'react';
import AddUserForm from './AddUserForm'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { reset, change } from 'redux-form';
import { addUser } from '../../../actions/Users'
import autobind from 'react-autobind'

class AddUser extends Component {
  constructor(props) {
    super(props)
    this.state = {}
    autobind(this)
  }

  onSubmit(values) {
    const
    { departmentsValue } = this.state,
    { user, actions: { addUser } } = this.props,
      data = {
        ...values,
        departments: departmentsValue,
        userTenantId: user.userTenantId,
      };
    addUser(data, this._addUserSuccess, this._addUserFailure)
  }

  _addUserSuccess() {
    const { updateList, actions: { resetForm } } = this.props
    this.setState({errorMessage: undefined}, () => {
      updateList()
      resetForm('addUserForm')
    })
  }

  _addUserFailure(errorMessage) {
    this.setState({errorMessage})
  }

  _handleDepartmentsChange(departmentsValue) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      departmentsValue: departmentsValue.map(department => department.id)
    }, () => changeFieldValue('departments', departmentsValue[0].displayName))
  }

  render() {
    const { departmentsList, managers, expenseManagers, leaveManagers, staffTypes } = this.props,
      getOptions = records => records.map( record => {return {label: `${record.firstName} ${record.lastName}`, value: record.id}}),
      staffTypesOptions = staffTypes.map( type => {return {label: type.staff_type, value: type.id}})
    return (
      <div className='mb-3 mt-3'>
        <h5>Manage Users</h5>
        <p className='text-muted'>Add More Users</p>
        <AddUserForm
          {...this.state}
          onSubmit={this.onSubmit}
          managers={getOptions(managers)}
          expenseManagers={getOptions(expenseManagers)}
          leaveManagers={getOptions(leaveManagers)}
          staffTypes={staffTypesOptions}
          departmentList={departmentsList}
          handleDepartments={this._handleDepartmentsChange}
        />
      </div>
    )
  }
}

const
  mapStateToProps = (state) => {
    return {
      user: state.session.user,
    }
  },
  mapDispatchToProps = (dispatch,ownProps) => {
    const
      FORM_NAME = 'addUserForm',
      changeFieldValue = (field, value) => change(FORM_NAME, field, value),
      resetForm = () => reset(FORM_NAME)
    return {
      actions : bindActionCreators({
        addUser,
        changeFieldValue,
        resetForm
      }, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AddUser);
