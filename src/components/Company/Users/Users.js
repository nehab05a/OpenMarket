import React, {Component} from 'react'
import { withRouter } from 'react-router-dom'
import UsersTable from './UsersTable'
import AddUser from './AddUsers'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchUsersList,
  updateCurrentUserStatus,
  fetchCurrentUserDetails,
  fetchManagersList,
  fetchExpenseManagersList,
  fetchLeaveManagersList } from '../../../actions/Users'
import { fetchStaffTypes } from '../../../actions/StaffTypes'
import { fetchDepartments } from '../../../actions/Departments'
import { departmentsListSelector } from '../../../selectors/listSelectors'
import autoBind from 'react-autobind'
import {isEqual} from 'lodash'

class Users extends Component {
  constructor(props) {
      super(props)
      this.state = {
        users: this.props.users,
      }
      autoBind(this)
  }

  componentWillMount(){
    const
      {fetchDepartments, fetchUsersList, fetchStaffTypes, fetchExpenseManagersList, fetchLeaveManagersList, fetchManagersList} = this.props.actions;
    fetchDepartments()
    fetchUsersList()
    fetchManagersList()
    fetchExpenseManagersList()
    fetchLeaveManagersList()
    fetchStaffTypes()
  }

  componentWillReceiveProps(nextProps){
    const { users } = this.props
    !isEqual(nextProps.users, users) && this.setState({users: nextProps.users})
  }

  updateUserStatus(id, status) {
    const
      { updateCurrentUserStatus } = this.props.actions,
      data= {
        id,
        status
      }
      updateCurrentUserStatus(data, this.updateStatusSuccess)
  }

  updateStatusSuccess(id) {
    const { fetchCurrentUserDetails, fetchUsersList } = this.props.actions
    fetchCurrentUserDetails(id)
    fetchUsersList()
  }

  render() {
    const
      {
        departmentsList,
        departments,
        history,
        actions: { fetchUsersList },
        managers,
        expenseManagers,
        leaveManagers,
        staffTypes
      } = this.props,
      { users } = this.state,
      fetchUpdated = () => fetchUsersList()
      return (
        <div className='container-fluid mt-2'>
          <div className='animated fadeIn'>
              <div className='card card-default'>
                  <div className='card-header main-header'>
                      Users
                  </div>

                  <div className='card-block'>
                      <div className='row'>
                          <div className='offset-md-1 col-md-11 '>
                              <AddUser
                                departmentsList={departmentsList}
                                updateList={fetchUpdated}
                                managers={managers}
                                expenseManagers={expenseManagers}
                                leaveManagers={leaveManagers}
                                staffTypes={staffTypes}
                              />
                          </div>
                      </div>

                      <div className='row'>
                          <div className='col-md-12'>
                              <UsersTable
                                data={users}
                                departments={departments}
                                updateStatus={this.updateUserStatus}
                              />
                          </div>
                      </div>

                      <div className='row mt-2'>
                          <div className='col-sm-12'>
                            <div className='float-right'>
                              <button className='btn btn-secondary' type='button' style={{margin: '20px 10px'}}
                                onClick={() => history.push('/company/organisation')}>
                                  <i className='fa fa-arrow-left' aria-hidden='true'></i>
                                  &nbsp; Back
                              </button>
                            </div>
                          </div>
                      </div>

                  </div>
              </div>
          </div>
        </div>
    )
  }
}
const
  mapStateToProps = (state) => {
    return {
      users: state.users,
      departmentsList : departmentsListSelector(state),
      departments: state.companySettings.departments,
      managers: state.managers.allManagers,
      expenseManagers: state.managers.expenseManagers,
      leaveManagers: state.managers.leaveManagers,
      staffTypes: state.companySettings.staffTypes
    }
  },

  mapDispatchToProps = (dispatch,ownProps) => {
    return {
      actions : bindActionCreators({
        fetchUsersList,
        fetchDepartments,
        updateCurrentUserStatus,
        fetchCurrentUserDetails,
        fetchManagersList,
        fetchExpenseManagersList,
        fetchLeaveManagersList,
        fetchStaffTypes
      }, dispatch)
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Users))
