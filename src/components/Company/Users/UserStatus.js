import React, { Component } from 'react';
import classnames from 'classnames'
import autoBind from 'react-autobind'

const getClassType = (state) => {
    switch (state) {
        case 'ACTIVE':
            return 'success';
        case 'PENDING':
            return 'primary';
        case 'SUSPENDED':
            return 'warning';
        default:
            return 'danger';
    }
}

class UserStatus extends Component {
  constructor(props) {
    super(props);

    this.state = {
        isOver : false,
    }
    autoBind(this);
  }

  _onMouseOver() {
    this.setState({ isOver : true})
  }

  _onMouseLeave() {
    this.setState({ isOver : false})
  }

  render() {
    const
      { isOver } = this.state,
      { children, updateStatus, userId } = this.props,
      _styles = {
        controller: {
          marginRight: '10px',
        },
        status: {
          width: '110px',
          textAlign: 'center'
        }
      },
      classType = getClassType(children || 'PENDING'),
      updateValue = (statusValue) => updateStatus(userId, statusValue);

    return (
      <div onMouseLeave={this._onMouseLeave}>
        {
          isOver
        ? <div className='d-flex'>
            <span className={classnames(`btn btn-success btn-sm`)} style={_styles.controller} onClick={() => updateValue('ACTIVE')}>
                <i className='fa fa-check'/>
            </span>
            <span className={classnames(`btn btn-warning btn-sm`)} style={_styles.controller} onClick={() => updateValue('SUSPENDED')}>
                <i className='fa fa-warning'/>
            </span>
            <span className={classnames(`btn btn-danger btn-sm`)}  onClick={() => updateValue('INACTIVE')}>
                <i className='fa fa-remove'/>
            </span>
          </div>
        : <span onMouseEnter={this._onMouseOver} style={_styles.status} className={classnames(`btn btn-${classType} btn-sm`)}>
            {children || 'PENDING'}
          </span>
        }
      </div>
    )
  }
}

export default UserStatus
