import React from 'react';

const UserAvatar = (props) => {
  const _styles = {
    userIcon: {
      color: '#20a8d8',
      fontSize: '25px',
      marginRight: '10px'
    }
  }

  return (
      <div className='d-flex align-items-center'>
          <span>
            {
              props.avatarUrl ?
                <div className='user-avatar'>
                  <img src={props.avatarUrl} className='img-avatar' alt='' height={25} width={25} style={{marginRight: '10px'}}/>
                </div>
              : <span style={_styles.userIcon} >
                  <i className='fa fa-user-circle-o'></i>
                </span>
            }
          </span>
          <span>{props.children}</span>
      </div>
  )
}
export default UserAvatar
