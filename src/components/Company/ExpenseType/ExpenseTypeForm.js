import React from 'react'
import PropTypes from 'prop-types'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../Commons/FormGroup'

const ExpenseTypesForm =(props) => {
    const {handleSubmit, initialValues, onCancel} = props
    return (
        <form onSubmit={handleSubmit} className='form-pb'>
          <h5 className='mb-2'>Enter Details for Expense Types</h5>
          <div className='row'>
            <Field className='col-md-12' name='name' type='text' component={FormGroup} label='Expense Name'/>
          </div>
          <div className='row form-group' style={{padding:'0px 15px'}}>
            <label className='form-control-label'>Description</label>
            <Field className='col-md-12 form-control' name='description' type='textarea' rows='6' placeholder='Description' component='textarea' label='Requirements'/>
          </div>
          <div  className='row d-flex justify-content-center'>
            <button className='col-md-3 btn btn-secondary' style={{margin: '10px'}} type='button' onClick={onCancel}>
              Cancel
            </button>
            <button className='col-md-3 btn btn-primary' style={{margin: '10px'}} type='submit'>
              {initialValues ? `Update` : `Save` }
            </button>
          </div>
        </form>
    )
}

ExpenseTypesForm.propTypes = {
  initialValues: PropTypes.object,
  onCancel: PropTypes.func
}

export default reduxForm({
  form: 'expenseTypeForm',
  enableReinitialize: true
})(ExpenseTypesForm)
