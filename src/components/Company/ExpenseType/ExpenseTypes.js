import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
import ExpenseTypesActions from './ExpensesActions'
import ExpenseTypesTable from './ExpensesTable'
import ExpenseTypeForm from './ExpenseTypeForm'
import * as actionsExpenseTypes from '../../../actions/ExpenseTypes'
import {withRouter} from 'react-router-dom'
import {createFilter} from 'react-search-input'
import {filter, isEqual} from 'lodash'
import autoBind from 'react-autobind'

class ExpenseTypes extends Component {
  constructor(props){
    super(props)
    this.state = {
      searchTerm : '',
      isOpen: false,
      expenseTypes: this.props.expenseTypes
    }
    autoBind(this)
  }

  componentWillMount(){
    const { fetchExpenseTypes } = this.props;
    fetchExpenseTypes()
  }

  componentWillReceiveProps(nextProps){
    const {expenseTypes} = this.props
    !isEqual(nextProps.expenseTypes, expenseTypes) && this.setState({expenseTypes: nextProps.expenseTypes})
  }

  _onSubmit(values) {
    const { updateExpenseTypes } = this.props;
    updateExpenseTypes(values, this._onSubmitSuccess)
  }

  _onEditRecord(initialValues) {
    this.setState({initialValues},
      this._toggleModal
    )
  }

  _onDeleteRecord(id) {
    const { deleteExpenseType } = this.props;
    deleteExpenseType(id, this._onSubmitSuccess)
  }

  _onSubmitSuccess(){
    const { fetchExpenseTypes } = this.props;
    this.setState({isOpen: false},
      () => fetchExpenseTypes()
    )
  }

  render() {
    const
    { isOpen, expenseTypes, initialValues } = this.state,
    { history } = this.props,
    _styles = {
      button:{
        margin: '20px 10px'
      }
    },
    filteredItems = filter(expenseTypes, createFilter(this.state.searchTerm, ['name']));

    return (
      <div className='container-fluid mt-2'>
        <div className='animated fadeIn'>
          <div className='card card-default'>
            <ExpenseTypesActions toggleModal={this._toggleModal} searchUpdated={this._searchUpdated}/>
            <ExpenseTypesTable data={filteredItems} onEdit={this._onEditRecord} onDelete={this._onDeleteRecord}/>
            <div className='row mt-2'>
                <div className='col-sm-12'>
                  <div className='float-right'>
                    <button className='btn btn-secondary' style={_styles.button}
                      type='button' onClick={() => history.push('/company/staff-types')}>
                        <i className='fa fa-arrow-left' aria-hidden='true'></i>
                        &nbsp; Back
                    </button>
                    {' '}
                    <button className='btn btn-primary' style={_styles.button}
                      type='button' onClick={() => history.push('/company/leave')}>
                        Next &nbsp;
                        <i className='fa fa-arrow-right' aria-hidden='true'></i>
                    </button>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <Modal isOpen={isOpen}>
         <ModalHeader toggle={this._toggleModal}>{initialValues ? `Update` : `Add`} Expense Type</ModalHeader>
         <ModalBody>
           <ExpenseTypeForm
            onSubmit={this._onSubmit}
            initialValues={initialValues}
            onCancel={this._toggleModal}
           />
         </ModalBody>
        </Modal>
      </div>
    );
  }

  _toggleModal(clear) {
    if(clear) this.setState({initialValues: undefined})
    this.setState({isOpen: !this.state.isOpen})
  }

  _searchUpdated (term) {
    this.setState({searchTerm: term})
  }

}

const
  mapStateToProps = (state) => ({
      expenseTypes: state.companySettings.expenseTypes
    }
  )

export default withRouter(connect(mapStateToProps, actionsExpenseTypes)(ExpenseTypes));
