import React from 'react'
import PropTypes from 'prop-types'
import {Field, reduxForm} from 'redux-form'
import {required, emailValid} from '../../Commons/Validators'
import SelectRedux from '../../Commons/SelectRedux'
import FormGroupInline from '../../Commons/FormGroupInline'

const AddPositionForm = (props) => {
    const {
      handleSubmit,
      submitting,
      invalid,
      users,
      errorMessage,
      leaveManagers,
      expenseManagers,
      staffTypes
    } = props
    return (
        <div>
          <form className='form-users' onSubmit={handleSubmit}>
          <div className='row mb-sm-2'>
            <Field className='col-md-2' name='firstName' type='text' component={FormGroupInline} label='First Name' validate={required}/>
            <Field className='col-md-2' name='lastName' type='text' component={FormGroupInline} label='Last Name' validate={required}/>
            <Field className='col-md-2' name='designation' type='text' component={FormGroupInline} label='Position' validate={required}/>
            <Field className='col-md-2' name='email' type='text' component={FormGroupInline} label='Email' validate={[required, emailValid]}/>
          </div>
          <div className='row'>
            <Field className='col-md-2' classLabel='sr-only' name='manager' options={users} component={SelectRedux} label='Report To' validate={required}/>
            <Field className='col-md-2' classLabel='sr-only' name='leaveManager' options={leaveManagers} component={SelectRedux} label='Leaves Report To'/>
            <Field className='col-md-2' classLabel='sr-only' name='expenseManager' options={expenseManagers} component={SelectRedux} label='Expense Report To'/>
            <Field className='col-md-2' classLabel='sr-only' name='staffType' options={staffTypes} component={SelectRedux} label='Staff Type'/>

            <div className='col-md-2'>
              <button className='btn btn-primary btn-inline' style={{width: '100%'}} type='submit'
                disabled={submitting || invalid}>
                Add
              </button>
            </div>
          </div>
          </form>
          <div className='has-danger'>
            {errorMessage && <div className='form-control-feedback'>
              {errorMessage || `Unable to add member. Please ensure the entered email.`}
            </div>}
          </div>
        </div>
    )
}

AddPositionForm.propTypes = {
  users: PropTypes.array,
  errorMessage: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
}

export default reduxForm({form: 'addPositionForm'})(AddPositionForm)
