import React, {Component} from 'react'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux';
import OrgChart from './OrganisationChart'
import AddPosition from './AddPosition'
import * as actions from '../../../actions/Organisation'
import { fetchStaffTypes } from '../../../actions/StaffTypes'
import { fetchUsersList, fetchExpenseManagersList, fetchLeaveManagersList } from '../../../actions/Users'
import autoBind from 'react-autobind'
import {isEqual} from 'lodash'

class Organisation extends Component {
  constructor() {
    super()
    autoBind(this)
  }

  componentWillMount() {
    this.state = {
      users:this.props.users,
      usersOrg:this.props.usersOrg,
      expenseManagers: this.props.expenseManagers,
      leaveManagers: this.props.leaveManagers,
      staffTypes: this.props.staffTypes
    }
    const {fetchUsersList, fetchExpenseManagersList, fetchLeaveManagersList, fetchOrganisation, fetchStaffTypes} = this.props.actions;
    fetchUsersList()
    fetchLeaveManagersList()
    fetchExpenseManagersList()
    fetchOrganisation()
    fetchStaffTypes()
  }

  componentWillReceiveProps(nextProps) {
    const {users, usersOrg, staffTypes, expenseManagers, leaveManagers} = this.props
    !isEqual(nextProps.users, users) && this.setState({users: nextProps.users})
    !isEqual(nextProps.usersOrg, usersOrg) && this.setState({usersOrg: nextProps.usersOrg})
    !isEqual(nextProps.expenseManagers, expenseManagers) && this.setState({expenseManagers: nextProps.expenseManagers})
    !isEqual(nextProps.leaveManagers, leaveManagers) && this.setState({leaveManagers: nextProps.leaveManagers})
    !isEqual(nextProps.staffTypes, staffTypes) && this.setState({staffTypes: nextProps.staffTypes})
  }

  _onNext() {
    const {history} = this.props
    history.push('/company/users')
  }

  _onBack() {
    const {history} = this.props
    history.push('/company/departments')
  }

  render() {
    const
      { fetchOrganisation, addOrganisationMember, updateOrganisation } = this.props.actions,
      { usersOrg, users, expenseManagers, leaveManagers, staffTypes } = this.state,
      _styles = {
        button:{
          margin: '20px 10px'
        }
      },
      renderChart = () => fetchOrganisation(),
      addMember = (member, handleSuccess, handleFailure) => addOrganisationMember(member, handleSuccess, handleFailure)
      return (
          <div className='container-fluid mt-2'>
            <div className="animated fadeIn">
              <div className="card card-default">
                <div className="card-header main-header">
                    Organisation
                </div>
                <div className="card-block">
                  <div className="row">
                    <div className="offset-md-1 col-md-11 ">
                        <AddPosition
                          users={users}
                          usersOrg={usersOrg}
                          renderChart={renderChart}
                          addMember={addMember}
                          expenseManagers={expenseManagers}
                          leaveManagers={leaveManagers}
                          staffTypes={staffTypes}
                        />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                        <OrgChart usersOrg={usersOrg} updateOrganisation={updateOrganisation}/>
                    </div>
                  </div>
                  <div className='row mt-2'>
                    <div className='col-sm-12'>
                      <div className='float-right'>
                        <button className='btn btn-secondary' type='button'
                          style={_styles.button} onClick={this._onBack}>
                          <i className='fa fa-arrow-left' aria-hidden='true'></i>
                          &nbsp; Back
                        </button>
                        {' '}
                        <button className='btn btn-primary' type='button'
                          style={_styles.button} onClick={this._onNext}>
                          Next &nbsp;
                          <i className='fa fa-arrow-right' aria-hidden='true'></i>
                        </button>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      )
  }
}

const
mapStateToProps = (state) => {
  return {
    users: state.users,
    expenseManagers: state.managers.expenseManagers,
    leaveManagers: state.managers.leaveManagers,
    usersOrg: state.companySettings.organisation,
    staffTypes: state.companySettings.staffTypes
  }
},

mapDispatchToProps = (dispatch,ownProps) => {
  return {
    actions : bindActionCreators({
      fetchUsersList,
      fetchExpenseManagersList,
      fetchLeaveManagersList,
      fetchStaffTypes,
      ...actions
    }, dispatch)
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Organisation));
