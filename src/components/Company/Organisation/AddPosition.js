import React, {Component} from 'react';
import AddPositionForm from './AddPositionForm'
import {connect} from 'react-redux'
import {reset} from 'redux-form'
import { bindActionCreators } from 'redux'
import autoBind from 'react-autobind'
import { fetchUsersList } from '../../../actions/Users'

class AddPosition extends Component {
  constructor(props){
    super(props)
    autoBind(this)
    this.state = {}
  }

  onSubmit(values) {
    this.props.addMember(values, this.onSubmitSuccess, this.onSubmitFailure)
  }

  onSubmitSuccess() {
    this.state = {users: this.props.users}
    const
      { fetchUsersList, resetForm } = this.props.actions,
      { renderChart } = this.props
    fetchUsersList()
    resetForm()
    renderChart()
  }

  onSubmitFailure(errorMessage) {
    this.setState({errorMessage})
  }

  render() {
      const { users, staffTypes, expenseManagers, leaveManagers } = this.props,
      userOptions = users.map( user => {return {label: `${user.userFullName} - ${user.position || ''}`, value: user.id}}),
      staffTypesOptions = staffTypes.map( type => {return {label: type.staff_type, value: type.id}}),
      expenseManagersOptions = expenseManagers.map( user => {return {label: `${user.firstName} ${user.lastName}`, value: user.id}}),
      leaveManagersOptions = leaveManagers.map( user => {return {label: `${user.firstName} ${user.lastName}`, value: user.id}});
      return (
          <div className='mb-3 mt-3'>
              <h5>Organisation Chart</h5>
              <p className='text-muted'>Quick Add</p>
              <AddPositionForm
                {...this.state}
                onSubmit={this.onSubmit}
                users={userOptions}
                expenseManagers={expenseManagersOptions}
                leaveManagers={leaveManagersOptions}
                staffTypes={staffTypesOptions}
              />
          </div>
      )
  }
}

const
  mapDispatchToProps = (dispatch,ownProps) => {
    const resetForm = () => reset('addPositionForm')
    return {
      actions : bindActionCreators({
        fetchUsersList,
        resetForm,
      }, dispatch)
    }
  };


export default connect(null, mapDispatchToProps)(AddPosition);
