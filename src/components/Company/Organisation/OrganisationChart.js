import React, {Component} from 'react'
import PropTypes from 'prop-types'
import OrgChart from './orgchart'
import cloneDeep from 'lodash/cloneDeep'
import autoBind from 'react-autobind'
import {withRouter} from 'react-router-dom'

class OrganisationChart extends Component {
    constructor(props){
      super(props)
      autoBind(this)
    }

    componentDidUpdate(prevProps) {
      const {usersOrg} = this.props;
      this.paintOrgChart(cloneDeep(usersOrg), this._updateHierarchy, this._editUser);
    }

    _updateHierarchy(values) {
      const { updateOrganisation } = this.props
      updateOrganisation(values)
    }

    _editUser(id) {
      this.props.history.push(`/setup/user/${id}`)
    }

    paintOrgChart(usersOrg, updateTree, edit) {
      setTimeout(() => {
        document.getElementById("chart-container").innerHTML = "";
        const orgchart =  new OrgChart({
              'chartContainer': '#chart-container',
              'pan': true,
              'data': usersOrg,
              'nodeContent': 'title',
              'toggleSiblingsResp': true,
              'draggable': true,
              'nodeID': 'id',
              'createNode': (node, data) => {
                  const
                    imageUrl = data.userPhoto,
                    matches = data.name && data.name.match(/\b(\w)/g),
                    alternateText = matches && matches.join('');

                  let secondMenuIcon = document.createElement('i'),
                      secondMenu = document.createElement('div');

                  secondMenuIcon.setAttribute('class', 'fa  fa-user-circle-o second-menu-icon');

                  secondMenuIcon.addEventListener('mouseenter', (event) => {
                      event.target.nextElementSibling.classList.toggle('hidden');
                      secondMenuIcon.setAttribute('class', 'fa fa-pencil-square-o second-menu-icon');
                  });

                  secondMenuIcon.addEventListener('mouseleave', (event) => {
                      event.target.nextElementSibling.classList.toggle('hidden');
                      secondMenuIcon.setAttribute('class', 'fa  fa-user-circle-o second-menu-icon');
                  });

                  secondMenuIcon.addEventListener('click', (event) => {
                      edit(data.id)
                  });

                  data.id && secondMenu.setAttribute('class', 'second-menu hidden');
                  secondMenu.innerHTML = data.id && `<img class="avatar" src=${imageUrl} alt=${alternateText}>`;

                  node.setAttribute('id', data.id)
                  node.appendChild(secondMenuIcon)
                  node.appendChild(secondMenu);
              },
              'dropCriteria': (draggedNode, dragZone, dropZone) => {
                  if (draggedNode.querySelector(':scope > .content').textContent.includes('manager') &&
                    dropZone.querySelector(':scope > .content').textContent.includes('engineer')) {
                      return false;
                    }
                  return true;
              }
          });

          orgchart.chart.addEventListener('nodedropped.orgchart', function (event) {
            const
              { draggedNode, dropZone } = event.detail,
      	      value = {
                newManagerId: dropZone.id,
                currentUserId: draggedNode.id
              }
            updateTree(value)
          });

      }, 0);
    }

  render() {
    return (
      <div >
          <div id="chart-container"></div>
      </div>
    );
  }
}

OrganisationChart.propTypes = {
  usersOrg: PropTypes.object,
  updateOrganisation: PropTypes.func,
}

export default withRouter(OrganisationChart);
