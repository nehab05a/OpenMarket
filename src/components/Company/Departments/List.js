import React, { Component} from 'react'
import PropTypes from 'prop-types'
import SearchInput, {createFilter} from 'react-search-input'
import {Button} from 'reactstrap'
import autoBind from 'react-autobind'
import {filter, startCase, isEmpty} from 'lodash'

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
        values : this.props.values || [],
        searchTerm : ''
    }
    autoBind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.values !== nextProps.values)
      this.setState({values: nextProps.values})
  }

  render(){
    const
      { values, searchTerm } = this.state,
      { list } = this.props,
      _styles = {
        title : {
          padding: '5px'
        },
        listElement : {
          fontSize: '16px',
          padding: '5px 0',
        },
        favIcon : {
          color : '#20a8d8',
          fontSize: '20px',
          marginRight: '10px'
        },
        selectedItem : {
          color : '#e6e6e6',
          fontSize: '20px',
          marginRight: '10px'
        },
        content : {
          padding: '15px 0px 15px 15px',
          overflow: 'auto',
          flex: 7,
          maxHeight: '700px',
          width: '100%'
        },
        button : {
          width: '90px',
          margin: '15px 0px'
        },
        searchBox: {
          margin: '15px 0px',
          width: '100%',
        },
        emptyContent: {
          textAlign: 'center',
          width: '100%',
          height: '700px',
        },
        item: {
          fontSize: 14,
          fontWeight: 500,
          padding: '5px',
          cursor: 'auto',
          width: '100%',
        },
        userIcon: {
          color: '#20a8d8',
          fontSize: '25px',
          marginRight: '10px'
        }
      },
      filteredItems = filter(list, createFilter(searchTerm, ['userFullName']));

    return(
        <div className='d-flex flex-column align-items-center'>
          <SearchInput className='list-search' style={_styles.searchBox} onChange={this._searchUpdated} />

          {isEmpty(filteredItems) ?
            <div style={_styles.emptyContent}
              className='text-muted d-flex flex-column justify-content-center align-items-center'>
              <i className='fa fa-users' />
              <span>Press + to add a user</span>
            </div>
          : <div style={_styles.content} id='list-scrollbar'>
              {filteredItems.map((element,index) => {
                const isSelected = values.some(item => item.id === element.id)
                return(
                <div key={index} onClick={() => this._onOptionSelected(element)}
                  className='d-flex justify-content-between align-items-center' style={_styles.listElement} >
                  <span className='d-flex align-items-center' style={_styles.item}>
                    <span style={_styles.userIcon} >
                      <i className='fa fa-user-circle-o' style={_styles.image}></i>
                    </span>
                    {startCase(element.userFullName)}
                  </span>
                  <i className={isSelected || element.isSelected ? 'fa fa-check-circle' : 'fa fa-circle-thin'}
                  style={isSelected || element.isSelected ? _styles.favIcon : _styles.selectedItem}></i>
                </div>
              )})}
            </div>
          }
          {!isEmpty(filteredItems) &&
            <div className='d-flex justify-content-around' style={{flex: 2, width:'100%'}}>
              <Button style={_styles.button} onClick={this._onCancel} color="secondary">Cancel</Button>
              <Button style={_styles.button} disabled={isEmpty(values)} onClick={this._onConfirm} color="primary">Apply</Button>
            </div>
          }
        </div>
      )
  }

  _onOptionSelected(value) {
    const
      {multiSelect, onAnswered} = this.props,
      existingValues = this.state.values;
    let values = [];
    if(multiSelect===true){
      values = existingValues.some(item => item.id === value.id) ?
        existingValues.filter((val) => val.id!==value.id) : [...existingValues, value]
    }else{
      values = [value];
    }
    this.setState({values});

    if(onAnswered && !multiSelect) onAnswered(values);
  }

  _onConfirm() {
    const
      {multiSelect, onAnswered} = this.props;

    if(onAnswered && multiSelect) onAnswered(this.state.values);
  }

  _onCancel() {
    this.setState({values : this.props.values});
  }

  _onChangeIndex(listIndex) {
    this.setState({listIndex})
  }

  _searchUpdated (term) {
    this.setState({searchTerm: term})
  }
}

List.propTypes = {
  list : PropTypes.array,
  selectedIconClass : PropTypes.string,
  nonSelectedIconClass : PropTypes.string,
  values : PropTypes.array,
  multiSelect : PropTypes.bool,
  onAnswered : PropTypes.func,
  showIcons : PropTypes.bool,
}

export default List;
