import React from 'react'
import PropTypes from 'prop-types'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import _ from 'lodash'
import ExpandedView from './ExpandedView'

const DepartmentsTable = ({
  data,
  updateStaff,
  updateCurrent,
  items,
  onView
}) => {
  const
    getMappedRecords = (records) => {
      const
        mappedRecords = _.filter(items, item => _.includes(records, item.id))
      return mappedRecords;
    },
    _styles = {
      icons: {
        fontSize : '12px',
      },
      tableHeader: {
        textAlign: 'left',
        color: '#7F8FA4',
        fontWeight: '600',
        padding: '15px'
      },
      tableRow: {
        alignItems: 'center',
      },
      tableCell: {
        padding: '8px 15px'
      }
    },
    columns = [{
      Header: 'Code',
      accessor: 'departmentCode',
      style: {
        color: '#7F8FA4'
      },
      sortable: true
    }, {
      Header: 'Departments Name',
      accessor: 'departmentName',
    }, {
      Header: 'Location',
      accessor: 'departmentLocation',
      style: {
        color: '#7F8FA4'
      }
    }, {
      Header: 'Managers',
      accessor: 'departmentViewers',
      Cell: (props) => {
        const {departmentViewers} = props.original,
          selectedManagers = getMappedRecords(departmentViewers)
        return (selectedManagers[0] && selectedManagers[0].userFullName) || ''
      },
    }, {
      Header: 'No. Staff',
      Cell: (props) => {
        const {departmentMembers} = props.original;
        return departmentMembers && departmentMembers.length
      },
      style: {
        color: '#7F8FA4'
      }
    }, {
      Cell: (props) => {
        const {original} = props;
        return (
          <button className='btn btn-primary' style={{width: '80px'}} type='button' onClick={() => onView(original)}>
            View
          </button>
        )},
    }, {
      expander: true,
      width: 65,
      Expander: ({isExpanded, ...rest}) => {
        return(
        <span style={_styles.icons}>
          {isExpanded ? <i className='icon-arrow-left' /> : <i className='icon-arrow-down'/>}
        </span>
      )},
      style: {cursor: 'pointer', fontSize: 25, padding: '0', textAlign: 'center', userSelect: 'none'},
    }];

    return (
      <div>
        <div className='table-wrap'>
          <ReactTable
            className='-striped -highlight'
            data={data}
            columns={columns}
            defaultPageSize={10}
            SubComponent={(row) => {
              const { departmentMembers, departmentId } = row.original,
                selectedStaff = getMappedRecords(departmentMembers)
              return(
                <div>
                  <ExpandedView
                    selected={{ members: selectedStaff, id: departmentId }}
                    updateStaff={updateStaff}
                    updateCurrent={updateCurrent}
                  />
                </div>
            )}}
            freezeWhenExpanded={true}
            getTheadThProps={() => {return {style: _styles.tableHeader}}}
            getTdProps={() => {return {style: _styles.tableCell}}}
            getTrProps={() => {return {style: _styles.tableRow}}}
          />
        </div>
        <div style={{textAlign: 'center'}}>
          <br />
          <em>Tip: Hold shift when sorting to multi-sort!</em>
        </div>
      </div>
    )
}

DepartmentsTable.propTypes = {
  data: PropTypes.array,
  updateStaff: PropTypes.func,
  updateCurrent: PropTypes.func,
  items: PropTypes.array,
  onView: PropTypes.func,
}

export default DepartmentsTable
