import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { capitalize } from 'lodash'

const
  Item = ({iconClass, title, content}) => {
    const
      _styles = {
        container: {
          width: '80%',
          padding: '10px 25%'
        },
        item: {
          fontSize: 14,
          fontWeight: 500,
          padding: '5px',
          cursor: 'auto',
          width: '100%',
        },
        image: {
          height: '25px',
          width: '25px',
          fontSize: '25px',
          color: '#20a8d8',
          marginRight: '10px',
          textAlign: 'center'
        },
        content: {
          color: '#7F8FA4',
        }
      }
    return (
      <div style={_styles.container} className='d-flex align-items-center'>
        <i className={iconClass} style={_styles.image} />
        <span className='d-flex flex-column'>
          <span>{title}</span>
          <span style={_styles.content}>{content}</span>
        </span>
      </div>
    )
  }

class DetailsView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showAllMembers : false,
      showAllViewers : false,
    }
  }
  render() {
      const
        {
          data,
          members,
          viewers,
          onEdit,
          onCancel,
        } = this.props,
        _styles = {
          container: {
            position: 'relative',
            height: '100%',
            border: '1px solid #DFE3E9',
            overflow: 'auto'
          },
          contentHeader: {
            width: '80%',
            padding: '10px 22%',
            color: '#20a8d8',
          },
          mainContent: {
            padding: '10px'
          },
          footer: {
            padding: '15px'
          },
          showMore: {
            textAlign: 'center',
            color: '#20a8d8',
            padding: '10px',
            width: '100%',
            cursor: 'auto'
          }
        },
        {showAllMembers, showAllViewers} = this.state;

      return (
          <div className='d-flex flex-column justify-content-center'>
            <img src='/img/department.png' alt='' style={{width: '100%', height: '125px'}}/>
            <div style={_styles.mainContent} className='d-flex flex-column justify-content-center align-items-center'>
              <h5>{capitalize(data.departmentName)}</h5>
              <span style={{color: '#7F8FA4'}}>{capitalize(data.departmentLocation)}</span>
            </div>
            <div>
              <span style={_styles.contentHeader}>Managers</span>
              {
                showAllViewers ?
                  viewers.map((viewer, i) => (<Item key={i} iconClass='fa fa-user-circle-o' title={`${viewer.firstName} ${viewer.lastName}`}/>))
                : viewers
                  .slice(0,3)
                  .map((viewer, i) => (<Item key={i} iconClass='fa fa-user-circle-o' title={`${viewer.firstName} ${viewer.lastName}`}/>))
              }
              {
                  viewers.length > 3 &&
                  <div style={_styles.showMore}
                    onClick={() => this.setState({showAllViewers: !showAllViewers})}>
                    Show {showAllViewers ? 'Less' : 'More'}
                  </div>
              }
            </div>
            <div>
              <span style={_styles.contentHeader}>Details</span>
              <Item iconClass='fa fa-lock' title='Code' content={data.departmentCode}/>
              <Item iconClass='fa fa-envelope' title='Email' content={data.departmentEmail}/>
              <Item iconClass='fa fa-phone' title='Phone' content={data.departmentTelephone}/>
              <Item iconClass='fa fa-fax' title='Fax' content={data.departmentFax}/>
              <Item iconClass='fa fa-map-marker' title='Address' content={data.departmentLocation}/>
            </div>
            <div>
              <span style={_styles.contentHeader}>Staff Members</span>
              {
                showAllMembers ?
                  members.map((member, i) => (<Item key={i} iconClass='fa fa-user-circle-o' title={`${member.userFullName}`}/>))
                : members
                  .slice(0,3)
                  .map((member, i) => (<Item key={i} iconClass='fa fa-user-circle-o' title={`${member.userFullName}`}/>))
              }
              {
                  members.length > 3 &&
                  <div style={_styles.showMore}
                    onClick={() => this.setState({showAllMembers: !showAllMembers})}>
                    Show {showAllMembers ? 'Less' : 'More'}
                  </div>
              }
            </div>
            <div style={_styles.footer} className='row d-flex justify-content-center'>
              <button className='col-md-3 btn btn-secondary' style={{marginRight: '20px'}} type='button' onClick={onCancel}>
                Cancel
              </button>
              <button className='col-md-3 btn btn-primary' type='button' onClick={onEdit}>
                Edit
              </button>
            </div>
          </div>
      );
  }
}

DetailsView.propTypes = {
  data: PropTypes.object,
  members: PropTypes.array,
  viewers: PropTypes.array,
  onEdit: PropTypes.func,
  onCancel: PropTypes.func,
}

export default DetailsView;
