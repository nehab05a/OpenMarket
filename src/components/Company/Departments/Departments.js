import React, {Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import DepartmentsActions from './DepartmentsActions'
import DepartmentsTable from './DepartmentsTable'
import DepartmentForm from './DepartmentsForm'
import List from './List'
import { allManagersListSelector, shiftUsersListSelector, locationListSelector } from '../../../selectors/listSelectors';
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
import * as actionsDepartments from '../../../actions/Departments'
import * as actionsLocations from '../../../actions/Locations'
import {fetchUsersList, fetchManagersList} from '../../../actions/Users'
import {createFilter} from 'react-search-input'
import {filter,includes,isEqual} from 'lodash'
import autoBind from 'react-autobind'
import {change} from 'redux-form'
import DetailsView from './DetailsView'

class Departments extends Component {
  constructor(props){
    super(props)
    this.state = {
      searchTerm : '',
      isOpen: false,
      currentSelection : {},
      departments : this.props.departments,
      users : this.props.users,
      managers : this.props.managers,
      viewDetails : false,
    }
    autoBind(this)
  }

  componentWillMount(){
    const
      { fetchDepartments, fetchUsersList, fetchLocations, fetchManagersList } = this.props.actions;
    fetchDepartments();
    fetchUsersList();
    fetchLocations();
    fetchManagersList();
  }

  componentWillReceiveProps(nextProps){
    const {
      departments,
      users,
      managers,
    } = this.props
    !isEqual(nextProps.departments, departments) && this.setState({departments: nextProps.departments})
    !isEqual(nextProps.users, users) && this.setState({users: nextProps.users})
    !isEqual(nextProps.managers, managers) && this.setState({managers: nextProps.managers})
  }

  _updateStaff(values) {
    const
      { updateDepartment } = this.props.actions,
      { departments, currentSelection } = this.state,
      { id } = currentSelection,
      restData = departments.filter(item => item.departmentId !== id),
      selectedData = departments.find(item => item.departmentId === id)
    selectedData.departmentMembers = values.map(value => value.id)

    this.setState({
      departments: [...restData,selectedData]
    }, () => updateDepartment({
      departmentId: id,
      members: values.map(value => value.id)
    }, this._onUpdateSuccess, this._onSubmitFailure))
  }

  _updateCurrent(currentSelection) {
    this.setState({currentSelection})
  }

  _onSubmit(values) {
    const
      { updateDepartment } = this.props.actions,
      { locationValue, viewersValue, membersValue } = this.state,
      { departmentId, departmentName, departmentCode, departmentFax, departmentEmail, departmentTelephone } = values,
      data = {
        departmentId,
        departmentName,
        departmentCode,
        fax: departmentFax,
        email: departmentEmail,
        telephone: departmentTelephone,
        location: locationValue || [],
        members: membersValue && membersValue.map(value => value.id),
        viewers: viewersValue && viewersValue.map(value => value.id),
      }
    updateDepartment(data, this._onSubmitSuccess, this._onSubmitFailure)
  }

  _onSubmitSuccess(){
    const { actions: {fetchDepartments} } = this.props;
    this._toggleModal(true)
    fetchDepartments()
  }

  _onUpdateSuccess(){
    const { actions: {fetchDepartments} } = this.props;
    fetchDepartments()
  }

  _onSubmitFailure(){
    this.setState({errorMessage: 'Something went wrong! Please try in some time.'})
  }

  _onNext() {
    const {history} = this.props
    history.push('/company/organisation')
  }

  _onBack() {
    const {history} = this.props
    history.push('/company/locations')
  }

  _onView(initialValues) {
    const
      membersValue = initialValues.departmentMembers && initialValues.departmentMembers.map(member => {return {id: member}}),
      viewersValue = initialValues.departmentViewers && initialValues.departmentViewers.map(viewer => {return {id: viewer}})
    this.setState({
      initialValues,
      viewDetails: true,
      membersValue,
      viewersValue
    },
      this._toggleModal
    )
  }

  _onEdit() {
    this.setState({viewDetails: false})
  }

  _handleLocationChange(locationValue) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      locationValue: {
        id: `${locationValue[0].id}`,
        name: locationValue[0].displayName
      }
    }, () => changeFieldValue('departmentLocation', locationValue[0].displayName))
  }

  _handleManagersChange(viewersValue) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      viewersValue: viewersValue.map(viewer => {return {id : viewer.id}})
    }, () => changeFieldValue('departmentViewers', viewersValue[0].displayName))
  }

  _handleUsersChange(membersValue) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      membersValue: membersValue.map(member => {return {id: member.id}})
    }, () => changeFieldValue('departmentMembers', membersValue[0].displayName))
  }

  render() {
    const
    { departments, users, managers, currentSelection, isOpen, viewDetails, initialValues, viewersValue, membersValue, locationValue } = this.state,
    { departmentId,
      departmentName,
      departmentCode,
      departmentFax,
      departmentEmail,
      departmentTelephone,
      departmentLocation,
      departmentMembers,
      departmentViewers
    } = initialValues || {},
    getStateValue = (record, values) => filter(values, value => includes(record, value.id)),
    members = getStateValue(departmentMembers, users),
    viewers = getStateValue(departmentViewers, managers),
    initialData = {
      departmentId,
      departmentName,
      departmentCode,
      departmentFax,
      departmentEmail,
      departmentTelephone,
      departmentLocation,
      departmentMembers: members[0] && members[0].userFullName,
      departmentViewers: viewers[0] && `${viewers[0].firstName} ${viewers[0].lastName}`,
    },
    filteredItems = filter(departments, createFilter(this.state.searchTerm, ['staff.userFullName', 'departmentName'])),
    _styles = {
      list: {
        users : {
          maxHeight: '100%'
        }
      },
      button:{
        margin: '20px 10px'
      }
    },
    { managersList, usersList, locationsList } = this.props;
    return (
      <div className='container-fluid mt-2'>
        <div className='animated fadeIn d-flex'>
          <div className='card card-default d-flex justify-content-between' style={{flex: '8'}}>
            <div>
              <DepartmentsActions
                addDepartments={() => {}}
                searchUpdated={this._searchUpdated}
                toggleModal={this._toggleModal}
              />
              <DepartmentsTable
                {...this.state}
                data={filteredItems}
                items={users}
                updateCurrent={this._updateCurrent}
                updateStaff={this._updateStaff}
                onView={this._onView}
              />
            </div>
            <div className='row mt-2'>
                <div className='col-sm-12'>
                  <div className='float-right'>
                    <button className='btn btn-secondary' type='button'
                      style={_styles.button} onClick={this._onBack}>
                      <i className='fa fa-arrow-left' aria-hidden='true'></i>
                      &nbsp; Back
                    </button>
                    {' '}
                    <button className='btn btn-primary' type='button'
                      style={_styles.button} onClick={this._onNext}>
                      Next &nbsp;
                      <i className='fa fa-arrow-right' aria-hidden='true'></i>
                    </button>
                    </div>
                </div>
            </div>
          </div>

          <div className='card card-default' style={{marginLeft: '15px',width: '100%', flex: '2'}}>
            <List
              list={users}
              values={currentSelection.members}
              onAnswered={this._updateStaff}
              multiSelect
            />
          </div>
        </div>
        <Modal isOpen={isOpen}>
         <ModalHeader toggle={this._toggleModal}>{viewDetails ? `Department` : initialValues ? `Update Department` : `Add Department`}</ModalHeader>
         {viewDetails ?
          <DetailsView
            data={initialValues || {}}
            onEdit={this._onEdit}
            onCancel={this._toggleModal}
            members={members}
            viewers={viewers}
          />
          :<ModalBody>
             <DepartmentForm
                {...this.state}
                initialValues={initialValues ? initialData : undefined}
                onSubmit={this._onSubmit}
                onCancel={this._toggleModal}
                managersList={managersList}
                selectedManagers={viewersValue}
                selectedLocation={locationValue && [locationValue]}
                usersList={usersList}
                selectedUsers={membersValue}
                locationsList={locationsList}
                handleLocation={this._handleLocationChange}
                handleManagers={this._handleManagersChange}
                handleUsers={this._handleUsersChange}
              />
            </ModalBody>}
        </Modal>
      </div>
    );
  }

  _toggleModal(clear) {
    if(clear) this.setState({
      initialValues: undefined,
      locationValue: undefined,
      viewersValue: undefined,
      membersValue: undefined,
      errorMessage: undefined,
      viewDetails: false,
    })
    this.setState({isOpen: !this.state.isOpen})
  }

  _searchUpdated (term) {
    this.setState({searchTerm: term})
  }

}

Departments.propTypes = {
  managersList: PropTypes.object,
  usersList: PropTypes.object,
  locationsList: PropTypes.object,
  departments: PropTypes.array,
  users: PropTypes.array,
  managers: PropTypes.array,
  actions: PropTypes.object,
}

const
  mapStateToProps = (state) => {
    return {
      managersList: allManagersListSelector(state),
      usersList: shiftUsersListSelector(state),
      locationsList: locationListSelector(state),
      departments: state.companySettings.departments,
      users: state.users,
      managers: state.managers.allManagers
    }
  },

  mapDispatchToProps = (dispatch) => {
    const changeFieldValue = (field, value) => change('departmentForm', field, value)
    return {
      actions : bindActionCreators({
        ...actionsDepartments,
        ...actionsLocations,
        fetchUsersList,
        fetchManagersList,
        changeFieldValue
      },dispatch)
    }
  }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Departments));
