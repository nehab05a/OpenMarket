import React from 'react'
import PropTypes from 'prop-types'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../Commons/FormGroup'
import ListSelectorGroup from '../../Commons/ListSelectorGroup'
import {required,emailValid} from '../../Commons/Validators'
import DepartmentPhoto from './DepartmentsPhoto'

const AddDepartmentForm =({
  handleSubmit,
  submitting,
  invalid,
  managersList,
  usersList,
  locationsList,
  onCancel,
  initialValues,
  handleLocation,
  handleManagers,
  handleUsers,
  selectedUsers,
  selectedManagers,
  selectedLocation,
  errorMessage
}) => {
    return (
        <form onSubmit={handleSubmit} className='form-pb'>
            <h5 className='mb-2'>Enter Details for Department</h5>
            <div className='row'>
              <Field className='col-md-6' name='departmentName' type='text' component={FormGroup} label='Department Name' validate={[required]}/>
              <Field className='col-md-6' name='departmentCode' type='text' component={FormGroup} label='Department Code '/>
            </div>
            <div className='row'>
              <Field className='col-md-6' name='departmentTelephone' type='text' component={FormGroup} label='Telephone'/>
              <Field className='col-md-6' name='departmentFax' type='text' component={FormGroup} label='Fax'/>
            </div>
            <div className='row'>
              <Field className='col-md-6' name='departmentEmail' type='text' component={FormGroup} label='Email Address' validate={[required,emailValid]}/>
              <Field className='col-md-6' name='departmentLocation' type='text' component={ListSelectorGroup}
                list={locationsList} label='Location' onAnswered={handleLocation} values={selectedLocation} validate={[required]}/>
            </div>
            <DepartmentPhoto />
            <div className='row'>
              <Field className='col-md-6' name='departmentViewers' type='text' component={ListSelectorGroup}
                list={managersList} label='Add Managers' multiSelect={true}
                nonSelectedIconClass='fa fa-circle-thin'
                selectedIconClass='fa fa-check-circle'
                showIcons onAnswered={handleManagers} values={selectedManagers}/>
              <Field className='col-md-6' name='departmentMembers' type='text' component={ListSelectorGroup}
                list={usersList} label='Add Users' multiSelect={true}
                nonSelectedIconClass='fa fa-circle-thin'
                selectedIconClass='fa fa-check-circle'
                showIcons onAnswered={handleUsers} values={selectedUsers}/>
            </div>
            <div className='has-danger d-flex justify-content-center'>
              {errorMessage && <div className='form-control-feedback'>{errorMessage}</div>}
            </div>
            <div  className='row d-flex justify-content-center'>
              <button className='col-md-3 btn btn-secondary' style={{margin: '10px'}} type='button' onClick={onCancel}>
                Cancel
              </button>
              <button className='col-md-3 btn btn-primary' style={{margin: '10px'}} type='submit' disabled={submitting || invalid}>
                {initialValues ? `Update` : `Save`}
              </button>
            </div>
        </form>
    )
}

AddDepartmentForm.propTypes = {
  managersList: PropTypes.object,
  usersList: PropTypes.object,
  locationsList: PropTypes.object,
  onCancel: PropTypes.func,
  initialValues: PropTypes.object,
  handleLocation: PropTypes.func,
  handleManagers: PropTypes.func,
  handleUsers: PropTypes.func,
  selectedUsers: PropTypes.array,
  selectedManagers: PropTypes.array,
}

export default reduxForm({
  form: 'departmentForm',
  enableReinitialize: true
})(AddDepartmentForm)
