import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'react-autobind'
import { chunk } from 'lodash'

class ExtendedView extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selected: props.selected
    }
    autobind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.selected !== nextProps.selected)
      this.setState({selected: nextProps.selected})
  }

  componentDidMount() {
    const { updateCurrent, selected } = this.props;
    updateCurrent(selected)
  }

  componentWillUnmount() {
    this.props.updateCurrent({members:[],id:''})
  }

  render() {
    const
      { selected: {members} } = this.state,
      _styles = {
        container: {
          position: 'relative',
          height: '100%',
          flex: 2,
          border: '1px solid #DFE3E9',
          overflow: 'auto'
        },
        item: {
          fontSize: 14,
          fontWeight: 500,
          color: '#607d8b',
          width: '25%',
          padding: '5px',
          cursor: 'default'
        },
        placeholder: {
          textAlign: 'center',
          width: '100%',
          height: '100%',
        },
        userIcon: {
          color: '#20a8d8',
          fontSize: '25px',
          margin: '0px 10px'
        }
      },
      slicedArray = chunk(members, 4);

    return (
      <div className='d-flex flex-column' style={_styles.container} id='list-scrollbar'>
        {slicedArray.length ?
          slicedArray.map((element,index) => (
            <div key={index} className='d-flex'>
              {
                element.map((item, i) => (
                  <div style={_styles.item} className='d-flex align-items-center' key={i}>
                    <span style={_styles.userIcon} >
                      <i className='fa fa-user-circle-o'></i>
                    </span>
                    {item.userFullName}
                  </div>
                ))
              }
            </div>
          ))
        : <div style={_styles.placeholder} className='text-muted d-flex justify-content-center align-items-center'>
            Drag Shift Workers Here
          </div>
        }
      </div>
    );
  }
}

ExtendedView.propTypes = {
  updateCurrent: PropTypes.func,
  selected: PropTypes.object
};

export default ExtendedView;
