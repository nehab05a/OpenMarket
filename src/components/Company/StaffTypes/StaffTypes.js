import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
import StaffTypesActions from './StaffTypesActions'
import StaffTypesTable from './StaffTypesTable'
import StaffTypesForm from './StaffTypesForm'
import * as actionsStaffTypes from '../../../actions/StaffTypes'
import {withRouter} from 'react-router-dom'
import {createFilter} from 'react-search-input'
import {filter, isEqual} from 'lodash'
import autoBind from 'react-autobind'

class StaffTypes extends Component {
  constructor(props){
    super(props)
    this.state = {
      searchTerm : '',
      isOpen: false,
      staffTypes: this.props.staffTypes
    }
    autoBind(this)
  }

  componentWillMount(){
    const { fetchStaffTypes } = this.props;
    fetchStaffTypes()
  }

  componentWillReceiveProps(nextProps){
    const {staffTypes} = this.props
    !isEqual(nextProps.staffTypes, staffTypes) && this.setState({staffTypes: nextProps.staffTypes})
  }

  _onSubmit(values) {
    const { updateStaffTypes } = this.props;
    updateStaffTypes(values, this._onSubmitSuccess)
  }

  _onEditRecord(initialValues) {
    this.setState({initialValues},
      this._toggleModal
    )
  }

  _onDeleteRecord(id) {
    const { deleteStaffType } = this.props;
    deleteStaffType(id, this._onSubmitSuccess)
  }

  _onSubmitSuccess(){
    const { fetchStaffTypes } = this.props;
    this.setState({isOpen: false},
      () => fetchStaffTypes()
    )
  }

  render() {
    const
    { isOpen, staffTypes, initialValues } = this.state,
    { history } = this.props,
    _styles = {
      button:{
        margin: '20px 10px'
      }
    },
    filteredItems = filter(staffTypes, createFilter(this.state.searchTerm, ['name']));

    return (
      <div className='container-fluid mt-2'>
        <div className='animated fadeIn'>
          <div className='card card-default'>
            <StaffTypesActions toggleModal={this._toggleModal} searchUpdated={this._searchUpdated}/>
            <StaffTypesTable data={filteredItems} onEdit={this._onEditRecord} onDelete={this._onDeleteRecord}/>
            <div className='row mt-2'>
                <div className='col-sm-12'>
                  <div className='float-right'>
                    <button className='btn btn-secondary' style={_styles.button}
                      type='button' onClick={() => history.push('/company/settings')}>
                        <i className='fa fa-arrow-left' aria-hidden='true'></i>
                        &nbsp; Back
                    </button>
                    {' '}
                    <button className='btn btn-primary' style={_styles.button}
                      type='button' onClick={() => history.push('/company/expense')}>
                        Next &nbsp;
                        <i className='fa fa-arrow-right' aria-hidden='true'></i>
                    </button>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <Modal isOpen={isOpen}>
         <ModalHeader toggle={this._toggleModal}>{initialValues ? `Update` : `Add`} Staff Type</ModalHeader>
         <ModalBody>
           <StaffTypesForm
            onSubmit={this._onSubmit}
            initialValues={initialValues}
            onCancel={this._toggleModal}
           />
         </ModalBody>
        </Modal>
      </div>
    );
  }

  _toggleModal(clear) {
    if(clear) this.setState({initialValues: undefined})
    this.setState({isOpen: !this.state.isOpen})
  }

  _searchUpdated (term) {
    this.setState({searchTerm: term})
  }

}

const
  mapStateToProps = (state) => ({
      staffTypes: state.companySettings.staffTypes
    }
  )

export default withRouter(connect(mapStateToProps, actionsStaffTypes)(StaffTypes));
