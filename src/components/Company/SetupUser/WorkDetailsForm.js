import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../Commons/FormGroup'
import SwitchFormRight from '../../Commons/SwitchFormRight'
import DatePickerGroup from '../../Commons/DatePickerGroup'

class WorkDetailsForm extends Component {
    render() {
        const {handleSubmit, submitting, invalid, previousPage} = this.props

        return (
            <form className='container form-pb' onSubmit={handleSubmit}>
                <div className='row'>
                    <div className='offset-sm-2 col-lg-8'>
                        <h5 className='mb-2'>Work Patterns</h5>
                        <div className='row'>
                            <Field name='monday' component={SwitchFormRight}  message='Monday'/>
                            <Field name='tuesday' component={SwitchFormRight}  message='Tuesday'/>
                        </div>
                        <div className='row'>
                            <Field name='wednesday' component={SwitchFormRight}  message='Wednesday'/>
                            <Field name='thursday' component={SwitchFormRight}  message='Thursday'/>
                        </div>
                        <div className='row'>
                            <Field name='friday' component={SwitchFormRight}  message='Friday'/>
                            <Field name='saturday' component={SwitchFormRight}  message='Saturday'/>
                        </div>
                        <div className='row'>
                            <Field name='sunday' component={SwitchFormRight}  message='Sunday'/>
                            <Field name='custom'  component={SwitchFormRight}  message='Custom'/>
                        </div>
                        <h5 className='my-2'>Employment Details</h5>
                        <div className='row'>
                            <Field className='col-md-6' name='annualLeaveAllowance' type='text' component={FormGroup} label='Annual Leave Allowance'/>
                            <Field name='fullTime' label='Leave Start Date/Employment Start' component={SwitchFormRight}  message='Full Time'/>
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='leaveStartDate' component={DatePickerGroup} label='Leave Start Date' placeholder='MM/DD/YYYY'/>
                            <Field className='col-md-6' name='employeeStartDate'  component={DatePickerGroup} label='Employment Start' placeholder='MM/DD/YYYY'/>
                        </div>
                        <h5 className='mb-2'>Hourly Rates</h5>
                        <div className='row'>
                            <Field className='col-md-6' name='standardHourlyRate' type='number' component={FormGroup} label='Standard Hourly Rate'/>
                            <Field className='col-md-6' name='chargeableHourlyRate'  type='number' component={FormGroup} label='Chargeable Hourly Rate'/>
                        </div>
                        <h5 className='mb-2'>Modules</h5>
                        <div className='row'>
                            <Field name='ROLE_MODULE_PROJECTS' component={SwitchFormRight}  message='Projects/Tasks'/>
                            <Field name='ROLE_MODULE_TIME' component={SwitchFormRight}  message='Time'/>
                        </div>
                        <div className='row'>
                            <Field name='ROLE_MODULE_EXPENSES' component={SwitchFormRight}  message='Expenses'/>
                            <Field name='ROLE_MODULE_LEAVE' component={SwitchFormRight}  message='Leave'/>
                        </div>
                        <div className='row'>
                            <Field name='ROLE_MODULE_REPORTS' component={SwitchFormRight}  message='Reports'/>
                            <Field name='ROLE_MODULE_FEEDS' component={SwitchFormRight}  message='Feeds'/>
                        </div>
                        <div className='row'>
                            <Field name='ROLE_MODULE_MY_SHIFTS'  component={SwitchFormRight}  message='My Shifts'/>
                            <Field name='ROLE_MODULE_SHIFT_MANAGER' component={SwitchFormRight}  message='Shifts Manager'/>
                        </div>
                        <div className='row mt-2'>
                            <div className='col-sm-12'>
                              <div className='float-right'>
                                <button className='btn btn-secondary' type='button' onClick={previousPage} style={{margin: '20px 10px'}}>
                                    <i className='fa fa-arrow-left' aria-hidden='true'></i>
                                    &nbsp; Back
                                </button>
                                {' '}
                                <button className='btn btn-primary' type='submit' disabled={submitting || invalid} style={{margin: '20px 10px'}}>
                                    Next &nbsp;
                                    <i className='fa fa-arrow-right' aria-hidden='true'></i>
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

export default reduxForm({
  form: 'setupWizard',               // <------ same form name
  destroyOnUnmount: false,        // <------ preserve form data
  forceUnregisterOnUnmount: true
})(WorkDetailsForm)
