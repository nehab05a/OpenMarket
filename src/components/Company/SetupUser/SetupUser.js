import React, {Component} from 'react'
import Home from '../../Home'
import MenuCompany from '../MenuCompany'
import UserDetailsForm from './UserDetailsForm'
import WorkDetailsForm from './WorkDetailsForm'
import SettingsForm from './SettingsForm'
import Wizard from './Wizard'
import Select from 'react-select'
import { bindActionCreators } from 'redux'
import { change, initialize } from 'redux-form'
import { fetchDepartments } from '../../../actions/Departments'
import { fetchLocations } from '../../../actions/Locations'
import { fetchStaffTypes } from '../../../actions/StaffTypes'
import * as selectors from '../../../selectors/listSelectors'
import * as actions from '../../../actions/Users'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom'
import autoBind from 'react-autobind'
import _ from 'lodash'
import moment from 'moment'

const Header = (props) => (
  <ul className='nav navbar-nav hidden-md-down'>
    <li className='nav-item px-1'>User Setup</li>
  </ul>
)

class SetupUser extends Component {

  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      users: this.props.users,
      locations: this.props.locations,
      departments: this.props.departments,
      StaffTypes: this.props.staffTypes,
      currentUser: {},
    }
    autoBind(this)
  }

  componentWillMount() {
    const { actions: {
      fetchUsersList,
      fetchCurrentUserDetails,
      fetchManagersList,
      fetchLeaveManagersList,
      fetchExpenseManagersList,
      fetchLocations,
      fetchStaffTypes,
      fetchDepartments
     }, match: {params : {id}} } = this.props;
    fetchCurrentUserDetails(id, null, this._handleSelectedValues)
    fetchUsersList()
    fetchExpenseManagersList()
    fetchLeaveManagersList()
    fetchLocations()
    fetchDepartments()
    fetchManagersList()
    fetchStaffTypes()
  }

  componentWillReceiveProps(nextProps) {
    const {users, departments, locations, currentUser, staffTypes} = this.props
    !_.isEqual(nextProps.currentUser, currentUser) && this.setState({currentUser: nextProps.currentUser})
    !_.isEqual(nextProps.users, users) && this.setState({users: nextProps.users})
    !_.isEqual(nextProps.departments, departments) && this.setState({departments: nextProps.departments})
    !_.isEqual(nextProps.locations, locations) && this.setState({locations: nextProps.locations})
    !_.isEqual(nextProps.staffTypes, staffTypes) && this.setState({staffTypes: nextProps.staffTypes})
  }

  nextPage() {
    this.setState({
      page: this.state.page + 1
    })
  }

  previousPage() {
    this.setState({
      page: this.state.page - 1
    })
  }

  _onSubmit(values) {
    const
      {
        lineManager,
        leaveManager,
        expenseManager,
        departmentsFieldValue,
        allowedDepartmentsFieldValue,
        location,
        staffType
      } = this.state,
      { history, actions: {updateCurrentUser} } = this.props,

      roles = ['ROLE_AMEND_BUDGETS', 'ROLE_AUTHORISE_EXPENSES', 'ROLE_AUTHORIZE_LEAVE', 'ROLE_BOOK_LEAVE_REQUEST',
        'ROLE_CHARGE_EXPENSES', 'ROLE_CREATE_LEAVETYPE', 'ROLE_CREATE_PROJECT', 'ROLE_CREATE_SHIFT', 'ROLE_DELETE_PROJECT',
        'ROLE_EXPORT_ALL_REPORTS', 'ROLE_EXPORT_BUDGETS', 'ROLE_EXPORT_EXPENSES', 'ROLE_FLAG_EXPENSES_AS_PAID',
        'ROLE_MODULE_EXPENSES', 'ROLE_MODULE_LEAVE', 'ROLE_MODULE_MY_SHIFTS', 'ROLE_MODULE_PROJECTS', 'ROLE_MODULE_SHIFT_MANAGER',
        'ROLE_MODULE_TIME', 'ROLE_VIEW_ALL_DEPARTMENT', 'ROLE_VIEW_ALL_REPORTS', 'ROLE_VIEW_TEAM_WORK_PATTERN',
        'ROLE_VIEW_THESE_REPORTS', 'ROLE_MODULE_FEEDS', 'ROLE_MODULE_REPORTS', 'ROLE_VIEW_THESE_DEPARTMENT' ],

      boolValues = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'custom', 'manager'],

      setBooleans = (records) => _.reduce(records, (result, record) => {
        switch (record) {
          case 'ROLE_VIEW_ALL_DEPARTMENT':
            result[record]= values.ROLE_VIEW_THESE_DEPARTMENT ? false : true
            break;
          case 'ROLE_VIEW_ALL_REPORTS':
            result[record]= values.ROLE_VIEW_THESE_REPORTS ? false : true
            break;
          default:
            result[record]= (values[record] || false)
        }
        return result;
      }, {}),

      payload = {
        ...values,
        modules: {
          ...setBooleans(roles),
        },
        ...setBooleans(boolValues),
        lineManager,
        leaveManager,
        expenseManager,
        departments: departmentsFieldValue && departmentsFieldValue.map(value => value.id),
        allowedToView: values.ROLE_VIEW_THESE_DEPARTMENT
                      ? allowedDepartmentsFieldValue && allowedDepartmentsFieldValue.map(value => value.id) : [],
        location,
        leaveStartDate: values.leaveStartDate && moment(values.leaveStartDate).format('DD/MM/YYYY'),
        employeeStartDate: values.employeeStartDate && moment(values.employeeStartDate).format('DD/MM/YYYY'),
        staffType: staffType && staffType.id
      },

      removeKey = (key) => payload[key] !== undefined && delete payload[key]
      roles.map(role => removeKey(role))

      updateCurrentUser(payload, () => history.push('/company/users'))
  }

  _handleSelectedValues(user) {
    const
      { users, departments, locations, staffTypes, actions: { initializeForm } } = this.props,
      { userDetails } = user,

      lineManager = userDetails && userDetails.lineManager,
      leaveManager = userDetails && userDetails.leaveManager,
      expenseManager = userDetails && userDetails.expenseManager,
      userDepartments = userDetails && userDetails.departments,
      allowedDepartments = userDetails && userDetails.allowedToView,
      location = userDetails && userDetails.location,
      staffType = userDetails && userDetails.staffType,

      getLineManagerValue = (value) => {
        const managerValue = value && _.find(users, user => user.id === value.id),
          managerFieldValue = (managerValue && managerValue.userFullName) || ''
        return managerFieldValue
      },

      getDepartmentValue = (value) => {
        const departmentValue = value && _.find(departments, department =>
          department.departmentId === (value[0] && value[0].id)),
          departmentsFieldValue = (departmentValue && departmentValue.departmentName) || ''
        return _.capitalize(departmentsFieldValue)
      },

      getLocationValue = (value) => {
        const locationValue = value && _.find(locations, location => location.id === value.id),
          locationFieldValue = (locationValue && locationValue.branch_name) || ''
        return locationFieldValue
      },

      getStaffTypeValue = (value) => {
        const staffTypeValue = value && _.find(staffTypes, staffType => staffType.id === value.id),
          staffTypeFieldValue = (staffTypeValue && staffTypeValue.staff_type) || ''
        return staffTypeFieldValue
      },

      initialValues = {
        ...user.userDetails,
        ...user.userPermissions,
        lineManager: getLineManagerValue(lineManager),
        leaveManager: getLineManagerValue(leaveManager),
        expenseManager: getLineManagerValue(expenseManager),
        departments: getDepartmentValue(userDepartments),
        location: getLocationValue(location),
        staffType: getStaffTypeValue(staffType),
        leaveStartDate: userDetails.leaveStartDate && moment(userDetails.leaveStartDate).format('MM/DD/YYYY'),
        employeeStartDate: userDetails.employeeStartDate && moment(userDetails.employeeStartDate).format('MM/DD/YYYY'),
        allowedToView: getDepartmentValue(allowedDepartments),
      }

    this.setState({
      lineManager,
      leaveManager,
      expenseManager,
      departmentsFieldValue: userDepartments,
      allowedDepartmentsFieldValue: allowedDepartments,
      location: userDetails && userDetails.location,
      staffType,
      initialValues
    })

    initializeForm(initialValues)
  }

  _handleFieldChange(value, field) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      [field]: {
        id: value[0].id,
      }
    }, () => changeFieldValue(field, value[0].displayName))
  }

  _handleDepartmentsChange(departmentValues) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      departmentsFieldValue: departmentValues.map(department => {return {id: department.id}} )
    }, () => changeFieldValue('departments', departmentValues[0].displayName))
  }

  _handleAllowedDepartmentsChange(departmentValues) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      allowedDepartmentsFieldValue: departmentValues.map(department => {return {id: department.id}} )
    }, () => changeFieldValue('allowedToView', departmentValues[0].displayName))
  }

  _handleUserSelection(selectedOption) {
    const
      userId = selectedOption.value,
      { fetchCurrentUserDetails } = this.props.actions,
      { currentUser } = this.state,
      personalDetails = currentUser && currentUser.userDetails,
      existingUser = {
        id: personalDetails.id,
        title:  personalDetails.title,
        firstName: personalDetails.firstName,
        lastName: personalDetails.lastName,
        email: personalDetails.email,
        telephone: personalDetails.telephone,
        extno: personalDetails.extno,
        faxno: personalDetails.faxno,
        mobile: personalDetails.mobile
      }
    this.setState({
      selectedUser: userId
    }, () => fetchCurrentUserDetails(userId, existingUser, this._handleSelectedValues))
  }

  _handleCheckboxChange(event) {
      this.setState({isDepartmentsChecked: event.target.checked});
  }

  render() {
    const
      {
        departmentsList,
        managersList,
        leaveManagersList,
        expenseManagersList,
        usersList,
        locationsList,
        userOptions,
        staffTypesList
      } = this.props,
      {
        page,
        selectedUser,
        lineManager,
        leaveManager,
        expenseManager,
        departmentsFieldValue,
        allowedDepartmentsFieldValue,
        location,
        staffType,
        isDepartmentsChecked,
        initialValues
      } = this.state,
      handleLineManager = (managerValue) => this._handleFieldChange(managerValue, 'lineManager'),
      handleLeaveManager = (managerValue) => this._handleFieldChange(managerValue, 'leaveManager'),
      handleExpenseManager = (managerValue) => this._handleFieldChange(managerValue, 'expenseManager'),
      handleLocation = (locationValue) => this._handleFieldChange(locationValue, 'location'),
      handleStaffType = (staffTypeValue) => this._handleFieldChange(staffTypeValue, 'staffType')

    return (
      <Home menu={MenuCompany} breadcrumbs={Header}>
        <div>
          <Wizard page={page} user={initialValues && `${_.capitalize(initialValues.firstName)} ${_.capitalize(initialValues.lastName)}`}/>
          <div className='container-fluid mt-2'>
            <div className='animated fadeIn'>
              <div className='card card-default'>
                <div className='card-block'>
                <div className='offset-sm-2 col-lg-8 row form-pb'>
                  <div className='col-md-6 form-group'>
                    <label className='form-control-label'>Add User Based On</label>
                    <Select value={selectedUser} options={userOptions}
                  	onChange={(value) => this._handleUserSelection(value)} />
                  </div>
                </div>
                  {
                    page === 1 &&
                    <UserDetailsForm
                      userPictureUrl={initialValues && initialValues.userPhoto}
                      users={userOptions}
                      onSubmit={this.nextPage}
                      handleLineManager={handleLineManager}
                      handleLeaveManager={handleLeaveManager}
                      handleExpenseManager={handleExpenseManager}
                      handleDepartments={this._handleDepartmentsChange}
                      handleLocation={handleLocation}
                      handleStaffType={handleStaffType}
                      managersList={managersList}
                      leaveManagersList={leaveManagersList}
                      expenseManagersList={expenseManagersList}
                      usersList={usersList}
                      departmentsList={departmentsList}
                      locationsList={locationsList}
                      staffTypesList={staffTypesList}
                      selectedLineManager={lineManager || []}
                      selectedLeaveManager={leaveManager || []}
                      selectedExpenseManager={expenseManager || []}
                      selectedDepartments={departmentsFieldValue || []}
                      selectedLocation={location || []}
                      selectedStaffType={staffType || []}
                    />
                  }
                  {
                    page === 2 &&
                    <WorkDetailsForm
                      users={userOptions}
                      previousPage={this.previousPage}
                      onSubmit={this.nextPage}
                    />
                  }
                  {
                    page === 3 &&
                    <SettingsForm
                      users={userOptions}
                      previousPage={this.previousPage}
                      onSubmit={this._onSubmit}
                      departmentsList={departmentsList}
                      handleCheckboxChange={this._handleCheckboxChange}
                      isDepartmentsChecked={isDepartmentsChecked}
                      allowedDepartments={allowedDepartmentsFieldValue || []}
                      handleAllowedDepartments={this._handleAllowedDepartmentsChange}
                    />
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home>
    )
  }
}

const
  mapStateToProps = (state) => {
    const {
        allManagersListSelector,
        shiftUsersListSelector,
        locationListSelector,
        departmentsListSelector,
        leaveManagersListSelector,
        expenseManagersListSelector,
        staffTypesListSelector
      } = selectors,
      users = state.users,
      userOptions = users.map( user => {return {label: `${user.userFullName} - ${user.position}`, value: user.id}}),
      managersList = allManagersListSelector(state),
      usersList = shiftUsersListSelector(state),
      locationsList = locationListSelector(state),
      departmentsList = departmentsListSelector(state),
      leaveManagersList = leaveManagersListSelector(state),
      expenseManagersList = expenseManagersListSelector(state),
      staffTypesList = staffTypesListSelector(state)

    return {
      users,
      userOptions,
      managersList,
      usersList,
      locationsList,
      staffTypesList,
      departmentsList,
      leaveManagersList,
      expenseManagersList,
      departments: state.companySettings.departments,
      locations: state.companySettings.locations,
      currentUser: state.companySettings.currentUser,
      staffTypes: state.companySettings.staffTypes
    }
  },

  mapDispatchToProps = (dispatch) => {
    const
      changeFieldValue = (field, value) => change('setupWizard', field, value),
      initializeForm = (initialFormData) => initialize('setupWizard', initialFormData)
    return {
      actions: bindActionCreators({
        ...actions,
        fetchDepartments,
        fetchLocations,
        fetchStaffTypes,
        changeFieldValue,
        initializeForm,
      }, dispatch)
    }
  }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SetupUser))
