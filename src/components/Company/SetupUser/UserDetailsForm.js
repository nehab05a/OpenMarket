import React, {Component} from 'react'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../Commons/FormGroup'
import ListSelectorGroup from '../../Commons/ListSelectorGroup'
import SwitchFormRight from '../../Commons/SwitchFormRight'
import SelectRedux from '../../Commons/SelectRedux'
import Profile from './Profile'

const titles =[
  {value:'Mr',label:'Mr'},
  {value:'Ms',label:'Ms'},
  {value:'Mrs',label:'Mrs'}
]
class UserDetailsForm extends Component {

    render() {
        const {
          handleSubmit,
          submitting,
          invalid,
          handleLineManager,
          handleLeaveManager,
          handleExpenseManager,
          handleDepartments,
          handleLocation,
          handleStaffType,
          managersList,
          leaveManagersList,
          expenseManagersList,
          departmentsList,
          locationsList,
          staffTypesList,
          selectedLineManager,
          selectedLeaveManager,
          selectedExpenseManager,
          selectedDepartments,
          selectedLocation,
          selectedStaffType,
          userPictureUrl
        } = this.props
        
        return (
            <form className='container form-pb' onSubmit={handleSubmit}>
                <div className='row'>
                    <div className='offset-sm-2 col-lg-8'>
                        <h5 className='mb-2'>User Details</h5>
                        <div className='row'>
                            <Field className='col-md-6' name='title'  options={titles} component={SelectRedux} label='Title' />
                            <Field className='col-md-6' name='firstName' type='text' component={FormGroup} label='Fist Name' />
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='lastName' type='text' component={FormGroup} label='Last Name' />
                            <Field className='col-md-6' name='email' type='email' component={FormGroup} disabled={true} label='Email Address/User Login ID'/>
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='telephone' type='number' component={FormGroup} label='Telephone No.' />
                            <Field className='col-md-6' name='extno' type='number' component={FormGroup} label='Ext' />
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='faxno' type='number' component={FormGroup} label='Fax No.' />
                            <Field className='col-md-6' name='mobile' type='number' component={FormGroup} label='Mobile No.' />
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='location' type='text' component={ListSelectorGroup}
                              list={locationsList} label='Location' values={[selectedLocation]} onAnswered={handleLocation}/>
                            <Field className='col-md-6' name='designation' type='text' component={FormGroup} label='Position' />
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='departments' type='text' component={ListSelectorGroup}
                              list={departmentsList} label='Departments' multiSelect={true}
                              nonSelectedIconClass='fa fa-circle-thin'
                              selectedIconClass='fa fa-check-circle'
                              showIcons onAnswered={handleDepartments} values={selectedDepartments}/>
                            <Field className='col-md-6' name='staffType' type='text' component={ListSelectorGroup}
                              list={staffTypesList} label='Staff Type' values={[selectedStaffType]} onAnswered={handleStaffType}/>
                        </div>
                        <div className='row'>
                            <Field name='manager' component={SwitchFormRight}  message='Provide Manageral Rights To User'/>
                        </div>
                        <h5 className='mb-2'>Managers</h5>
                        <div className='row'>
                            <Field className='col-md-6' name='lineManager' type='text' component={ListSelectorGroup}
                              list={managersList} label='Line Manager'
                              onAnswered={handleLineManager} values={[selectedLineManager]}/>
                            <Field className='col-md-6' name='leaveManager' type='text' component={ListSelectorGroup}
                              list={leaveManagersList} label='Leave Reports To'
                              onAnswered={handleLeaveManager} values={[selectedLeaveManager]}/>
                        </div>
                        <div className='row'>
                            <Field className='col-md-6' name='expenseManager' type='text' component={ListSelectorGroup}
                              list={expenseManagersList} label='Expenses Reports To'
                              onAnswered={handleExpenseManager} values={[selectedExpenseManager]}/>
                        </div>
                        <h5 className='mb-2'>Profile Image</h5>

                        <Profile url={userPictureUrl || undefined}/>

                        <div className='row mt-2'>
                            <div className='col-sm-12'>
                              <div className='float-right'>
                                <button className='btn btn-primary' type='submit' disabled={submitting || invalid} style={{margin: '20px 10px'}}>
                                    Next &nbsp;
                                    <i className='fa fa-arrow-right' aria-hidden='true'></i>
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

export default reduxForm({
  form: 'setupWizard',            // <------ same form name
  destroyOnUnmount: false,        // <------ preserve form data
  forceUnregisterOnUnmount: true
})(UserDetailsForm)
