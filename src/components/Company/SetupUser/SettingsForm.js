import React from 'react'
import {Field, reduxForm, formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import SelectRedux from '../../Commons/SelectRedux'
import SwitchFormRight from '../../Commons/SwitchFormRight'
import ListSelectorGroup from '../../Commons/ListSelectorGroup'

let SettingsForm = ({
  handleSubmit,
  submitting,
  invalid,
  previousPage,
  ROLE_VIEW_THESE_DEPARTMENT,
  viewTheseReports,
  departmentsList,
  handleAllowedDepartments,
  allowedDepartments
}) => (
  <form className='container form-pb' onSubmit={handleSubmit}>
      <div className='row'>
          <div className='offset-sm-2 col-lg-8'>
              <h6 className='mb-2'>Settings Leave</h6>
              <div className='row'>
                  <Field name='ROLE_AUTHORIZE_LEAVE' component={SwitchFormRight}  message='Allowed to authorise leave'/>
              </div>
              <h6 className='mt-2'>Settings Expenses</h6>
              <div className='row'>
                  <Field name='ROLE_AUTHORISE_EXPENSES' component={SwitchFormRight}  message='Allowed to Authorise Expenses'/>
                  <Field name='ROLE_EXPORT_EXPENSES' component={SwitchFormRight}  message='Allowed to export Expenses'/>
              </div>
              <div className='row'>
                  <Field name='ROLE_FLAG_EXPENSES_AS_PAID' component={SwitchFormRight}  message='Allowed to mark Expenses as Paid'/>
              </div>
              <h6 className='mb-2'>Settings Shifts</h6>
              <div className='row'>
                  <Field name='ROLE_CREATE_SHIFT' component={SwitchFormRight}  message='Allowed to authorise shifts'/>
              </div>
              <h6 className='mt-2'>Settings Reports</h6>
              <div className='row d-flex align-items-center'>
                  {!viewTheseReports &&
                    <Field name='ROLE_VIEW_ALL_REPORTS' component={SwitchFormRight}  message='Allowed to view all reports'/>}
                  <Field name='viewTheseReports'  component={SwitchFormRight}  message='Allowed to view only these reports'/>
                  {viewTheseReports &&
                    <Field className='col-md-6' name='resports' type='text' component={SelectRedux} />
                  }
              </div>
              <div className='row'>
                  <Field name='ROLE_EXPORT_ALL_REPORTS'  component={SwitchFormRight}  message='Allowed to export all reports'/>
              </div>
              <h6 className='mt-2'>Settings Projects</h6>
              <div className='row'>
                  <Field name='ROLE_CREATE_PROJECT' component={SwitchFormRight}  message='Allowed to create a project'/>
                  <Field name='ROLE_DELETE_PROJECT'  component={SwitchFormRight}  message='Allowed to Delete Project'/>
              </div>
              <div className='row'>
                  <Field name='ROLE_EXPORT_BUDGETS' component={SwitchFormRight}  message='Allowed to export budgets'/>
                  <Field name='ROLE_AMEND_BUDGETS'  component={SwitchFormRight}  message='Allowed to amend budgets'/>
              </div>
              <h6 className='mt-2'>Settings Department</h6>
              <div className='row d-flex align-items-center'>
                  {!ROLE_VIEW_THESE_DEPARTMENT &&
                    <Field name='ROLE_VIEW_ALL_DEPARTMENT' component={SwitchFormRight}  message='Allowed to view all Department'/>
                  }
                  <Field name='ROLE_VIEW_THESE_DEPARTMENT' component={SwitchFormRight}  message='Allowed to view only these'/>
                  {ROLE_VIEW_THESE_DEPARTMENT &&
                    <Field className='col-md-6' name='allowedToView' type='text' component={ListSelectorGroup}
                      list={departmentsList} placeholder='View These Departments' multiSelect={true}
                      nonSelectedIconClass='fa fa-circle-thin'
                      selectedIconClass='fa fa-check-circle'
                      showIcons onAnswered={handleAllowedDepartments} values={allowedDepartments}/>
                  }
              </div>

              <div className='row mt-2'>
                  <div className='col-sm-12'>
                    <div className='float-right'>
                      <button className='btn btn-secondary' type='button' onClick={previousPage} style={{margin: '20px 10px'}}>
                          <i className='fa fa-arrow-left' aria-hidden='true'></i>
                          &nbsp; Back
                      </button>
                      {' '}
                      <button className='btn btn-primary' type='submit' disabled={submitting || invalid} style={{margin: '20px 10px'}}>
                          Finish &nbsp;
                          <i className='fa fa-arrow-right' aria-hidden='true'></i>
                      </button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </form>
)

const selector = formValueSelector('setupWizard')

SettingsForm = connect(
  state => {
    const { viewTheseReports, ROLE_VIEW_THESE_DEPARTMENT } = selector(state, 'viewTheseReports', 'ROLE_VIEW_THESE_DEPARTMENT')
    return {
      viewTheseReports,
      ROLE_VIEW_THESE_DEPARTMENT
    }
  }
)(SettingsForm)

SettingsForm = reduxForm({
  form: 'setupWizard',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true
})(SettingsForm)

export default SettingsForm
