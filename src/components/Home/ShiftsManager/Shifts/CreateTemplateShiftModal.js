import React, { Component } from 'react'
import _ from 'lodash'
import OrderedList from '../../../Commons/OrderedList/'
import { templateListSelector } from '../../../../selectors/listSelectors'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import actions from '../../../../actions/ShiftsActions'
import autoBind from 'react-autobind'

class CreateTemplateShift extends Component {
    constructor(props) {
      super(props);
      this.state = {
        shiftTemplates: props.shiftTemplates
      }
      autoBind(this);
    }

    componentWillMount() {
      this.props.actions.getShiftTemplateList();
    }

    componentWillReceiveProps(nextProps) {
      (nextProps.shiftTemplates !== this.state.shiftTemplates) && this.setState({shiftTemplates: nextProps.shiftTemplates})
    }

    render() {
      const
        _styles = {
          modalContainer : {
            width: '400px'
          }
        },
        { templateList, toggleModal } = this.props,
        { shiftTemplates } = this.state,
        favorites = _.filter(shiftTemplates, item => item.isFavorite);
      return (
        <div>
          <OrderedList
            list={templateList}
            multiSelect
            markFavourite
            values={favorites.map(fav => {return {id: fav.templateId}})}
            nonSelectedIconClass='icon-star'
            selectedIconClass='fa fa-star'
            showIcons
            onAnswered={this.onSubmit}
            updateFavourite={this.updateFavourite}
            onCancel={toggleModal}
            confirmText='Create'
            exclusiveSelect={this.getTemplateDetails}
            toggleFavorites={this.getFavoriteTemplates}
          />
        </div>
      )
    }

    getTemplateDetails(template) {
      this.props.actions.getTemplateDetails(template.id, template => this.props.onTemplateSelection(template, true));
    }

    updateFavourite(template) {
      this.props.actions.updateFavorite(template)
    }
}

const
  mapStateToProps = (state, ownProps) => {
    return {
      templateList: templateListSelector(state),
      shiftTemplates: state.shifts.templates.list
    };
  },

  mapDispatchToProps = (dispatch) => {
    const { getShiftTemplateList, updateFavorite, getTemplateDetails } = actions;
    return {
        actions: bindActionCreators( {
          getShiftTemplateList,
          updateFavorite,
          getTemplateDetails,
        }, dispatch)
    };
  }


export default connect(mapStateToProps,mapDispatchToProps)(CreateTemplateShift);
