import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {ModalFooter,Button} from 'reactstrap'
import FormGroup from '../../../Commons/FormGroup'
import {required} from '../../../Commons/Validators'
import DatePickerGroup from '../../../Commons/DatePickerGroup'
import ListSelectorGroup from '../../../Commons/ListSelectorGroup'
import SwitchFormRight from '../../../Commons/SwitchFormRight'
import TimePicker from '../../../Commons/TimePicker'
import CreateShiftTypeForm from './CreateShiftTypeForm'
import moment from 'moment'

const
CreateShiftType = props => <CreateShiftTypeForm {...props}/>,
CreateNewShiftModalForm = ({
  handleSubmit,
  submitting,
  invalid,
  managersList,
  shiftTypeList,
  staffTypesList,
  locationList,
  departmentList,
  staffList,
  selectedManagers,
  selectedShiftType,
  selectedStaffType,
  selectedLocation,
  selectedDepartment,
  selectedStaff,
  handleLocation,
  handleStaffType,
  handleShiftType,
  handleShiftManager,
  handleDepartment,
  handleStaffList,
  onSubmit,
  initialValues,
  createShiftType
}) => {
    const
      isUpdateShift = initialValues && initialValues.shiftAs === 'shift' ? true : false,
      _styles = {
        filterItem:{
          width:'200px',
          display: 'block',
          position: 'relative',
          color: '#607d8b',
          fontWeight: '600',
          fontSize: '12px',
          clear: 'both'
        },
      };
    return (
        <div className='form-pb'>
          <div className='row'>
            <Field className='col-md-6' name='name' type='text' component={FormGroup}
              label='Shift Name' validate={required} disabled={isUpdateShift}/>
            <Field className='col-md-6' name='shift_type' type='text' validate={required}
              component={ListSelectorGroup} list={shiftTypeList} label='Shift Type'
              addItem={true} createComponent={CreateShiftType} onCreateEntry={createShiftType}
              onAnswered={handleShiftType} values={selectedShiftType} disabled={isUpdateShift}/>
          </div>
          <div className='row'>
            <Field className='col-md-6' name='branch_obj' type='text' validate={required}
              component={ListSelectorGroup} list={locationList} label='Branch/Location'
              onAnswered={handleLocation} values={selectedLocation} disabled={isUpdateShift}/>
            <Field className='col-md-6' name='department' type='text' validate={required}
              component={ListSelectorGroup} list={departmentList} label='Department'
              onAnswered={handleDepartment} values={selectedDepartment} disabled={isUpdateShift}/>
          </div>
          <div className='row'>
            <Field className='col-md-6' name='staff_type' type='text' validate={required}
              component={ListSelectorGroup} list={staffTypesList} label='Staff Type'
              addItem={true} createComponent={CreateShiftType} onAnswered={handleStaffType}
              values={selectedStaffType} disabled={isUpdateShift}/>
            <Field className='col-md-6' name='number_of_emps' type='text' component={FormGroup}
              label='Number OF Employees' validate={required} disabled={isUpdateShift}/>
          </div>
          <div className='row'>
            <Field className='col-md-6' name='date' type='text' component={DatePickerGroup} label='Shift Day'
              validate={required}  disabled={isUpdateShift} minDate={moment()}/>
            <Field className='col-md-6' name='managerList' type='text' component={ListSelectorGroup}
              list={managersList} label='Shift Manager' multiSelect={true}
              nonSelectedIconClass='fa fa-circle-thin' validate={required}
              selectedIconClass='fa fa-check-circle' disabled={isUpdateShift}
              showIcons onAnswered={handleShiftManager} values={selectedManagers}/>
          </div>
          <div className='row'>
            <Field className='col-md-6' name='start_time' type='text' component={TimePicker}
              label='Start Time' disabled={isUpdateShift} validate={required}/>
            <Field className='col-md-6' name='end_time' type='text' component={TimePicker}
              label='End Time' disabled={isUpdateShift} validate={[required]}/>
          </div>
          <div className='row'>
            <Field name='isHotShift' label='Hot Shift' component={SwitchFormRight}
              message='Yes' disabled={isUpdateShift}/>
            <Field name='firstComeFirstServe' label='First Come First Serve'
              component={SwitchFormRight} message='Yes' disabled={isUpdateShift}/>
          </div>
          <div className='row'>
            <Field name='isPermitted' label='Trade Shift Permitted'
              component={SwitchFormRight} message='Yes' disabled={isUpdateShift}/>
            <Field name='isApproved' label='Direct Trade' component={SwitchFormRight}
              message='Yes' disabled={isUpdateShift}/>
          </div>
          <div className='row'>
            <Field className='col-md-6' name='incentive' type='text' component={FormGroup}
              label='Incentives' disabled={isUpdateShift} validate={required}/>
            <Field name='allowed_to_decline_request' label='Allowed To Decline Shift Requests'
             component={SwitchFormRight}  message='Yes' disabled={isUpdateShift}/>
          </div>
          <div className='row'>
            <Field name='allow_clocked_in_remotely' label='Allowed To Clock In Remotely'
              component={SwitchFormRight}  message='Yes' disabled={isUpdateShift}/>
            <Field className='col-md-6' name='staffList' type='text' component={ListSelectorGroup}
              list={staffList} label='Shift Workers' multiSelect={true}
              nonSelectedIconClass='fa fa-circle-thin'
              selectedIconClass='fa fa-check-circle'
              showIcons onAnswered={handleStaffList} values={selectedStaff} validate={required}/>
          </div>
          <div className='row form-group' style={{padding:'0px 15px'}}>
            <label className="form-control-label">Notes</label>
            <Field className='col-md-12 form-control' name='note' type='textarea' rows='6' placeholder='Add Notes'
              component='textarea' label='Notes' validate={required} disabled={isUpdateShift}/>
          </div>
          <div className='row form-group' style={{padding:'0px 15px'}}>
            <label className="form-control-label">Requirements</label>
            <Field className='col-md-12 form-control' name='requirement' type='textarea' rows='6' placeholder='Add Requirements'
              component='textarea' label='Requirements' validate={required} disabled={isUpdateShift}/>
          </div>
          <ModalFooter className='d-flex justify-content-center'>
              <Button className='col-md-4' color='primary' onClick={handleSubmit(values =>
                onSubmit({
                  ...values,
                  saveAs: 'shift'
                }))} disabled={submitting || invalid}>
                {initialValues && initialValues.shiftAs === 'shift' ? 'Update Shift' : 'Create shift'}
              </Button>
              <Button className='col-md-4' color='success' onClick={handleSubmit(values =>
                onSubmit({
                  ...values,
                  saveAs: 'template'
                }))} disabled={submitting || invalid}>Save as a Template</Button>
          </ModalFooter>
        </div>
    )
}
export default reduxForm({
    form: 'createShiftForm',
    enableReinitialize : true,
})(CreateNewShiftModalForm)
