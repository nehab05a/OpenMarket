import React from 'react';
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../../Commons/FormGroup';
import {required} from '../../../Commons/Validators';
import { Button } from 'reactstrap'

const CreateShiftTypeForm = ({
  handleSubmit,
  submitting,
  invalid,
  onCancel
}) => {
    return (
      <form onSubmit={handleSubmit} className='form-pb'>
        <div className='row'>
            <Field className='col-md-12' name='shift_type_name' type='text' component={FormGroup} label='Name' validate={required}/>
        </div>
        <div className='row form-group' style={{padding:'0px 15px'}}>
          <label className="form-control-label">Notes</label>
          <Field className='col-md-12 form-control' name='shift_type_note' type='textarea' rows='6' placeholder='Add Notes' component='textarea' label='Notes' validate={required}/>
        </div>
        <div className='d-flex  justify-content-around'>
          <Button className='col-md-4' onClick={onCancel} color="secondary">Cancel</Button>
          <Button type='submit' className='col-md-4' color="primary">Create</Button>
        </div>
      </form>
    )
}
export default reduxForm({
    form: 'createShiftTypeForm',
})(CreateShiftTypeForm)
