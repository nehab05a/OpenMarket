import React, { Component } from 'react'
import autoBind from 'react-autobind'
import ShiftDetails from '../ShiftDetails'
import CreateShiftModal from '../CreateShiftModal'
import shiftActions from '../../../../actions/ShiftsActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from 'moment'

class ShiftContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          showDetails : false,
      }
      autoBind(this);
    }

    render() {
      const
        { item, index } = this.props,
        _styles = {
          listContainer: {
            padding: '5px 10px',
            borderBottom: '1px solid #cfd8dc'
          }
        },
        { showDetails, selectedRecord, editRecord } = this.state
      return (
        <div>
          <div key={index} style={_styles.listContainer} className='d-flex' onClick={this._getDetails}>
            <div className='d-flex flex-column' style={{flex: '10'}}>
              <span style={{color: '#20a8d8', fontWeight: 500}}>{item.shift_name}</span>
              <span style={{color: 'black', fontWeight: 500}}>{item.shift_type}</span>
              <span>{item.branch_location}</span>
              <span className='d-flex justify-content-between'>
                <span style={{color: 'green'}}>{moment(item.shift_date, "DD/MM/YYYY").format('Do MMM YYYY')}</span>
                <span>{`${item.shift_start_time} - ${item.shift_end_time}`}</span>
              </span>
            </div>
          </div>
          {showDetails && Number(selectedRecord.shift_id) === Number(item.id) &&
            <div>
              <ShiftDetails
                shiftDetails={selectedRecord}
                onEditClicked={this._onShiftEdit}
              />
            </div>
          }
          {editRecord &&
            <CreateShiftModal editRecord={editRecord} />
          }
        </div>
      )
    }

    _toggleShiftDetails() {
      this.setState({ showDetails: !this.state.showDetails })
    }

    _onShiftEdit() {
      const { item } = this.props
      this.props.actions.getShiftTimeDetails(item.id, data => this._handleShiftEdit(data))
    }

    _handleShiftEdit(editRecord) {
      const { item } = this.props
      this.setState({
        editRecord:{
          ...editRecord,
          shiftTimeId: item.id
        }
      }, this._toggleShiftDetails)
    }

    _setSelectedItem(selectedRecord) {
      this.setState({
        selectedRecord
      }, this._toggleShiftDetails)
    }

    _getDetails() {
      const { item } = this.props
      this.props.actions.getShiftsDetails(item, this._setSelectedItem)
    }
}

const
  mapDispatchToProps = (dispatch) => {
    const { getShiftsDetails, getShiftTimeDetails } = shiftActions
    return {
        actions: bindActionCreators( {
          getShiftsDetails,
          getShiftTimeDetails
        }, dispatch)
    };
  }

export default connect(null, mapDispatchToProps)(ShiftContent);
