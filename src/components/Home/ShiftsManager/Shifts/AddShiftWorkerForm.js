import React from 'react';
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../../../Commons/FormGroup';
import ListSelectorGroup from '../../../Commons/ListSelectorGroup';
import {required} from '../../../Commons/Validators';
import DropFile from '../DropFile'

const AddShiftWorkerForm = ({
  handleSubmit,
  submitting,
  invalid,
  toggleModal,
  locationList,
}) => {
    return (
        <form onSubmit={handleSubmit} className='form-pb'>
            <div className='row'>
                <Field className='col-md-6' name='title' type='text' component={FormGroup} label='Title' validate={required}/>
                <Field className='col-md-6' name='firstName' type='text' component={FormGroup} label='First Name' validate={required}/>
            </div>
            <div className='row'>
                <Field className='col-md-6' name='lastName' type='text' component={FormGroup} label='Last Name' validate={required}/>
                <Field className='col-md-6' name='startTime' type='text' component={FormGroup} label='Start Time' validate={required}/>
            </div>
            <div className='row'>
              <Field className='col-md-6' name='telephoneNumber' type='text' component={FormGroup} label='Telephone Number' validate={required}/>
              <Field className='col-md-6' name='mobileNumber' type='text' component={FormGroup} label='Mobile Number' validate={required}/>
            </div>
            <div className='row'>
              <Field className='col-md-6' name='location' type='text' list={locationList} component={ListSelectorGroup} label='Location' validate={required}/>
              <Field className='col-md-6' name='position' type='text' component={FormGroup} label='Position' validate={required}/>
            </div>
            <div className='row'>
              <Field className='col-md-6' name='lineManager' type='text' component={FormGroup} label='Line Manager' validate={required}/>
              <div className=' form-group col-md-6'>
                  <label className='form-control-label'>Upgrade to Full User</label>
                  <div className='form-control control-switch'>
                      <label className='switch switch-3d switch-primary'>
                          <input type='checkbox' className='switch-input'/>
                          <span className='switch-label'></span>
                          <span className='switch-handle'></span>
                      </label>
                      <label className='control-switch-label'>
                          Yes
                      </label>
                  </div>
              </div>
            </div>
            <DropFile />
        </form>
    )
}
export default reduxForm({
    form: 'addShiftWorkerForm',
})(AddShiftWorkerForm)
