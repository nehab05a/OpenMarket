import React from 'react';
import {connect} from 'react-redux';
import Dropzone from 'react-dropzone'
import { Tooltip } from 'reactstrap';
import isEmpty from 'lodash/isEmpty';
const DropFile = props => (
    <div>
        {props.logo
            ? <div className="row">
                    <div className=" form-group col-md-12">
                        <label className="form-control-label">Upload Profile Image</label>
                        <div className="form-control">
                          <button id="tooltipLogo" type="button" onClick={props.deleteLogo} className="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <Tooltip placement="right" isOpen={props.tooltipLogo} target="tooltipLogo" toggle={props.toggle}>
                            Delete Image
                          </Tooltip>
                            {props.logo.map((file,index) =>
                              <img key={index} className="img-fluid" src={file.preview} role="presentation"/>
                            )}
                        </div>
                    </div>
                </div>
            : <div className="row">
                <div className="form-group col-md-12">
                    <label className="form-control-label">Upload Profile Image</label>
                    <Dropzone onDrop={props.onDrop} className="drop-zone"
                      multiple={false}
                      maxSize={2097152}
                       accept="image/*">
                        <div>
                            <h5 className="text-muted">Drag Image Here</h5>
                        </div>
                    </Dropzone>
                    <label className="text-muted">Only images e.g .jpge,.png, only. Image size must be less than 2 mb</label>
                </div>
            </div>}
    </div>
)
const mapStateToProps = (state) => {
    return {logo: state.files.logo,
            tooltipLogo:state.tooltips.tooltipLogo
          }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onDrop: (files) => {
          if(!isEmpty(files)){
            dispatch({type: 'UPLOAD_LOGO', logo: files});
          }
        },
        deleteLogo: () => {
          dispatch({type: 'DELETE_LOGO'});
        },
        toggle: () => {
            dispatch({type: 'TOOGLE_TT_LOGO'});
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(DropFile);
