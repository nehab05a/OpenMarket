import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {ModalFooter, ModalBody, Button} from 'reactstrap'
import {required} from '../../../Commons/Validators'
import ListSelectorGroup from '../../../Commons/ListSelectorGroup'

const
ReAllocateForm = ({
  handleSubmit,
  submitting,
  invalid,
  staffTypesList,
  staffList,
  selectedStaffType,
  selectedStaff,
  handleStaffType,
  handleStaffList,
}) => {
    return (
      <form onSubmit={handleSubmit} className='form-pb' style={{borderTop: '1px solid #cfd8dc'}}>
        <ModalBody className='row'>
          <Field className='col-md-6' name='staff_type' type='text'
            component={ListSelectorGroup} list={staffTypesList} label='Staff Type'
            addItem={true} onAnswered={handleStaffType} validate={[required]}/>
          <Field className='col-md-6' name='staffList' type='text' component={ListSelectorGroup}
            list={staffList} label='Shift Workers' validate={[required]}
            onAnswered={handleStaffList} />
        </ModalBody>
        <ModalFooter className='d-flex justify-content-center'>
          <Button className='col-md-6' color='primary' onClick={() => {}} disabled={submitting || invalid}>Re-Allocate Shift</Button>
        </ModalFooter>
      </form>
    )
}
export default reduxForm({
    form: 'shiftReallocateForm',
    enableReinitialize : true,
})(ReAllocateForm)
