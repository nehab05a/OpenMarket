import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import SearchInput, {createFilter} from 'react-search-input'
import { Modal, Button } from 'reactstrap'
import { filter, capitalize, isEmpty } from 'lodash'
import autoBind from 'react-autobind'
import Details from './Details'
import Filter from '../Filter'
import moment from 'moment'

class ResponsesContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          searchTerm : '',
          showDetails : false,
          declinedItem : {},
      }
      autoBind(this);
    }

    render() {
      const
        _styles = {
          searchBox: {
            width: '130px',
            marginRight: '8px',
          },
          filterLink: {
            fontSize : '25px',
            color : '#b0bec5'
          },
          tools: {
            padding: '10px 15px',
            borderBottom: '1px solid #cfd8dc'
          },
          listContainer: {
            padding: '10px 15px',
            borderBottom: '1px solid #cfd8dc'
          },
          image: {
            height: '18px',
            width: '18px',
            marginRight: '10px'
          },
          remove: {
            flex:1,
            textAlign: 'center',
            padding:'8px',
            color: 'crimson',
            width: '100%',
            borderRight: '1px solid #cfd8dc',
            fontWeight: '500',
            cursor: 'pointer'
          },
          reAllocate:{
            flex:1,
            textAlign:'center',
            padding:'8px',
            color: 'darkgreen',
            fontWeight: '500',
            width: '100%',
            cursor: 'pointer'
          },
          emptyContent: {
            textAlign: 'center',
            width: '100%',
            height: '700px',
          },
          noMatch: {
            width: '100%',
            color:'#20a8d8',
            paddingTop: '40px',
            textAlign:'center'
          },
        },
        { list, staffList, staffTypesList, handleStaffType, handleStaff, applyFilter } = this.props,
        { searchTerm, showDetails, declinedItem } = this.state,
        filteredItems = filter(list, createFilter(searchTerm, ['user_fullname', 'staff_type_name']));

      return (
        <div>
          <div>
            <div className='d-flex justify-content-between align-items-center' style={_styles.tools}>
              <h5 style={{fontSize: '15px'}}>Shifts</h5>
              <div className='d-flex align-items-center'>
                <SearchInput className='list-search' style={_styles.searchBox}  onChange={this._searchUpdated} />
                <Filter applyFilter={applyFilter}/>
              </div>
            </div>
            <div style={{overflow: 'auto', height:'620px'}} id='list-scrollbar'>
              {
                isEmpty(filteredItems) ?
                <div style={_styles.noMatch}>No Records found</div>
              : filteredItems.map((item, index) => {
                  const statusLabel = item.declined_date !== null ? 'Declined' : item.shift_accepted ? 'Accepted' : 'Awaiting'
                  return (
                  <div key={index} onClick={() => statusLabel === 'Declined' && this._showShiftDetails(item)}>
                  <div style={_styles.listContainer} className='d-flex'>
                    <div className='d-flex flex-column' style={{flex: '7'}}>
                      <span style={{color: '#20a8d8', fontWeight: 500}}>{item.user_fullname}</span>
                      <span style={{color: 'black', fontWeight: 500}}>{item.shift_type_name}</span>
                      <span style={{fontSize: '13px'}}>{item.staff_type_name}</span>
                      <span style={{fontSize: '13px'}}>{item.branch_location}</span>
                    </div>
                    <div  style={{flex: '3'}} className='d-flex flex-column align-items-center'>
                      <span style={{color: 'black', fontWeight: 500}}>{moment(item.assigned_date).format('MMM DD')}</span>
                      <Button
                        color={item.declined_date !== null ? 'danger' : item.shift_accepted ? 'success' : 'warning'}>
                        {capitalize(statusLabel)}
                      </Button>
                    </div>
                  </div>
                  </div>
                )})
              }
            </div>
          </div>
          <Modal style={{ width: '420px'}} isOpen={showDetails}>
            <Details
              item={declinedItem}
              toggleModal={this._showShiftDetails}
              removeFromShift={this._removeFromShift}
              reAllocateShift={this._reAllocateShift}
              handleStaffType={handleStaffType}
              handleStaffList={handleStaff}
              staffTypesList={staffTypesList}
              staffList={staffList}
            />
          </Modal>
        </div>
      )
    }

    _searchUpdated (term) {
      this.setState({searchTerm: term})
    }

    _toggleFilter() {
      this.setState({showFilter: !this.state.showFilter})
    }

    _showShiftDetails(declinedItem) {
      this.setState({ showDetails:!this.state.showDetails, declinedItem })
    }

    _removeFromShift(assigned_id) {
      this.props.removeShift(assigned_id, this._showShiftDetails())
    }

    _reAllocateShift(values) {
      this.props.reAllocateShift(values, this._showShiftDetails())
    }
}

const
  mapStateToProps = (state, ownProps) => {
    return {};
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {}, dispatch)
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(ResponsesContent);
