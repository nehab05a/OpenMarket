import React, { Component } from 'react'
import ResponsesContent from './ResponsesContent'
import autobind from 'react-autobind'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { change } from 'redux-form'
import shiftActions from '../../../../actions/ShiftsActions'
import { staffTypesListSelector, staffByStaffTypeListSelector } from '../../../../selectors/listSelectors';
import { fetchStaffTypes } from '../../../../actions/StaffTypes'
import {isEqual, chain, includes, isEmpty} from 'lodash'

class Responses extends Component{
  constructor(props) {
    super(props)
    this.state = {
      addViewOne : false,
      addViewTwo : false,
      responsesList: props.responsesList
    }
    autobind(this)
  }

  componentWillMount() {
    const { fetchStaffTypes, getShiftManagerResponses } = this.props.actions
    fetchStaffTypes()
    getShiftManagerResponses()
  }

  componentWillReceiveProps(nextProps) {
    !isEqual(nextProps.responsesList, this.props.responsesList) && this.setState({responsesList: nextProps.responsesList})
  }

  _onRemoveShift(shiftId) {
    this.props.actions.removeFromShift(shiftId, () => this.props.actions.getShiftManagerResponses())
  }

  _reAllocateShift(assigned_id) {
    const
      { user_id } = this.state,
      payload = {
        user_id,
        assigned_id
      }
    this.props.actions.reAllocateShift(payload, () => this.props.actions.getShiftManagerResponses())
  }

  _handleStaffType(value, field) {
    const { changeFieldValue, getStaffByStaffType } = this.props.actions
    changeFieldValue('staff_type', value[0].displayName)
    getStaffByStaffType(value[0].displayName)
  }

  _handleStaff(values, field) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      user_id: values[0].id
    }, () => changeFieldValue('staffList', values[0].displayName))
  }

  _applyFilter(filters) {
    const
      { responsesList } = this.props,
      { branchFilters, staffTypeFilters, shiftTypeFilters } = filters,
      filteredList = chain(responsesList)
                    .filter(item => !isEmpty(branchFilters) ? includes(branchFilters.map(branch => Number(branch.id)), item.branch_id) : item)
                    .filter(item => !isEmpty(shiftTypeFilters) ? includes(shiftTypeFilters.map(shiftType => Number(shiftType.id)), item.shift_type_id) : item)
                    .filter(item => !isEmpty(staffTypeFilters) ? includes(staffTypeFilters.map(staffType => Number(staffType.id)), item.staff_type_id) : item)
                    .value()

      this.setState({responsesList: filteredList})
  }

  render() {
    const
      _styles = {
        cards : {
          flex:1,
          marginRight:'20px',
          minHeight: '700px',
        },
        placeholder: {
          textAlign: 'center',
          width: '100%',
          minHeight: '700px',
        },
      },
      { addViewOne, addViewTwo, responsesList } = this.state,
      { staffTypesList, staffList } = this.props

    return(
      <div className='d-flex justify-content-between'>
        <div className='card card-default'style={_styles.cards}>
            <ResponsesContent
              list={responsesList}
              removeShift={this._onRemoveShift}
              reAllocateShift={this._reAllocateShift}
              staffTypesList={staffTypesList}
              staffList={staffList}
              handleStaffType={this._handleStaffType}
              handleStaff={this._handleStaff}
              applyFilter={this._applyFilter}
            />
        </div>
        <div className='card card-default' style={_styles.cards}>
        { addViewOne ?
            <ResponsesContent
              list={responsesList}
              removeShift={this._onRemoveShift}
              reAllocateShift={this._reAllocateShift}
              staffTypesList={staffTypesList}
              staffList={staffList}
              handleStaffType={this._handleStaffType}
              handleStaff={this._handleStaff}
              applyFilter={this._applyFilter} />
          : <div style={_styles.placeholder} onClick={() => this.setState({addViewOne: true})}
            className='text-muted d-flex flex-column justify-content-center align-items-center'>
              <i className='fa fa-calendar' style={{fontSize: '25px'}} />
              <span>Add Shift To View</span>
            </div>
        }
        </div>
        <div className='card card-default' style={_styles.cards}>
        { addViewTwo ?
            <ResponsesContent
              list={responsesList}
              removeShift={this._onRemoveShift}
              reAllocateShift={this._reAllocateShift}
              staffTypesList={staffTypesList}
              staffList={staffList}
              handleStaffType={this._handleStaffType}
              handleStaff={this._handleStaff}
              applyFilter={this._applyFilter}
            />
          : <div style={_styles.placeholder} onClick={() => this.setState({addViewTwo: true})}
            className='text-muted d-flex flex-column justify-content-center align-items-center'>
              <i className='fa fa-calendar' style={{fontSize: '25px'}} />
              <span>Add Shift To View</span>
            </div>
        }
        </div>
      </div>
    )
  }
}

const
  mapStateToProps = (state) => {
    return {
      responsesList: state.shifts.shiftManagerResponses,
      staffTypesList: staffTypesListSelector(state),
      staffList: staffByStaffTypeListSelector(state),
    };
  },

  mapDispatchToProps = (dispatch) => {
    const changeFieldValue = (field, value) => change('shiftReallocateForm', field, value)
    return {
        actions: bindActionCreators( {
          ...shiftActions,
          fetchStaffTypes,
          changeFieldValue
        }, dispatch)
    };
  }

export default connect(mapStateToProps, mapDispatchToProps)(Responses);
