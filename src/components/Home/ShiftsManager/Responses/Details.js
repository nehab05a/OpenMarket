import React, {Component} from 'react'
import {ModalBody,ModalHeader,ModalFooter,Button} from 'reactstrap'
import moment from 'moment'
import ReAllocateForm from './ReAllocate'
import autoBind from 'react-autobind'

class ShiftMap extends Component {
  constructor() {
    super()
    this.state = {
      reAllocate: false
    }
    autoBind(this)
  }

  render() {
    const
      _styles = {
        listContainer: {
          padding: '10px 15px',
        },
        image: {
          height: '18px',
          width: '18px',
          marginRight: '10px'
        },
        remove: {
          flex:1,
          textAlign: 'center',
          padding:'8px',
          color: 'crimson',
          width: '100%',
          borderRight: '1px solid #cfd8dc',
          fontWeight: '500',
          cursor: 'pointer'
        },
        reAllocate:{
          flex:1,
          textAlign:'center',
          padding:'8px',
          color: 'darkgreen',
          fontWeight: '500',
          width: '100%',
          cursor: 'pointer'
        },
      },
      { reAllocate } = this.state,
      { item, toggleModal, removeFromShift, staffList, staffTypesList, handleStaffType, handleStaffList } = this.props;
    return (
      <div className='card card-default'>
          <ModalHeader toggle={toggleModal}>Shift Detail</ModalHeader>
          <ModalBody>
              <div style={_styles.listContainer} className='d-flex'>
                <div className='d-flex flex-column' style={{flex: '7'}}>
                  <div>
                    <span style={{color: '#20a8d8',fontWeight: 500}}>{item.user_fullname}</span>
                  </div>
                  <span style={{color: 'black', fontWeight: 500}}>{item.shift_type_name}</span>
                  <span style={{fontSize: '13px'}}>{item.staff_type_name}</span>
                  <span>{`${item.assigned_shift_start_time} - ${item.assigned_shift_end_time}`}</span>
                  <span style={{fontSize: '13px'}}>{item.branch_location}</span>
                  <span style={{color: 'crimson', fontWeight: 500}}>Declined on {item.declined_date}</span>
                </div>
                <div  style={{flex: '3'}} className='d-flex flex-column align-items-center' onClick={this._showShiftDetails}>
                  <span style={{color: 'black', fontWeight: 500}}>{moment(item.assigned_date).format('MMM DD')}</span>
                  <Button color='danger'>Declined</Button>
                </div>
              </div>
          </ModalBody>
          <ModalFooter style={{padding: '0px', borderTop: '1px solid #cfd8dc'}}>
            <span style={_styles.remove} color='secondary' onClick={() => removeFromShift(item.assigned_id)}>Remove From Shift</span>
            <span style={_styles.reAllocate} color='secondary' onClick={this._toggleReallocate}>Re-Allocate</span>
          </ModalFooter>
          {
            reAllocate &&
            <ReAllocateForm
              onSubmit={this._reAllocateShift}
              staffTypesList={staffTypesList}
              staffList={staffList}
              handleStaffType={handleStaffType}
              handleStaffList={handleStaffList}
            />
          }
      </div>
    )
  }

  _toggleReallocate() {
    this.setState({
      reAllocate: !this.state.reAllocate
    })
  }

  _reAllocateShift() {
    const { item: {assigned_id} } = this.props
    this.props.reAllocateShift(assigned_id)
  }
}

export default ShiftMap
