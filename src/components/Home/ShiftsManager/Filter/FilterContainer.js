import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Dropdown, DropdownMenu, DropdownItem } from 'reactstrap'
import autoBind from 'react-autobind'
import FilterSelector from './FilterSelector'
import shiftActions from '../../../../actions/ShiftsActions'
import { fetchStaffTypes } from '../../../../actions/StaffTypes'
import { locationListSelector, shiftTypesListSelector, staffTypesListSelector } from '../../../../selectors/listSelectors';

class FilterShift extends Component {
    constructor(props) {
      super(props);

      this.state = {
          showFilter : false,
          branchFilters : [],
          shiftTypeFilters : [],
          staffTypeFilters : []
      }
      autoBind(this);
    }

    componentWillMount() {
      const { fetchStaffTypes, getShiftTypes} = this.props.actions
      fetchStaffTypes()
      getShiftTypes()
    }

    _toggleFilter() {
      this.setState({showFilter: !this.state.showFilter})
    }

    _setFilter(value, filterField) {
      this.setState({
        [filterField]: value
      }, () => this._setFilterData())
    }

    _resetFilter() {
      this.setState({
        branchFilters: [],
        staffTypeFilters: [],
        shiftTypeFilters: []
      }, () => this._setFilterData())
    }

    _setFilterData() {
      const
        { branchFilters, shiftTypeFilters, staffTypeFilters } = this.state,
        data = {
          branchFilters,
          shiftTypeFilters,
          staffTypeFilters
        };
      this.props.applyFilter(data)
    }

    render() {
      const
        { shiftTypeList, branchList, staffTypeList } = this.props,
        { showFilter, branchFilters, shiftTypeFilters, staffTypeFilters } = this.state,
        _styles = {
          filterLink: {
            fontSize : '25px',
            color : '#b0bec5'
          },
          filterItem:{
            width:'200px',
            display: 'block',
            position: 'relative',
            color: '#607d8b',
            fontWeight: '600',
            fontSize: '12px',
            clear: 'both'
          },
        },

        filters = [
            {
              component: (
                <div style={_styles.filterItem}>
                  <FilterSelector
                    label='Branch'
                    list={branchList}
                    filters={branchFilters}
                    setFilter={ (value) => this._setFilter(value, 'branchFilters')}
                  />
                </div>
              ),
              name: 'By Branch'
            },
            {
              component: (
                <div style={_styles.filterItem}>
                  <FilterSelector
                    label='Shift Type'
                    list={shiftTypeList}
                    filters={shiftTypeFilters}
                    setFilter={ (value) => this._setFilter(value, 'shiftTypeFilters')}
                  />
                </div>
              ),
              name: 'By Shift Type'
            },
            {
              component: (
                <div style={_styles.filterItem}>
                  <FilterSelector
                    label='Staff Type'
                    list={staffTypeList}
                    filters={staffTypeFilters}
                    setFilter={ (value) => this._setFilter(value, 'staffTypeFilters')}
                  />
                </div>
              ),
              name: 'By Staff Type'
            },
            {
              component: (
                <div onClick={this._resetFilter} style={{..._styles.filterItem, color:'#20a8d8'}} className='d-flex justify-content-center'>
                  <span>Reset Filters</span>
                </div>
              ),
              name: 'Reset'
            }
          ];

      return (
        <Dropdown isOpen={showFilter} toggle={this._toggleFilter}>
          <a onClick={this._toggleFilter} data-toggle='dropdown' href='#' role='button'>
              <i className='fa fa-filter' style={_styles.filterLink}></i>
          </a>
          <DropdownMenu className='dropdown-menu-right'>
            {filters && filters.map((item, key) => (
              <DropdownItem key={key}>
                  {item.component}
              </DropdownItem>
            ))}
          </DropdownMenu>
        </Dropdown>
      )
    }
}

const
  mapStateToProps = (state, ownProps) => {
    return {
      shiftTypeList: shiftTypesListSelector(state),
      branchList: locationListSelector(state),
      staffTypeList: staffTypesListSelector(state)
    };
  },

  mapDispatchToProps = (dispatch) => {
    const { getShiftTypes } = shiftActions
    return {
        actions: bindActionCreators( {
          getShiftTypes,
          fetchStaffTypes,
        }, dispatch)
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(FilterShift);
