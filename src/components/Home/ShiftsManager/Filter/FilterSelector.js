import React, { Component } from 'react';
import { Modal, ModalBody, ModalHeader } from 'reactstrap'
import OrderedList from '../../../Commons/OrderedList'
import autoBind from 'react-autobind'

class FilterSelection extends Component{
  constructor(props){
    super(props)
    this.state = {
      showSelection: false,
      selectedValue: this.props.filters
    }
    autoBind(this)
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.filters !== nextProps.filters)
      this.setState({selectedValue: nextProps.filters})
  }

  _toggleSelection() {
    this.setState({showSelection: !this.state.showSelection})
  }

  _setFilter(selectedValue) {
    this.setState({selectedValue})
    this.props.setFilter(selectedValue)
    this._toggleSelection()
  }

  _resetFilter() {
    this.setState({selectedValue: []})
    this.props.setFilter(this.state.selectedValue)
  }

  render() {
    const
      { label } = this.props,
      { showSelection, selectedValue } = this.state;

    return(
       <div>
        <span onClick={this._toggleSelection} className='d-flex justify-content-between'>
          <span style={{color: 'black'}}>{label}</span>
          <span style={{color:'#20a8d8'}}>{selectedValue.length || `All`}</span>
        </span>
        <Modal isOpen={showSelection} style={{width: '400px'}}>
          <ModalHeader toggle={this._toggleSelection}>{label}</ModalHeader>
          <ModalBody>
            <OrderedList
              {...this.props}
              values={selectedValue}
              onCancel={this._resetFilter}
              onAnswered={this._setFilter}
              multiSelect={true}
              nonSelectedIconClass='fa fa-circle-thin'
              selectedIconClass='fa fa-check-circle'
              showIcons
              cancelText='Reset'
            />
          </ModalBody>
        </Modal>
      </div>
    )
  }
}

export default FilterSelection;
