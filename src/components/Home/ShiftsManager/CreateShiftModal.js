import React, {Component} from 'react'
import {Modal,ModalBody,ModalHeader,Button} from 'reactstrap'
import ShiftForm from './Shifts/ShiftForm'
import CreateTemplateShift from './Shifts/CreateTemplateShiftModal'
import autobind from 'react-autobind'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { change } from 'redux-form'
import shiftActions from '../../../actions/ShiftsActions'
import { fetchStaffTypes } from '../../../actions/StaffTypes'
import { fetchLocations } from '../../../actions/Locations'
import {
  shiftManagersListSelector,
  locationListSelector,
  shiftTypesListSelector,
  staffTypesListSelector,
  staffByStaffTypeListSelector,
  departmentsByBranchListSelector
} from '../../../selectors/listSelectors';
import moment from 'moment'

class ShiftModal extends Component {
    constructor(props) {
      super(props)
      this.state = {
        showModal : false,
        showTemplates : false,
        showCreateShifts : false,
      }
      autobind(this)
    }

    componentWillMount() {
      const {
        editRecord,
        actions: {
          getShiftTypes,
          fetchStaffTypes,
          fetchLocations,
          getShiftManagerList
        }} = this.props
      getShiftTypes()
      fetchStaffTypes()
      fetchLocations()
      getShiftManagerList()
      editRecord && this._handleSelected(editRecord)
    }

    _onCreateShift(values) {
      const
        { createShift } = this.props.actions,
        { managerList, staffList, branch_obj, department, staff_type, shift_type } = this.state,
        getKey = values.saveAs === 'template' ? 'template' : 'shift',
        payload = {
          managerList,
          staffList,
          branch_obj,
          department,
          staff_type,
          shift_type,
          incentive: values.incentive,
          number_of_emps: values.number_of_emps,
          allow_clocked_in_remotely: values.allow_clocked_in_remotely || false,
          [`allowed_to_decline_${getKey}_request`]: values.allowed_to_decline_request || false,
          firstComeFirstServe: values.firstComeFirstServe || false,
          isHotShift: values.isHotShift || false,
          [`${getKey}_name`]: values.name,
          [`${getKey}_date`]: moment(values.date, 'MM/DD/YYYY').format('DD/MM/YYYY'),
          [`${getKey}_start_time`]: moment(values.start_time).format('h:mm A'),
          [`${getKey}_end_time`]: moment(values.end_time).format('h:mm A'),
          [`${getKey}_note`]: values.note,
          [`${getKey}_requirement`]: values.requirement,
          saveAs: getKey,
          shiftTimeId: values.shiftTimeId,
          isApproved: values.isApproved || false,
          isPermitted: values.isPermitted || false
        }
      createShift(payload, this.toggleCreateNewShift )
    }

    toggleCreateShift() {
      this.setState({showModal : !this.state.showModal})
    }

    toggleCreateNewShift(clearValues) {
      clearValues && this.setState({initialValues: undefined})
      this.setState({
        showCreateShifts : !this.state.showCreateShifts,
        showTemplates : false,
        showModal : false
      })
    }

    toggleCreateTemplateShift() {
      this.setState({
        showTemplates : !this.state.showTemplates,
        showModal : false
      })
    }

    _handleFieldChange(value, field) {
      const { changeFieldValue } = this.props.actions
      this.setState({
        [field]: value[0].id,
      }, () => changeFieldValue(field, value[0].displayName))
    }

    _handleLocation(value, field) {
      const { changeFieldValue, getDepartmentByBranch } = this.props.actions
      this.setState({
        [field]: value[0].id,
      }, () => changeFieldValue(field, value[0].displayName))
      getDepartmentByBranch(value[0].id)
    }

    _handleDepartment(value, field) {
      const { changeFieldValue } = this.props.actions
      this.setState({
        [field]: {
          id : value[0].id
        },
      }, () => changeFieldValue(field, value[0].displayName))
    }

    _handleStaffType(value, field) {
      const { changeFieldValue, getStaffByStaffType } = this.props.actions
      this.setState({
        [field]: value[0].id,
      }, () => changeFieldValue(field, value[0].displayName))
      getStaffByStaffType(value[0].displayName)
    }

    _handleListFieldChange(values, field) {
      const { changeFieldValue } = this.props.actions
      this.setState({
        [field]: values.map(value => value.id)
      }, () => changeFieldValue(field, values[0].displayName))
    }

    _handleSelected(shiftDetails, isTemplate) {
      const
        { getStaffByStaffType, getDepartmentByBranch } = this.props.actions,
        { shiftTimeId, managers, users, branch, department, staffType, shiftType,
          incentive, allow_clocked_in_remotely, number_of_emps, firstComeFirstServe,
          isHotShift, is_hot_template, shiftDate, template_date, isApproved, isPermitted } = shiftDetails,
        getKey = isTemplate ? 'template' : 'shift',
        initialValues = {
          shiftTimeId,
          incentive,
          number_of_emps,
          allow_clocked_in_remotely,
          firstComeFirstServe,
          isApproved,
          isPermitted,
          managerList: managers.managersName && managers.managersName[0],
          staffList: users.userName && users.userName[0],
          branch_obj: branch.branchName,
          department: department.departmentName,
          staff_type: staffType.staffName,
          shift_type: shiftType.shiftName,
          allowed_to_decline_request: shiftDetails[`allowed_to_decline_${getKey}_request`],
          isHotShift: isTemplate ? is_hot_template : isHotShift,
          name: shiftDetails[`${getKey}_name`],
          note: shiftDetails[`${getKey}_note`],
          requirement: shiftDetails[`${getKey}_requirement`],
          date: moment(template_date || shiftDate).format('MM/DD/YYYY'),
          start_time: moment(shiftDetails[`${getKey}_start_time`], 'h:mm A'),
          end_time: moment(shiftDetails[`${getKey}_end_time`], 'h:mm A'),
          shiftAs: getKey
        }

        this.setState({
          initialValues,
          managerList: managers.managersId,
          staffList: users.userId,
          branch_obj: branch.id,
          department: { id: department.id },
          staff_type: staffType.id,
          shift_type: shiftType.id,
        }, this.toggleCreateNewShift)
        getStaffByStaffType(staffType.staffName)
        getDepartmentByBranch(branch.id)
    }

    _getFavoriteTemplates() {
      this.setState({
        favorites: !this.state.favorites
      }, () => this.props.actions.getFavoriteTemplates())
    }

    _getShiftDetails(shiftId) {
      this.props.actions.getTemplateDetails(shiftId, shiftData => this._handleSelected(shiftData));
    }

    render() {
      const
        _styles = {
          button : {
            margin : '10px',
            padding : '8px'
          }
        },

        handleShiftType = (shiftTypeValue) => this._handleFieldChange(shiftTypeValue, 'shift_type'),
        handleLocation = (locationValue) => this._handleLocation(locationValue, 'branch_obj'),
        handleStaffType = (staffTypeValue) => this._handleStaffType(staffTypeValue, 'staff_type'),
        handleShiftManager = (managerValues) => this._handleListFieldChange(managerValues, 'managerList'),
        handleStaffList = (staffValues) => this._handleListFieldChange(staffValues, 'staffList'),
        handleDepartment = (departmentValues) => this._handleDepartment(departmentValues, 'department'),

        {
          showModal,
          showTemplates,
          showCreateShifts,
          shift_type,
          branch_obj,
          staff_type,
          managerList,
          staffList,
          department,
          initialValues,
          favorites
        } = this.state,
        { editRecord,
          actions: {
            createShiftType
          }
        } = this.props,

        selectedManagers = managerList && managerList.map(manager => {return {id: manager}}),
        selectedShiftType = shift_type && [{id: shift_type}],
        selectedStaffType = staff_type && [{id: staff_type}],
        selectedLocation = branch_obj && [{id: branch_obj}],
        selectedDepartment = department && [department],
        selectedStaff = staffList && staffList.map(user => {return {id: user}}),

        isModalOpen = showCreateShifts || showTemplates || showModal,
        modalHeader = showCreateShifts ?
            (initialValues && initialValues.shiftAs === 'shift') ?
            'Update Shift' : 'Create Shift'
          : showTemplates ? 'Create Shift From Template'
          : null,
        headerToggle = showCreateShifts ? this.toggleCreateNewShift
          : showTemplates ? this.toggleCreateTemplateShift
          : this.toggleCreateShift,
        modalContent = showCreateShifts ?
            <ShiftForm
              {...this.props}
              onSubmit={this._onCreateShift}
              selectedStaff={selectedStaff}
              selectedManagers={selectedManagers}
              selectedLocation={selectedLocation}
              selectedDepartment={selectedDepartment}
              selectedStaffType={selectedStaffType}
              selectedShiftType={selectedShiftType}
              handleStaffType={handleStaffType}
              handleShiftType={handleShiftType}
              handleLocation={handleLocation}
              handleShiftManager={handleShiftManager}
              handleStaffList={handleStaffList}
              handleDepartment={handleDepartment}
              initialValues={initialValues}
              onEditShift={this._getShiftDetails}
              createShiftType={createShiftType}
            />
          : showTemplates ?
            <CreateTemplateShift onTemplateSelection={this._handleSelected}/>
          : <div className='d-flex flex-column justify-content-center align-items-center'>
              <Button style={_styles.button} className='col-md-8' color='primary' onClick={() => this.toggleCreateNewShift(true)}>New Shift</Button>
              <Button style={_styles.button} className='col-md-8' color='success' onClick={this.toggleCreateTemplateShift}>Create Shift From Template</Button>
              <Button style={_styles.button} className='col-md-8' color="secondary" onClick={this.toggleCreateShift}>Cancel</Button>
            </div>;

      return (
        <div>
          {!editRecord &&
            <span>
              <i className='icon-plus' style={{'fontSize': '20px'}} onClick={this.toggleCreateShift}></i>
            </span>
          }
          <Modal style={{ maxWidth: showCreateShifts ? '580px' : '450px'}} isOpen={isModalOpen}>
            <ModalHeader toggle={headerToggle}>
            <div className='d-flex  justify-content-between align-items-center' style={{width: '100%'}}>
              <span>{modalHeader}</span>
              {showTemplates &&
                <i className={favorites ? 'fa fa-star' : 'icon-star'}
                  style={{fontSize: '20px', color: favorites ? '#20a8d8' : '#CCC', marginRight: '20px'}}
                  onClick={this._getFavoriteTemplates}>
                </i>
              }
            </div>
            </ModalHeader>
            <ModalBody>{modalContent}</ModalBody>
          </Modal>
        </div>
      )
    }
}

const
  mapStateToProps = (state, ownProps) => {
    return {
      shiftTypeList: shiftTypesListSelector(state),
      locationList: locationListSelector(state),
      managersList: shiftManagersListSelector(state),
      staffTypesList: staffTypesListSelector(state),
      staffList: staffByStaffTypeListSelector(state),
      departmentList: departmentsByBranchListSelector(state)
    };
  },

  mapDispatchToProps = (dispatch) => {
    const changeFieldValue = (field, value) => change('createShiftForm', field, value)
    return {
        actions: bindActionCreators( {
          ...shiftActions,
          fetchStaffTypes,
          fetchLocations,
          changeFieldValue,
        }, dispatch)
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(ShiftModal)
