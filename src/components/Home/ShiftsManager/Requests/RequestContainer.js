import React, { Component } from 'react'
import RequestsContent from './RequestsContent'
import shiftActions from '../../../../actions/ShiftsActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import autoBind from 'react-autobind'
import {isEqual, chain, includes, isEmpty} from 'lodash'

class Requests extends Component{
  constructor(props) {
    super(props)
    this.state = {
      addViewOne : false,
      addViewTwo : false,
      requestsList : this.props.requestsList
    }
    autoBind(this)
  }

  componentWillMount() {
    this.props.actions.getShiftManagerRequests()
  }

  componentWillReceiveProps(nextProps) {
    !isEqual(nextProps.requestsList, this.props.requestsList) && this.setState({requestsList: nextProps.requestsList})
  }

  render() {
    const
      _styles = {
        cards : {
          flex:1,
          marginRight:'20px',
          minHeight: '700px',
        },
        placeholder: {
          textAlign: 'center',
          width: '100%',
          minHeight: '700px',
        },
      },
      { addViewOne, addViewTwo, requestsList } = this.state

    return(
      <div className='d-flex justify-content-between'>
        <div className='card card-default'style={_styles.cards}>
            <RequestsContent
              list={requestsList}
              acceptShift={this._acceptShift}
              declineShift={this._declineShift}
              applyFilter={this._applyFilter}
            />
        </div>
        <div className='card card-default' style={_styles.cards}>
        { addViewOne ?
            <RequestsContent
              list={requestsList}
              acceptShift={this._acceptShift}
              declineShift={this._declineShift}
              applyFilter={this._applyFilter}
            />
          : <div style={_styles.placeholder} onClick={() => this.setState({addViewOne: true})}
            className='text-muted d-flex flex-column justify-content-center align-items-center'>
              <i className='fa fa-calendar' style={{fontSize: '25px'}} />
              <span>Add Shift To View</span>
            </div>
        }
        </div>
        <div className='card card-default' style={_styles.cards}>
        { addViewTwo ?
            <RequestsContent
              list={requestsList}
              acceptShift={this._acceptShift}
              declineShift={this._declineShift}
              applyFilter={this._applyFilter}
            />
          : <div style={_styles.placeholder} onClick={() => this.setState({addViewTwo: true})}
            className='text-muted d-flex flex-column justify-content-center align-items-center'>
              <i className='fa fa-calendar' style={{fontSize: '25px'}} />
              <span>Add Shift To View</span>
            </div>
        }
        </div>
      </div>
    )
  }

  _acceptShift(values) {
    const
      { normalShiftAccept, getShiftManagerRequests } = this.props.actions
    normalShiftAccept(values, getShiftManagerRequests)
  }

  _declineShift(values) {
    const
      { normalShiftDecline, getShiftManagerRequests } = this.props.actions
    normalShiftDecline(values, getShiftManagerRequests)
  }

  _applyFilter(filters) {
    const
      { requestsList } = this.props,
      { branchFilters, staffTypeFilters, shiftTypeFilters } = filters,
      filteredList = chain(requestsList)
                    .filter(item => !isEmpty(branchFilters) ? includes(branchFilters.map(branch => Number(branch.id)), item.branch_id) : item)
                    .filter(item => !isEmpty(shiftTypeFilters) ? includes(shiftTypeFilters.map(shiftType => Number(shiftType.id)), item.shift_type_id) : item)
                    .filter(item => !isEmpty(staffTypeFilters) ? includes(staffTypeFilters.map(staffType => Number(staffType.id)), item.staff_type_id) : item)
                    .value()

      this.setState({requestsList: filteredList})
  }

}


const
  mapStateToProps = (state, ownProps) => {
    return {
      requestsList: state.shifts.shiftManagerRequests
    };
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...shiftActions
        }, dispatch)
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(Requests);
