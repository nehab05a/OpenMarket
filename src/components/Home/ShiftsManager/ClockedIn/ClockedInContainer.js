import React, { Component } from 'react'
import ClockedInContent from './ClockedInContent'

class ClockedIn extends Component{
  constructor(props) {
    super(props)
    this.state = {
      addViewOne : false,
      addViewTwo : false,
    }
  }
  render() {
    const
      _styles = {
        cards : {
          flex:1,
          marginRight:'20px',
          minHeight: '700px',
        },
        placeholder: {
          textAlign: 'center',
          width: '100%',
          minHeight: '700px',
        },
      },
      { addViewOne, addViewTwo } = this.state,
      testData = [{name: 'Nina Smith'}];
    return(
      <div className='d-flex justify-content-between'>
        <div className='card card-default'style={_styles.cards}>
            <ClockedInContent list={testData} />
        </div>
        <div className='card card-default' style={_styles.cards}>
        { addViewOne ?
            <ClockedInContent list={testData} />
          : <div style={_styles.placeholder} onClick={() => this.setState({addViewOne: true})}
            className='text-muted d-flex flex-column justify-content-center align-items-center'>
              <i className='fa fa-calendar' style={{fontSize: '25px'}} />
              <span>Add Shift To View</span>
            </div>
        }
        </div>
        <div className='card card-default' style={_styles.cards}>
        { addViewTwo ?
            <ClockedInContent list={testData} />
          : <div style={_styles.placeholder} onClick={() => this.setState({addViewTwo: true})}
            className='text-muted d-flex flex-column justify-content-center align-items-center'>
              <i className='fa fa-calendar' style={{fontSize: '25px'}} />
              <span>Add Shift To View</span>
            </div>
        }
        </div>
      </div>
    )
  }
}

export default ClockedIn;
