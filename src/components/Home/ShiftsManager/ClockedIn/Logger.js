import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Modal, ModalBody, ModalHeader, ModalFooter, Button } from 'reactstrap'
import autoBind from 'react-autobind'
import ClockInOutLogger from './ClockInOutLoggerForm'

class ClockedInContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          isOver : false,
          showLogger : false,
          isClockIn : false
      }
      autoBind(this);
    }

    render() {
      const
        _styles = {
          widgetIn: {
            width: '70px',
            height: '70px',
            background: '#20a8d8',
            color: 'white'
          },
          widgetOut: {
            width: '70px',
            height: '70px',
            background: 'crimson',
            color: 'white'
          },
          icon: {
            fontSize: '25px',
          },
          clock: {
            fontSize: '20px',
            color: 'green',
            marginRight: '10px'
          }
        },
        { isOver, showLogger, isClockIn } = this.state;
      return (
        <div>
          <div style={{paddingRight: '10px', height: '70px'}}>
              {isOver ?
                  <div onMouseLeave={this._onMouseLeave}>
                    {
                      isClockIn ?
                        <div className='d-flex'>
                          <span style={_styles.widgetIn} onClick={this._showClockInLogger}
                            className='d-flex flex-column justify-content-center align-items-center'>
                            <i className='icon-clock' style={_styles.icon}/>
                            <span style={{textAlign: 'center'}}>Edit</span>
                          </span>
                          <span style={_styles.widgetOut} onClick={this._showClockOutLogger}
                            className='d-flex flex-column justify-content-center align-items-center'>
                            <i className='icon-clock' style={_styles.icon}/>
                            Clock Out
                          </span>
                        </div>
                      : <span style={_styles.widgetIn} onClick={this._showClockInLogger}
                          className='d-flex flex-column justify-content-center align-items-center'>
                          <i className='icon-clock' style={_styles.icon}/>
                          <span >Clock In</span>
                        </span>
                    }
                  </div>
                : <div className='d-flex align-items-center' style={{height: '100%'}}
                    onMouseEnter={this._onMouseOver}>
                    {isClockIn ? <i className='icon-clock' style={_styles.clock}/> : null}
                    <span className='d-flex flex-column justify-content-between'>
                      <span>Marble Arch</span>
                      <span>08.00 - 12.00</span>
                    </span>
                  </div>
              }
            </div>

            <Modal style={{ width: '420px'}} isOpen={showLogger}>
              <ModalHeader toggle={this._showLogger}>{isClockIn ? 'Clock In' : 'Clock Out'}</ModalHeader>
              <ModalBody>
                <ClockInOutLogger
                  isClockIn={isClockIn}
                  initialValues={{shiftEndTime: '10.00', shiftStartTime: '10.00'}}
                />
              </ModalBody>
              <ModalFooter className='d-flex justify-content-center'>
                  <Button className='col-md-4' color='secondary' onClick={() => {}}>Cancel</Button>
                  <Button className='col-md-4' color='primary' onClick={() => {}}>Done</Button>
              </ModalFooter>
            </Modal>
        </div>
      )
    }

    _showLogger() {
      this.setState({
        showLogger : !this.state.showLogger,
      })
    }

    _showClockInLogger() {
      this._showLogger()
      this.setState({
        isClockIn : true
      })
    }

    _showClockOutLogger() {
      this._showLogger();
      this.setState({
        isClockIn : false
      })
    }

    _onMouseOver() {
      this.setState({ isOver : true})
    }

    _onMouseLeave() {
      this.setState({ isOver : false})
    }
}

const
  mapStateToProps = (state, ownProps) => {
    return {};
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {}, dispatch)
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(ClockedInContent);
