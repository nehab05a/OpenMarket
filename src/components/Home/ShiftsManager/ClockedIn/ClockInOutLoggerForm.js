import React, { Component } from 'react'
import FormGroup from '../../../Commons/FormGroup'
import {Field, reduxForm} from 'redux-form'
import {required} from '../../../Commons/Validators'

class LoggerForm extends Component {
  render() {
    const { isClockIn, handleClockIn, handleClockOut } = this.props;
    return (
      <form onSubmit={isClockIn ? handleClockIn : handleClockOut} className='container form-pb'>
        <div className='row form-group' style={{padding:'0px 15px'}}>
          <label className="form-control-label">Notes</label>
          <Field className='col-md-12 form-control' name='notes' type='textarea' rows='6' placeholder='Add Comments' component='textarea' label='Notes' validate={required}/>
        </div>
        <div className='row'>
            <Field className='col-md-6'
              name={isClockIn ? 'shiftStartTime' : 'shiftEndTime'}
              label={isClockIn ? 'Shift Time Start ' : 'Shift Time End'}
              placeholder='10.00'
              type='text' component={FormGroup} validate={required}/>
            <Field className='col-md-6'
              name={isClockIn ? 'shiftStartTimeActual' : 'shiftEndTimeActual'}
              label={isClockIn ? 'Shift Time Start Actual' : 'Shift Time End Actual'}
              placeholder='00.00'
              type='text' component={FormGroup} />
        </div>
      </form>
    )
  }
}

export default reduxForm({
    form: 'clockInLogger',
    enableReinitialize : true,
})(LoggerForm)
