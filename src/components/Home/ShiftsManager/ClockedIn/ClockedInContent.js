import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import SearchInput, {createFilter} from 'react-search-input'
import { filter, isEmpty } from 'lodash'
import autoBind from 'react-autobind'
import Logger from './Logger'
import CreateShiftModal from '../CreateShiftModal'
import Filter from '../Filter'

class ClockedInContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          searchTerm : '',
          isOver : false,
      }
      autoBind(this);
    }

    render() {
      const
        _styles = {
          searchBox: {
            width: '130px',
            marginRight: '8px',
          },
          filterLink: {
            fontSize : '25px',
            color : '#b0bec5'
          },
          tools: {
            padding: '10px 15px',
            borderBottom: '1px solid #cfd8dc'
          },
          listContainer: {
            borderBottom: '1px solid #cfd8dc'
          },
          item: {
            padding: '10px 15px',
            flex: '7'
          },
          image: {
            height: '18px',
            width: '18px',
            marginRight: '10px'
          },
          emptyContent: {
            textAlign: 'center',
            width: '100%',
            height: '700px',
          }
        },
        { list } = this.props,
        { searchTerm } = this.state,
        filteredItems = filter(list, createFilter(searchTerm, ['name']));
      return (
        <div>
          { isEmpty(filteredItems) ?
              <div style={_styles.emptyContent}
                className='text-muted d-flex flex-column justify-content-center align-items-center'>
                <span>No Shift Record Found</span>
                <CreateShiftModal />
              </div>
            : <div>
                <div className='d-flex justify-content-between align-items-center' style={_styles.tools}>
                  <h5 style={{fontSize: '15px'}}>Shifts</h5>
                  <div className='d-flex align-items-center'>
                    <SearchInput className='list-search' style={_styles.searchBox}  onChange={this._searchUpdated} />
                    <Filter />
                  </div>
                </div>
                <div style={{overflow: 'auto', height:'620px'}} id='list-scrollbar'>
                  {
                    filteredItems.map((item, index) => (
                      <div className='d-flex justify-content-center align-items-center' style={_styles.listContainer} key={index}>
                        <div style={{width: '5px',height: '65px', background:'green'}}></div>
                        <div style={_styles.item} className='d-flex'>
                          <div className='d-flex flex-column'>
                            <div>
                              <img src="/img/avatars/6.jpg" style={_styles.image} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                              <span style={{color: '#20a8d8',fontWeight: 500}}>{item.name}</span>
                            </div>
                            <span style={{color: 'black', fontWeight: 500}}>Morning Shift</span>
                          </div>
                        </div>
                        <Logger />
                      </div>
                    ))
                  }
                </div>
              </div>
            }
        </div>
      )
    }

    _searchUpdated (term) {
      this.setState({searchTerm: term})
    }

    _toggleFilter() {
      this.setState({showFilter: !this.state.showFilter})
    }
}

const
  mapStateToProps = (state, ownProps) => {
    return {};
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {}, dispatch)
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(ClockedInContent);
