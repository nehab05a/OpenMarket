import React, { Component } from 'react'
import Calendar from '../../../Commons/CustomCalendar'
import autoBind from 'react-autobind'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import shiftActions from '../../../../actions/ShiftsActions'
import moment from 'moment'
import { locationListSelector } from '../../../../selectors/listSelectors';
import { managersShiftsCalendarSelector } from '../../../../selectors/calendarSelector';
import DailyContents from './DailyContent'
import {isEqual} from 'lodash'

const sideComponent = props => <DailyContents {...props}/>

class ShiftCalendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showDetails : false,
      showBranchFilter : false,
      showBranchSelection : false,
      branch_id : this.props.branch_id,
      branch_name : this.props.branch_name,
      day_contents : this.props.shiftsForDay
    }

    autoBind(this)
  }

  componentWillReceiveProps(nextProps) {
    !isEqual(nextProps.shiftsForDay, this.state.day_contents) && this.setState({day_contents: nextProps.shiftsForDay})
  }

  showShiftDetails() {
    this.setState({ showDetails:!this.state.showDetails })
  }

  toggleBranchFilter(exclusiveToggle) {
    exclusiveToggle ? this.setState({showBranchSelection: !this.state.showBranchSelection})
    : this.setState({showBranchFilter: !this.state.showBranchFilter})
  }

  render() {
    const
      { calendarData, viewBy, branchList } = this.props,
      { showBranchFilter, branch_id, branch_name, day_contents, showBranchSelection } = this.state,
      calendarFilters = [
        {
          filterLabel: 'Branch',
          component: 'listSelector',
          multiSelect: false,
          filterData: branchList,
          setFilter: this._setBranch,
          toggleSelection: () => this.toggleBranchFilter(),
          showSelection: showBranchFilter,
          selectedValue: [{id: branch_id}],
        }
      ],
      branchSelection = {
        multiSelect: false,
        filterData: branchList,
        setFilter: this._setBranch,
        toggleSelection: () => this.toggleBranchFilter(true),
        showSelection: showBranchSelection,
        selectedValue: [{id: branch_id}],
      }


    return (
      <Calendar
        sideComponent={sideComponent}
        sideComponentList={day_contents}
        eventList={calendarData || []}
        getData={this._getShifts}
        viewBy={viewBy}
        filters={calendarFilters}
        selectedBranch={branch_name}
        getShiftsForDay={this._getShiftsForDay}
        branchSelection={branchSelection}
      />
    )
  }

  _setBranch(branch, callBack = () => {}) {
    this.setState({
      branch_id: branch[0].id,
      branch_name: branch[0].displayName
    }, callBack)
  }

  _getShifts(values){
    const
      { branch_id } = this.state,
      data = {
        ...values,
        branch_id
      }
    this.props.actions.getShiftsForCalendar(data)
  }

  _getShiftsForDay(day) {
    const
      shift_date = moment(day).format('DD/MM/YYYY'),
      { branch_id } = this.state,
      data = {
        shift_date,
        branch_id
      }
    this.props.actions.getShiftsForDay(data)
  }
}

const
  mapStateToProps = (state) => {
    const
      branchList = locationListSelector(state),
      shiftsForDay = state.shifts && state.shifts.shiftsForDay,
      viewBy = state.shifts.shiftManagerCalender && state.shifts.shiftManagerCalender.viewBy,
      calendarData = managersShiftsCalendarSelector(state)

    // TODO: Get calendarData from selector
    return {
      viewBy,
      calendarData,
      branchList,
      branch_id: state.session.user && state.session.user.branch_id,
      branch_name: state.session.user && state.session.user.branch_name,
      shiftsForDay,
    }
  },

  mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
      ...shiftActions,
    }, dispatch)
  });

export default connect(mapStateToProps, mapDispatchToProps)(ShiftCalendar);
