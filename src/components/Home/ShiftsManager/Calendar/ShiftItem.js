import React, { Component } from 'react'
import autoBind from 'react-autobind'
import ShiftDetails from '../ShiftDetails'
import shiftActions from '../../../../actions/ShiftsActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import CreateShiftModal from '../CreateShiftModal'
import moment from 'moment'
import { Button } from 'reactstrap'

class ShiftContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          showDetails : false,
      }
      autoBind(this);
    }

    render() {
      const
        { item, index } = this.props,
        _styles = {
          listContainer: {
            padding: '5px 10px 5px 0px',
            borderBottom: '1px solid #cfd8dc'
          },
          statusBandStyle: {
            width: '6px',
            height: '45px',
            background: item.shift_remaining_empCount > 0 ? '#dfe3e9' : '#1eac74',
            marginRight: '10px'
          }
        },
        { showDetails, selectedRecord, editRecord } = this.state

      return (
        <div>
          <div style={_styles.listContainer} className='d-flex' onClick={this._getDetails}>
            <span style={_styles.statusBandStyle}></span>
            <div className='d-flex flex-column' style={{flex: '10'}}>
              <span className='d-flex justify-content-between'>
                <span style={{fontWeight: 500}}>{`${index}. ${item.shift_name}`}</span>
                <span>
                  <span style={{marginRight: '10px'}}>{item.staff_type_name}</span>
                  <span>{item.branch_name}</span>
                </span>
              </span>
              <span className='d-flex justify-content-between'>
                <span>{`${item.shift_start_time} - ${item.shift_end_time}`}</span>
                <span style={{fontWeight: 500}}>{moment(item.shift_date, 'DD/MM/YYYY').format('DD MMM')}</span>
              </span>
            </div>
          </div>
          {showDetails && selectedRecord.shift_id === item.shift_id &&
            <div>
              <ShiftDetails
                shiftDetails={selectedRecord}
                onEditClicked={this._onShiftEdit}
              />
              <div className='d-flex justify-content-around align-items-center'>
                <Button color='primary' onClick={this._toggleShiftDetails}>Close</Button>
                <Button color='success'>Save As Template</Button>
              </div>
            </div>
          }
          {editRecord &&
            <CreateShiftModal editRecord={editRecord} />
          }
        </div>
      )
    }

    _toggleShiftDetails() {
      this.setState({ showDetails: !this.state.showDetails })
    }

    _onShiftEdit() {
      const { item } = this.props
      this.props.actions.getShiftTimeDetails(item.shift_id, data => this._handleShiftEdit(data))
    }

    _handleShiftEdit(editRecord) {
      const { item } = this.props
      this.setState({
        editRecord:{
          ...editRecord,
          shiftTimeId: item.shift_id
        }
      }, this._toggleShiftDetails)
    }

    _setSelectedItem(selectedRecord) {
      this.setState({
        selectedRecord
      }, this._toggleShiftDetails)
    }

    _getDetails() {
      const { item } = this.props
      this.props.actions.getShiftsDetails(item, this._setSelectedItem)
    }
}

const
  mapDispatchToProps = (dispatch) => {
    const { getShiftsDetails, getShiftTimeDetails } = shiftActions
    return {
        actions: bindActionCreators( {
          getShiftsDetails,
          getShiftTimeDetails
        }, dispatch)
    };
  }

export default connect(null, mapDispatchToProps)(ShiftContent);
