import React, { Component } from 'react'
import Shifts from './Shifts'
import Calendar from './Calendar'
import ClockedIn from './ClockedIn'
import Requests from './Requests'
import Responses from './Responses'
import autobind from 'react-autobind'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import shiftActions from '../../../actions/ShiftsActions'
import TabContainer from './TabContainer'

class ShiftsManager extends Component{
  constructor(props) {
    super(props)
    this.state = {
      page: 0,
    }
    autobind(this)
  }

  componentWillMount() {
    const { getShiftsOfShiftManager, getShiftManagerResponses, getShiftManagerRequests } = this.props.actions
    getShiftsOfShiftManager()
    getShiftManagerRequests()
    getShiftManagerResponses()
  }

  setPage(page) {
    this.setState({page})
  }

  render(){
    const
      { page } = this.state,
      { requestCount, responseCount, shiftsCount } = this.props,
      tabItems = [
        {
          title: 'Calendar',
          tabContent: <Calendar setPage={this.setPage}/>
        },
        {
          title: 'Shifts',
          badgeValue: shiftsCount,
          tabContent: <Shifts setPage={this.setPage}/>
        },
        {
          title: 'Clock In',
          tabContent: <ClockedIn setPage={this.setPage}/>
        },
        {
          title: 'Responses',
          badgeValue: responseCount,
          tabContent: <Responses setPage={this.setPage}/>
        },
        {
          title: 'Requests',
          badgeValue: requestCount,
          tabContent: <Requests setPage={this.setPage}/>
        },
      ]
    return(
      <TabContainer currentPage={page} setPage={this.setPage} tabItems={tabItems}/>
    )
  }
}

const
  mapStateToProps = (state, ownProps) => {
    const
      {shiftManagerResponses, shiftManagerRequests, shifts} = state.shifts
    return {
      requestCount: shiftManagerRequests && shiftManagerRequests.length,
      responseCount: shiftManagerResponses && shiftManagerResponses.length,
      shiftsCount: shifts && shifts.length
    };
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...shiftActions
        }, dispatch)
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(ShiftsManager)
