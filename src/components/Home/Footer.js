import React from 'react';

const Footer = (props) => (
    <footer className="app-footer">
        <a href="http://open-market-europe.com">Team </a>
        &copy; Open Market Europe LTD.
        <span className="float-right">Made in London</span>
    </footer>
)

export default Footer;
