import React from 'react'
import { connect } from 'react-redux'
import {Field, reduxForm, formValueSelector } from 'redux-form'
import ListSelectorGroup from '../../Commons/ListSelectorGroup'
import FormGroup from '../../Commons/FormGroup'
import SelectRedux from '../../Commons/SelectRedux'
import SwitchFormRight from '../../Commons/SwitchFormRight'
import DatePickerGroup from '../../Commons/DatePickerGroup'
import TimeZones from '../../../config/data/TimeZones'
import NumberFormats from '../../../config/data/NumberFormats'
import {FormatDates} from '../../../config/data/Date'
import Languages from '../../../config/data/Languages'
import {required} from '../../Commons/Validators'

const isDateRequired = (value, allValues) => {
  const {
    deligateLineManager,
    deligateleaveManger,
    deligateExpenseManager
  } = allValues,
  showLeaveDates = deligateLineManager || deligateleaveManger || deligateExpenseManager
  return showLeaveDates ? required(value) : undefined
}

let SettingsForm = ({
  handleSubmit,
  submitting,
  invalid,
  managersList,
  handleDelegateLineManager,
  handleDelegateLeaveManager,
  handleDelegateExpenseManager,
  selectedDelegateLineManager,
  selectedDelegateLeaveManager,
  selectedDelegateExpenseManager,
  toggleModal,
  showLeaveDates,
  history
}) => (
  <form className='container form-pb' onSubmit={handleSubmit}>
      <div className='row'>
          <div className='offset-sm-2 col-lg-8'>
            <h5 className='mb-2'>User Settings</h5>
            <div className='row'>
              <Field className='col-md-6' name='dateFormat' type='text' options={FormatDates} component={SelectRedux} label='Default Date Format' />
              <Field className='col-md-6' name='language' type='text' options={Languages} component={SelectRedux} label='Default Language'/>
            </div>
            <div className='row'>
              <Field className='col-md-6' name='timeZone' type='text' options={TimeZones} component={SelectRedux} label='Default Time Zone' />
              <Field className='col-md-6' name='numberFormat' options={NumberFormats} type='text' component={SelectRedux} label='Default Number Format' />
            </div>
            <hr />
            <h5 className='mb-2'>Managers</h5>
            <div className='row'>
                <Field className='col-md-6' name='lineManager' disabled={true} type='text' component={FormGroup} label='Line Manager'/>
                <Field className='col-md-6' name='leaveManager' disabled={true} type='text' component={FormGroup} label='Leave Reports To'/>
            </div>
            <div className='row'>
                <Field className='col-md-6' name='expenseManager' disabled={true} type='text' component={FormGroup} label='Expenses Reports To'/>
            </div>
            <hr />
            <h5>Settings Feed</h5>
            <div className='row'>
                <Field name='allowedToViewteamfeeds' component={SwitchFormRight}  message='Allowed To Publish Leaves In Feeds'/>
            </div>
            <hr />
            <h5 className='mb-2'>Forward My Authorization Responsibilities</h5>
            <div className='row'>
                <Field className='col-md-6' name='deligateLineManager' type='text' component={ListSelectorGroup}
                  list={managersList} label='Forward Line Manager Authorization Request'
                  onAnswered={handleDelegateLineManager} values={[selectedDelegateLineManager]}/>
                <Field className='col-md-6' name='deligateleaveManger' type='text' component={ListSelectorGroup}
                  list={managersList} label='Forward Leave Manager Authorization Request'
                  onAnswered={handleDelegateLeaveManager} values={[selectedDelegateLeaveManager]}/>
            </div>
            <div className='row'>
                <Field className='col-md-6' name='deligateExpenseManager' type='text' component={ListSelectorGroup}
                  list={managersList} label='Forward Expense Manager Authorization Request'
                  onAnswered={handleDelegateExpenseManager} values={[selectedDelegateExpenseManager]}/>
            </div>
            <div className='row'>
              <Field className='col-md-6' name='leaveStartDate' disabled={!showLeaveDates}
                component={DatePickerGroup} label='From' validate={isDateRequired} required={showLeaveDates}/>
              <Field className='col-md-6' name='leaveEndDate' disabled={!showLeaveDates}
                component={DatePickerGroup} label='To' validate={isDateRequired} required={showLeaveDates}/>
            </div>
            <hr />
            <h5>Password</h5>
            <div className='row'>
                <div className='col-sm-6'>
                    <button className='btn btn-outline-primary btn-block' type='button' onClick={() => toggleModal()}>
                        <i className='fa fa-key'></i>
                        Change Password
                    </button>
                </div>
            </div>

            <div className='row mt-2'>
                <div className='col-sm-12'>
                  <div className='float-right'>
                    <button className='btn btn-secondary' type='button' style={{margin: '20px 10px', width: '120px'}} onClick={() => history.push('/dashboard')}>
                        Cancel
                    </button>
                    <button className='btn btn-primary' type='submit' disabled={submitting || invalid} style={{margin: '20px 10px', width: '120px'}}>
                        Save
                    </button>
                    </div>
                </div>
            </div>
          </div>
      </div>
  </form>
)

const selector = formValueSelector('userProfile')

SettingsForm = connect(
  state => {
    const {
      deligateLineManager,
      deligateleaveManger,
      deligateExpenseManager
    } = selector(state, 'deligateLineManager', 'deligateleaveManger', 'deligateExpenseManager')

    return {
      showLeaveDates: deligateLineManager && deligateleaveManger && deligateExpenseManager
    }
  }
)(SettingsForm)

export default reduxForm({
  form: 'userProfile',
  enableReinitialize: true
})(SettingsForm)
