import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import { change } from 'redux-form'
import * as selectors from '../../../selectors/listSelectors'
import * as actions from '../../../actions/Users'
import ProfileForm from './ProfileSettingsForm'
import ModalChangePwdForm from '../../Commons/ModalChangePwdForm'
import { changePassword } from '../../../actions'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom'
import autoBind from 'react-autobind'
import _ from 'lodash'
import moment from 'moment'
import {Modal, ModalHeader} from 'reactstrap'

class ProfileSetup extends Component {

  constructor(props) {
    super(props)
    this.state = {
      userId: this.props.userId,
      users: this.props.users,
      isOpen: false
    }
    autoBind(this)
  }

  componentWillMount() {
    const { actions: {
      fetchManagersList,
      fetchUsersList,
      getUserSettings
    }} = this.props;
    getUserSettings(this._handleSelectedValues)
    fetchUsersList()
    fetchManagersList()
  }

  componentWillReceiveProps(nextProps) {
    const {userId, users} = this.props
    nextProps.users !== users && this.setState({users: nextProps.users})
    nextProps.userId !== userId && this.setState({userId: nextProps.userId})
  }

  _onSubmit(values) {
    const
      { deligateLineManager,
        deligateleaveManger,
        deligateExpenseManager
      } = this.state,
      { updateUserSetting } = this.props.actions,
      payload = {
        dateFormat: values.dateFormat || null,
        timeZone: values.timeZone || null,
        numberFormat: values.numberFormat || null,
        language: values.language || null,
        leaveStartDate: (values.leaveStartDate && moment(values.leaveStartDate).format('DD/MM/YYYY')) || null,
        leaveEndDate: (values.leaveEndDate && moment(values.leaveEndDate).format('DD/MM/YYYY')) || null,
        lineManagerTo: deligateLineManager,
        leaveManagerTo: deligateleaveManger,
        expenseManagerTo: deligateExpenseManager,
        allowedToViewteamfeeds: values.allowedToViewteamfeeds || false
      }
      updateUserSetting(payload, this._submitSuccess);
  }

  _submitSuccess() {
    this.props.history.push('/')
  }

  _handleSelectedValues(user) {
    const
      lineManager = user && user.lineManager,
      leaveManager = user && user.leaveManager,
      expenseManager = user && user.expenseManager,
      deligateLineManager = user && user.deligateLineManager,
      deligateleaveManger = user && user.deligateleaveManger,
      deligateExpenseManager = user && user.deligateExpenseManager,

    initialValues = {
      ...user,
      lineManager: lineManager.Name,
      leaveManager: leaveManager.Name,
      expenseManager: expenseManager.Name,
      deligateLineManager: deligateLineManager.Name,
      deligateleaveManger: deligateleaveManger.Name,
      deligateExpenseManager: deligateExpenseManager.Name,
      leaveStartDate: user.leaveStartDate && moment(user.leaveStartDate, 'DD/MM/YYYY').format('MM/DD/YYYY'),
      leaveEndDate: user.leaveEndDate && moment(user.leaveEndDate, 'DD/MM/YYYY').format('MM/DD/YYYY'),
    }

    this.setState({
      lineManager,
      leaveManager,
      expenseManager,
      deligateLineManager: deligateLineManager.Id,
      deligateleaveManger: deligateleaveManger.Id,
      deligateExpenseManager: deligateExpenseManager.Id,
      initialValues
    })
  }

  _handleFieldChange(value, field) {
    const { changeFieldValue } = this.props.actions
    this.setState({
      [field]: `${value[0].id}`,
    }, () => changeFieldValue(field, value[0].displayName))
  }

  changePassword(values)  {
    const { oldPassword, newPassword, confirmPassword} = values
    if(newPassword !== confirmPassword) this.setState({errorMessage: 'Password does not match'})
    else this.props.actions.changePassword({oldPassword, newPassword}, this.handleChangePassword, this.handleFailure)
  }

  handleFailure(submitMessage) {
    this.setState({submitMessage})
  }

  handleChangePassword() {
    this.setState({submitMessage: undefined}, () => this.toggleChangePwd())
  }

  toggleChangePwd() {
    this.setState({isOpen: !this.state.isOpen})
  }

  render() {
    const
      { managersList, usersList } = this.props,
      {
        deligateLineManager,
        deligateleaveManger,
        deligateExpenseManager,
        initialValues,
        isOpen
      } = this.state,
      handleDelegateLineManager = (managerValue) => this._handleFieldChange(managerValue, 'deligateLineManager'),
      handleDelegateLeaveManager = (managerValue) => this._handleFieldChange(managerValue, 'deligateleaveManger'),
      handleDelegateExpenseManager = (managerValue) => this._handleFieldChange(managerValue, 'deligateExpenseManager')

    return (
        <div>
          <div className='container-fluid mt-2'>
            <div className='animated fadeIn'>
              <div className='card card-default'>
                <div className='card-block'>
                  <ProfileForm
                    initialValues={initialValues}
                    onSubmit={this._onSubmit}
                    managersList={managersList}
                    usersList={usersList}
                    handleDelegateLineManager={handleDelegateLineManager}
                    handleDelegateLeaveManager={handleDelegateLeaveManager}
                    handleDelegateExpenseManager={handleDelegateExpenseManager}
                    selectedDelegateLineManager={{id: deligateLineManager}}
                    selectedDelegateLeaveManager={{id: deligateleaveManger}}
                    selectedDelegateExpenseManager={{id: deligateExpenseManager}}
                    toggleModal={this.toggleChangePwd}
                    history={this.props.history}
                  />
                </div>
              </div>
            </div>
          </div>
          <Modal isOpen={isOpen} toggle={this.toggleChangePwd}>
            <ModalHeader toggle={this.toggleChangePwd}>Change Password</ModalHeader>
              <ModalChangePwdForm
                onSubmit={this.changePassword} toggle={this.toggleChangePwd}  {...this.state}/>
          </Modal>
        </div>
    )
  }
}

const
  mapStateToProps = (state) => {
    const { allManagersListSelector, shiftUsersListSelector } = selectors,
      users = state.users,
      user = state.session.user,
      userId = user && user.id,
      managersList = allManagersListSelector(state),
      usersList = shiftUsersListSelector(state)

    return {
      managersList,
      usersList,
      userId,
      users
    }
  },

  mapDispatchToProps = (dispatch) => {
    const
      changeFieldValue = (field, value) => change('userProfile', field, value)
    return {
      actions: bindActionCreators({
        ...actions,
        changeFieldValue,
        changePassword
      }, dispatch)
    }
  }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProfileSetup))
