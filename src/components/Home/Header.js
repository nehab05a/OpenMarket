import React from 'react'
import PropTypes from 'prop-types'
import {Dropdown, DropdownMenu, DropdownItem} from 'reactstrap'
import {Link, withRouter} from 'react-router-dom'
import {logout} from '../../actions'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux';
import _ from 'lodash'

const
  sidebarToggle = (e)=>{
      e.preventDefault();
      document.body.classList.toggle('sidebar-hidden');
  },

  mobileSidebarToggle = (e) =>{
      e.preventDefault();
      document.body.classList.toggle('sidebar-mobile-show');
  },

  Header = (props) =>{
    const
      _styles = {
        userIcon: {
          color: '#7F8FA4',
          fontSize: '25px',
          margin: '0px 10px'
        }
      },
      { logout, history, isAdmin, profilePictureUrl } = props,
      { userPhoto, alternateText } = profilePictureUrl,
      Breadcrumbs=props.breadcrumbs

    return(
      <header className='app-header navbar'>
          <button className='navbar-toggler mobile-sidebar-toggler hidden-lg-up' onClick={mobileSidebarToggle} type='button'>
              <i className='fa fa-bars'></i>
          </button>
          <Link to='/dashboard' className='navbar-brand'>
              <img src='/img/logo.png' alt='Logo' className='hidden-md-down img-fluid'></img>
          </Link>
          <ul className='nav navbar-nav hidden-md-down'>
              <li className='nav-item'>
                  <a className='nav-link navbar-toggler sidebar-toggler' onClick={sidebarToggle} href='#'>
                      <i className='fa fa-bars'></i>
                  </a>
              </li>
          </ul>
          <Breadcrumbs menu={props.menu}></Breadcrumbs>
          <ul className='nav navbar-nav ml-auto navbar-nav-right'>
              <li className='nav-item'>
                  <Dropdown isOpen={props.toogleDDHeader} toggle={props.toggle}>
                      <a onClick={props.toggle} className='nav-link dropdown-toggle nav-link' data-toggle='dropdown' href='#' role='button' aria-haspopup='true' aria-expanded={props.toogleDDHeader}>
                        <span style={_styles.userIcon} >
                          {profilePictureUrl ?
                            <img src={userPhoto} alt={alternateText} width='35' height='35' style={{borderRadius: '50%'}}></img>
                          : <i className='fa fa-user-circle-o'></i>}
                        </span>
                      </a>

                      <DropdownMenu className='dropdown-menu-right'>
                          <DropdownItem onClick={() => history.push('/profile')}>
                              <i className='fa fa-user'></i>
                              Edit Profile</DropdownItem>
                          {
                            isAdmin &&
                            <DropdownItem onClick={() => history.push('/company/getstart')}>
                                  <i className='fa fa-briefcase'></i>
                                  Company Settings
                            </DropdownItem>
                          }
                            <DropdownItem onClick={() =>logout(history)}>
                                <i className='fa fa-lock'></i>
                                  Logout
                            </DropdownItem>

                      </DropdownMenu>
                  </Dropdown>
              </li>
          </ul>
      </header>
    )
}

const
  mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
      logout: logout,
      toggle: () => ({
        type: 'TOOGLE_DD_HEADER'
      })
    }, dispatch)
  },
  mapStateToProps = (state) => {
    return {
      toogleDDHeader: state.dropdowns.toogleDDHeader
    };
  },
  { func, bool } = PropTypes;

Header.propTypes = {
  logout: func.isRequired,
  toogleDDHeader: bool.isRequired
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
