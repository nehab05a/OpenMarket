import React from 'react';
import Header from './Header';
import Sidebar from './Sidebar';
import Footer from './Footer'
import { connect } from 'react-redux'
import _ from 'lodash'

const Home = (props) => {
  const
    { user } = props,
    { userPhoto, firstName, lastName } = user,
    alternateText = firstName && lastName && `${_.capitalize(firstName.charAt(0))}${_.capitalize(lastName.charAt(0))}`,
    isShiftManager = user.roles && _.includes(user.roles, 'ROLE_CREATE_SHIFT', 'ROLE_MODULE_SHIFT_MANAGER'),
    isAdmin = user.roles && _.includes(user.roles, 'ROLE_ORG_ADMIN', 'ROLE_ADMIN')
  if(isShiftManager === false) _.remove(props.menu, (item) => item.path === '/shiftsmanager')

  return (
    <div className="app">
        <Header menu={props.menu} breadcrumbs={props.breadcrumbs} isAdmin={isAdmin} profilePictureUrl={{userPhoto, alternateText}}/>
        <div className="app-body">
            <Sidebar menu={props.menu}/>
            <main className="main" style={{overflowX: 'scroll'}}>
                 {props.children}
            </main>
        </div>
        <Footer/>
    </div>
  )
},

mapStateToProps = (state) => {
  return{
    user: state.session.user
  }
}


export default connect(mapStateToProps, null)(Home);
