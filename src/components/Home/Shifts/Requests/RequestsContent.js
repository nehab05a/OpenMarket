import React, { Component } from 'react'
import SearchInput, {createFilter} from 'react-search-input'
import { filter, isEmpty, capitalize } from 'lodash'
import autoBind from 'react-autobind'
import moment from 'moment'
import Filter from '../Filter'

class ShiftsCard extends Component {
    constructor(props) {
      super(props);

      this.state = {
          searchTerm : '',
          showDetails : false,
      }
      autoBind(this);
    }

    render() {
      const
        _styles = {
          searchBox: {
            width: '130px',
            marginRight: '8px',
          },
          filterLink: {
            fontSize : '25px',
            color : '#b0bec5'
          },
          tools: {
            padding: '10px 15px',
            borderBottom: '1px solid #cfd8dc'
          },
          listContainer: {
            padding: '10px 15px',
            borderBottom: '1px solid #cfd8dc'
          },
          decline: {
            flex:1,
            textAlign: 'center',
            padding:'8px',
            color: 'crimson',
            width: '100%',
            borderRight: '1px solid #cfd8dc',
            fontWeight: '500',
            cursor: 'pointer'
          },
          accept:{
            flex:1,
            textAlign:'center',
            padding:'8px',
            color: 'darkgreen',
            fontWeight: '500',
            width: '100%',
            cursor: 'pointer'
          },
          emptyContent: {
            textAlign: 'center',
            width: '100%',
            height: '700px',
          },
          noMatch: {
            width: '100%',
            color:'#20a8d8',
            paddingTop: '40px',
            textAlign:'center'
          },
        },
        { list, applyFilter } = this.props,
        { searchTerm } = this.state,
        filteredItems = filter(list, createFilter(searchTerm, ['assigned_shift_name', 'user_fullname']));

        return (
          <div>
            <div className='d-flex justify-content-between align-items-center' style={_styles.tools}>
              <h5 style={{fontSize: '15px'}}>Shifts</h5>
              <div className='d-flex align-items-center'>
                <SearchInput className='list-search' style={_styles.searchBox}  onChange={this._searchUpdated} />
                <Filter applyFilter={applyFilter} />
              </div>
            </div>
            <div style={{overflow: 'auto', height:'620px'}} id='list-scrollbar'>
              {
                isEmpty(filteredItems) ?
                <div style={_styles.noMatch}>No match found</div>
              : filteredItems.map((item, index) => (
                  <div key={index}>
                    <div style={_styles.listContainer} className='d-flex flex-column'>
                      <div className='d-flex flex-column' style={{flex: '7'}}>
                        <div className='d-flex justify-content-between'>
                          <span style={{color: '#20a8d8',fontWeight: 500}}>
                            {`${index+1}. ${capitalize(item.trade_shift === 'true' ? item.user_fullname : item.assigned_shift_name)}`}
                          </span>
                          {item.trade_shift === 'true' && <span style={{color: 'crimson', fontWeight: 500}}>Trade Shift</span>}
                        </div>
                        <span style={{color: 'black', fontWeight: 500}}>{item.shift_type_name}</span>
                        <span style={{fontSize: '13px'}}>{item.branch_location}</span>
                        <span className='d-flex justify-content-between'>
                          <span style={{color: 'green'}}>{moment(item.assigned_date, "DD/MM/YYYY").format('Do MMM YYYY')}</span>
                          <span>{`${item.assigned_shift_start_time} - ${item.assigned_shift_end_time}`}</span>
                        </span>
                      </div>
                      {item.trade_shift === 'true' &&
                        <div className='d-flex flex-column'>
                          <span style={{color: '#20a8d8',fontWeight: 500,marginTop: '5px'}}>{capitalize(item.trade_shift_name || item.accepted_user)}</span>
                          <span className='d-flex justify-content-between'>
                            <span style={{color: 'green'}}>{moment(item.request_trade_shift_date, "DD/MM/YYYY").format('Do MMM YYYY')}</span>
                            <span>{`${item.request_shift_start_time} - ${item.request_shift_end_time}`}</span>
                          </span>
                        </div>
                      }
                    </div>
                    <div className='d-flex justify-content-center' style={{ borderBottom: '1px solid #cfd8dc'}}>
                      <span style={_styles.decline}
                        color='secondary' onClick={() => this._declineShift(item)}>Decline</span>
                      <span style={_styles.accept}
                        color='secondary' onClick={() => this._acceptShift(item)}>{item.trade_shift === 'true' ? 'Trade' : 'Accept'}</span>
                    </div>
                  </div>
                ))
              }
            </div>
          </div>
        )
    }

    _acceptShift(item) {
      const
        key = item.trade_shift === 'true' ? 'request_trade_shift_id' : 'user_id',
        data = [{
          is_aprrove: 'true',
          assignedShift_id: item.assigned_id,
          [key]: item.trade_shift === 'true' ? item.request_trade_shift_id : item.shift_assigned_user_id
        }],
        payload = {
          data
        }
      this.props.acceptShift(payload)
    }

    _declineShift(item) {
      const
        data = {
          declinedShiftFor: item.shift_time_id,
          shiftAssignedUser: item.assigned_id,
          declinedLevel : item.request_level,
        }
      this.props.declineShift(data)
    }

    _searchUpdated (term) {
      this.setState({searchTerm: term})
    }

    _toggleFilter() {
      this.setState({showFilter: !this.state.showFilter})
    }

}

export default ShiftsCard;
