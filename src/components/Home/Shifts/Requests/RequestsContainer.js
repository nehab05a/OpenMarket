import React, { Component } from 'react';
import RequestsContent from './RequestsContent';
import _ from 'lodash';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import shiftActions from '../../../../actions/ShiftsActions'
import {isEqual, chain, includes, isEmpty} from 'lodash'

class RequestsContainer extends Component {
    constructor(props) {
     super(props)
      this.state = {
        addViewOne : false,
        addViewTwo : false,
        shiftsRequestList: this.props.shiftsRequestList || []
      }
      autoBind(this);
    }

    componentWillMount() {
      this.props.actions.getUserShiftsRequests()
    }

    componentWillReceiveProps(nextProps) {
      !isEqual(this.props.shiftsRequestList, nextProps.shiftsRequestList) && this.setState({shiftsRequestList: nextProps.shiftsRequestList})
    }

    render() {
      const
        _styles = {
          cards : {
            flex:1,
            marginRight:'20px',
            minHeight: '700px',
          },
          placeholder: {
            textAlign: 'center',
            width: '100%',
            minHeight: '700px',
          },
        },
        { addViewOne, addViewTwo, shiftsRequestList } = this.state

      return (
        <div>
          <div className='d-flex justify-content-between'>
            <div className='card card-default'style={_styles.cards}>
              <RequestsContent
                list={shiftsRequestList}
                acceptShift={this._acceptShift}
                declineShift={this._declineShift}
                applyFilter={this._applyFilter}
              />
            </div>
            <div className='card card-default' style={_styles.cards}>
            { addViewOne ?
                <RequestsContent
                  list={shiftsRequestList}
                  acceptShift={this._acceptShift}
                  declineShift={this._declineShift}
                  applyFilter={this._applyFilter}
                />
              : <div style={_styles.placeholder} onClick={() => this.setState({addViewOne: true})}
                className='text-muted d-flex flex-column justify-content-center align-items-center'>
                  <i className='fa fa-calendar' style={{fontSize: '25px'}} />
                  <span>Add Shift To View</span>
                </div>
            }
            </div>
            <div className='card card-default' style={_styles.cards}>
            { addViewTwo ?
                <RequestsContent
                  list={shiftsRequestList}
                  acceptShift={this._acceptShift}
                  declineShift={this._declineShift}
                  applyFilter={this._applyFilter}
                />
              : <div style={_styles.placeholder} onClick={() => this.setState({addViewTwo: true})}
                className='text-muted d-flex flex-column justify-content-center align-items-center'>
                  <i className='fa fa-calendar' style={{fontSize: '25px'}} />
                  <span>Add Shift To View</span>
                </div>
            }
            </div>
          </div>
        </div>
      )
    }

    _acceptShift(values) {
      const
        { normalShiftAccept, getUserShiftsRequests } = this.props.actions
      normalShiftAccept(values, getUserShiftsRequests)
    }

    _declineShift(values) {
      const
        { normalShiftDecline, getUserShiftsRequests } = this.props.actions
      normalShiftDecline(values, getUserShiftsRequests)
    }

    _applyFilter(filters) {
      const
        { shiftsRequestList } = this.props,
        { branchFilters, shiftTypeFilters } = filters,
        filteredList = chain(shiftsRequestList)
                      .filter(item => !isEmpty(branchFilters) ? includes(branchFilters.map(branch => Number(branch.id)), item.branch_id) : item)
                      .filter(item => !isEmpty(shiftTypeFilters) ? includes(shiftTypeFilters.map(shiftType => Number(shiftType.id)), item.shift_type_id) : item)
                      .value()

        this.setState({shiftsRequestList: filteredList})
    }
}

const
  mapStateToProps = (state) => {
    return {
      shiftsRequestList: state.shifts.userShiftRequests
    };
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...shiftActions,
        }, dispatch)
    };
  }

export default connect(mapStateToProps, mapDispatchToProps)(RequestsContainer);
