import React, {Component} from 'react'
import { Modal, ModalBody, ModalHeader, Button } from 'reactstrap'
import autobind from 'react-autobind'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import shiftActions from '../../../actions/ShiftsActions'
import DatePickerGroup from '../../../components/Commons/DatePickerGroup'
import moment from 'moment'

class TradeShiftModal extends Component {
    constructor(props) {
      super(props)
      this.state = {
        showModal : false,
        showWeekdays : false,
        showSpecificShift : false
      }
      autobind(this)
    }

    componentWillMount() {
      const { branch_id, actions: { getCalendarForTrade } } = this.props
      getCalendarForTrade({
        viewType : 'year',
        offset : 0,
        branch_id,
      }, this._getIncludedDates)
    }

    render() {
      const
        _styles = {
          button : {
            padding: '10px',
            fontWeight: 500,
            borderBottom: '1px solid #cfd8dc'
          },
          weekday : {
            width: '100%',
            textAlign: 'center',
            fontSize: '20px',
            color: '#20a8d8',
            cursor: 'pointer'
          },
          listContainer: {
            padding: '5px 10px 5px 0px',
            borderBottom: '1px solid #cfd8dc'
          }
        },
        { showWeekdays, showSpecificShift, includeDates, tradeShifts, isSpecificShift } = this.state,
        { isModalOpen, toggleModal } = this.props,
        weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        availableShifts = tradeShifts && tradeShifts.filter(tradeShift => tradeShift.shift_accepted !== 'true'),
        modalHeader = isSpecificShift ? 'Select shift to trade with' : 'Trade Shift',
        modalBody = isSpecificShift ? (
          <div style={{height: '400px'}}>
          {availableShifts.length > 0 ? availableShifts.map((item, index) => {
            const bandColor = item.shift_requested === 'false' ?
                          item.shift_accepted === 'true' ? '#20a8d8' : '#dfe3e9'
                        : item.shift_accepted === 'true' ?  '#20a8d8' : '#fdc83f',
                  statusBandStyle = {
                    width: '6px',
                    height: '45px',
                    background: bandColor,
                    marginRight: '10px'
                  }
            return (
            <div key={`shift - ${index}`} style={_styles.listContainer} className='d-flex' onClick={() => this._tradeShift({
                trade_shift_type: 'Specific shift',
                requested_trade_shift: item.shift_id
              })}>
              <span style={statusBandStyle}></span>
              <div className='d-flex flex-column'>
                <span className='d-flex justify-content-between'>
                  <span style={{fontWeight: 500}}>{item.shift_name}</span>
                  <span>
                    <span style={{marginRight: '10px'}}>{item.staff_type_name}</span>
                    <span>{item.branch_name}</span>
                  </span>
                </span>
                <span className='d-flex justify-content-between'>
                  <span>{`${item.shift_start_time} - ${item.shift_end_time}`}</span>
                  <span style={{fontWeight: 500}}>{moment(item.shift_date, 'DD/MM/YYYY').format('DD MMM')}</span>
                </span>
              </div>
            </div>
          )})
          : <div>No Available Shifts To Trade</div>
          }
          </div>
        ) : (
          <div>
            <div>
              <div style={_styles.button} onClick={() => this._tradeShift({
                trade_shift_type: 'Any day'
              })}>Any day</div>
              <div style={_styles.button} onClick={this._toggleWeekdaySelection}>Specific week day</div>
              {showWeekdays &&
                <div className='d-flex flex-column align-items-center justify-content-center'>
                {weekdays.map((weekday, index) => (
                  <div key={index} style={_styles.weekday} onClick={() => this._tradeShift({
                      trade_shift_type: 'Any weekday',
                      weekday: `${index+1}`
                    })}>{weekday}</div>
                  ))
                }
                </div>
              }
              <div style={_styles.button} onClick={this._toggleSpecificDaySelection}>Specific dates/shifts</div>
              {showSpecificShift &&
                <div className='d-flex justify-content-center'>
                <DatePickerGroup
                  onAnswered={this._getShiftsForDay}
                  placeholder='Select date'
                  minDate={moment()}
                  includeDates={includeDates}
                />
                </div>
              }
              <div style={_styles.button} onClick={this._cancelShift}>I can't work this shift</div>
            </div>
            <div className='d-flex justify-content-center' style={{margin: '8px'}}>
              <Button className='col-md-8' onClick={toggleModal}>Cancel</Button>
            </div>
          </div>
        )

      return (
        <div>
          <Modal isOpen={isModalOpen}  style={{width: isSpecificShift ? '350px': null}}>
            <ModalHeader toggle={toggleModal}>{modalHeader}</ModalHeader>
            <ModalBody>{modalBody}</ModalBody>
          </Modal>
        </div>
      )
    }

    _tradeShift(values) {
      const
        { toggleModal, refreshView, shifttime_id, actions: { tradeShift } } = this.props,
        data = {
          ...values,
          shifttime_id,
        }
      tradeShift(data, () => {
        toggleModal()
        refreshView()
      })
    }

    _cancelShift() {
      const { toggleModal, shifttime_id, actions: { cancelShift } } = this.props
      cancelShift(shifttime_id, toggleModal)
    }

    _getIncludedDates(calendarData) {
      const
        includeDates = calendarData.map(shiftRecord => moment(moment(shiftRecord.shift_date, 'DD/MM/YYYY').format('MM/DD/YYYY')))
      this.setState({includeDates})
    }

    _getShiftsForDay(day) {
      const
        shift_date = moment(day).format('DD/MM/YYYY'),
        { branch_id } = this.props,
        data = {
          shift_date,
          branch_id
        }
        this.props.actions.getTradeShifts(data, this._setTradeShifts )
    }

    _setTradeShifts(tradeShifts) {
      this.setState({
        isSpecificShift: true,
        tradeShifts
      })
    }

    _toggleWeekdaySelection() {
      this.setState({showWeekdays: !this.state.showWeekdays})
    }

    _toggleSpecificDaySelection() {
      this.setState({showSpecificShift: !this.state.showSpecificShift})
    }
}

const
  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...shiftActions,
        }, dispatch)
    };
  }

export default connect(null,mapDispatchToProps)(TradeShiftModal)
