import React,{Component} from 'react'
import ShiftCalendar from './Calendar'
import autobind from 'react-autobind'
import RequestsContainer from './Requests'
import MyShifts from './MyShifts'
import ShiftsClockedInContainer from './ClockedIn'
import TabContainer from './TabContainer'
import shiftActions from '../../../actions/ShiftsActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class Shifts extends Component{
  constructor(props) {
    super(props)
    this.state = {
      page: 0,
    }
    autobind(this)
  }

  componentWillMount() {
    const { getUserShifts, getUserShiftsRequests } = this.props.actions
    getUserShifts()
    getUserShiftsRequests()
  }

  setPage(page) {
    this.setState({page})
  }

  render(){
    const
      { page } = this.state,
      { requestCount, shiftsCount } = this.props,
      tabItems = [
        {
          title: 'Calendar',
          tabContent: <ShiftCalendar />
        },
        {
          title: 'Shifts',
          badgeValue: shiftsCount,
          tabContent: <MyShifts />
        },
        {
          title: 'Clock In',
          tabContent: <ShiftsClockedInContainer />
        },
        {
          title: 'Requests',
          badgeValue: requestCount,
          tabContent: <RequestsContainer />
        },
      ]
    return(
      <TabContainer currentPage={page} setPage={this.setPage} tabItems={tabItems}/>
    )
  }
}

const
  mapStateToProps = (state, ownProps) => {
    const
      {userShifts, userShiftRequests} = state.shifts
    return {
      requestCount: userShiftRequests && userShiftRequests.length,
      shiftsCount: userShifts && userShifts.length
    };
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...shiftActions
        }, dispatch)
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(Shifts)
