import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';
import moment from 'moment';

const ShiftLocation = ({ iconClass }) => <i className={iconClass} style={{fontSize: '35px', color: '#20a8d8'}}></i>;

class ShiftMap extends Component {
  static defaultProps = {
    zoom: 15
  };

  render() {
    const { shiftDetails } = this.props
    console.log(shiftDetails);
    return (
      <div style={{padding: '5px 10px'}}>
        <div style={{color: 'grey', lineHeight:'1.2'}}>
          <p className='d-flex flex-column'>
            <span style={{color: '#20a8d8', fontWeight: 500}}>{shiftDetails.shift_name}</span>
            <span style={{color: 'black', fontWeight: 500}}>{shiftDetails.shift_type_name}</span>
            <span className='d-flex justify-content-between'>
              <span style={{color: 'green'}}>{moment(shiftDetails.shift_date, "DD/MM/YYYY").format('Do MMM YYYY')}</span>
              <span>{`${shiftDetails.shift_start_time} - ${shiftDetails.shift_end_time}`}</span>
            </span>
            <span>Report To: {shiftDetails.managerList[0].user_fullname}</span>
          </p>
          <p className='d-flex flex-column' style={{fontSize: '12px', lineHeight:'1.4'}}>
            <span>Notes - {shiftDetails.note}</span>
            <span>Requirements - {shiftDetails.shift_requirement}</span>
          </p>
          <p className='d-flex flex-column'>
            <span style={{color: 'black', fontWeight: 500}}>Location</span>
            <span>{shiftDetails.branch_location}</span>
          </p>
        </div>
        <div className='map'>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: 'AIzaSyDLkbQy-AsAZBvioJBhpv39vQkDEViB3O8',
              language: 'en',
            }}
            defaultCenter={{ lat: Number(shiftDetails.branch_lati), lng: Number(shiftDetails.branch_lang)}}
            defaultZoom={this.props.zoom}
          >
            <ShiftLocation
              lat={Number(shiftDetails.branch_lati)}
              lng={Number(shiftDetails.branch_lang)}
              iconClass='fa fa-map-marker'/>
          </GoogleMapReact>
        </div>
        <div>
          <div className='d-flex justify-content-between' style={{color: 'black', fontWeight: 500}}>
            <span>Shift Workers</span>
            <span>{shiftDetails.userList.length} of {shiftDetails.shift_required_empCount}</span>
          </div>
          {shiftDetails.userList.map((worker, index) => (
            <div key={index} className='d-flex justify-content-between' style={{color: '#20a8d8',fontWeight: 400, lineHeight: 1.3, padding: '5px 0px'}}>
              <span>{worker.user_fullname}</span>
              <i className={worker.shift_accepted === 'true' ? 'fa fa-check-circle' : 'fa fa-question-circle'}
                style={{color: worker.shift_accepted === 'true' ? '#1dad74' : '#f2a81f'}}>
              </i>
            </div>
          ))}
          {shiftDetails.tradeShiftRequestedDate &&
            <span style={{fontSize: '12px', color: 'crimson'}}>
              Trade requested on {shiftDetails.tradeShiftRequestedDate}
            </span>
          }
        </div>
      </div>
    )
  }
}

export default ShiftMap
