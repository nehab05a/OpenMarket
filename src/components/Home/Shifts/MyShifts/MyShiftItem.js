import React, { Component } from 'react'
import autoBind from 'react-autobind'
import shiftActions from '../../../../actions/ShiftsActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from 'moment'
import GoogleMapReact from 'google-map-react';

const ShiftLocation = ({ iconClass }) => <i className={iconClass} style={{fontSize: '35px', color: '#20a8d8'}}></i>;

class ShiftContent extends Component {
    static defaultProps = {
      zoom: 15
    };

    constructor(props) {
      super(props);

      this.state = {
          showDetails : false,
      }
      autoBind(this);
    }

    render() {
      const
        { item, index } = this.props,
        _styles = {
          listContainer: {
            padding: '5px 10px',
            borderBottom: '1px solid #cfd8dc'
          },
          actionContainer: {
            padding: '8px',
            fontWeight: 600,
            background: '#20a8d8',
            color: 'white'
          }
        },
        { showDetails } = this.state
      return (
        <div>
          <div key={index} style={_styles.listContainer} className='d-flex' onClick={this._toggleShiftDetails}>
            <div className='d-flex flex-column' style={{flex: '10'}}>
              <span style={{color: '#20a8d8', fontWeight: 500}}>{`${index}. ${item.shift_name}`}</span>
              <span style={{color: 'black', fontWeight: 500}}>{item.staff_type}</span>
              <span>{item.branch_location}</span>
              <span className='d-flex justify-content-between'>
                <span style={{color: 'green'}}>{moment(item.shift_date, "DD/MM/YYYY").format('Do MMM YYYY')}</span>
                <span>{`${item.shift_start_time} - ${item.shift_end_time}`}</span>
              </span>
            </div>
          </div>
          {showDetails &&
            <div>
              <div className='map'>
                <GoogleMapReact
                  bootstrapURLKeys={{
                    key: 'AIzaSyDLkbQy-AsAZBvioJBhpv39vQkDEViB3O8',
                    language: 'en',
                  }}
                  defaultCenter={{ lat: Number(item.branch_lati), lng: Number(item.branch_lang)}}
                  defaultZoom={this.props.zoom}
                >
                  <ShiftLocation
                    lat={Number(item.branch_lati)}
                    lng={Number(item.branch_lang)}
                    iconClass='fa fa-map-marker'/>
                </GoogleMapReact>
              </div>
              <div className='d-flex align-items-center' style={_styles.actionContainer}>
                <i className='icon-clock' style={{fontSize: '25px', marginRight: '20px'}}/>
                <span style={{fontSize: '18px'}}>Clock In</span>
              </div>
            </div>
          }
        </div>
      )
    }

    _toggleShiftDetails() {
      this.setState({ showDetails: !this.state.showDetails })
    }

}

const
  mapDispatchToProps = (dispatch) => {
    const { getShiftsDetails, getShiftTimeDetails } = shiftActions
    return {
        actions: bindActionCreators( {
          getShiftsDetails,
          getShiftTimeDetails
        }, dispatch)
    };
  }

export default connect(null, mapDispatchToProps)(ShiftContent);
