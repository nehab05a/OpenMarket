import React, { Component } from 'react';
import ShiftsContent from './MyShiftsContent';
import _ from 'lodash';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import shiftActions from '../../../../actions/ShiftsActions'
import {isEqual, chain, includes, isEmpty} from 'lodash'

class MyShiftsContainer extends Component {
    constructor(props) {
      super(props);

      this.state = {
        addViewOne : false,
        addViewTwo : false,
        shiftsList: props.shiftsList
      }
      autoBind(this);
    }

    componentWillMount() {
      this.props.actions.getUserShifts()
    }

    componentWillReceiveProps(nextProps) {
      !isEqual(nextProps.shiftsList, this.state.shiftsList) && this.setState({shiftsList: nextProps.shiftsList})
    }

    render() {
      const
        _styles = {
          cards : {
            flex:1,
            marginRight:'20px',
            minHeight: '700px',
          },
          placeholder: {
            textAlign: 'center',
            width: '100%',
            minHeight: '700px',
          },
        },
        { addViewOne, addViewTwo, shiftsList } = this.state

      return (
        <div>
          <div className='d-flex justify-content-between'>
            <div className='card card-default'style={_styles.cards}>
              <ShiftsContent list={shiftsList} applyFilter={this._applyFilter}/>
            </div>
            <div className='card card-default' style={_styles.cards}>
            { addViewOne ?
                <ShiftsContent list={shiftsList} applyFilter={this._applyFilter}/>
              : <div style={_styles.placeholder} onClick={() => this.setState({addViewOne: true})}
                className='text-muted d-flex flex-column justify-content-center align-items-center'>
                  <i className='fa fa-calendar' style={{fontSize: '25px'}} />
                  <span>Add Shift To View</span>
                </div>
            }
            </div>
            <div className='card card-default' style={_styles.cards}>
            { addViewTwo ?
                <ShiftsContent list={shiftsList} applyFilter={this._applyFilter}/>
              : <div style={_styles.placeholder} onClick={() => this.setState({addViewTwo: true})}
                className='text-muted d-flex flex-column justify-content-center align-items-center'>
                  <i className='fa fa-calendar' style={{fontSize: '25px'}} />
                  <span>Add Shift To View</span>
                </div>
            }
            </div>
          </div>
        </div>
      )
    }

    _applyFilter(filters) {
      const
        { shiftsList } = this.props,
        { branchFilters, shiftTypeFilters } = filters,
        filteredList = chain(shiftsList)
                      .filter(item => !isEmpty(branchFilters) ? includes(branchFilters.map(branch => Number(branch.id)), item.branch_id) : item)
                      .filter(item => !isEmpty(shiftTypeFilters) ? includes(shiftTypeFilters.map(shiftType => shiftType.id), item.shiftTypeId) : item)
                      .value()

        this.setState({shiftsList: filteredList})
    }
}

const
  mapStateToProps = (state) => {
    return {
      shiftsList: state.shifts.userShifts
    };
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...shiftActions,
        }, dispatch)
    };
  }

export default connect(mapStateToProps, mapDispatchToProps)(MyShiftsContainer)
