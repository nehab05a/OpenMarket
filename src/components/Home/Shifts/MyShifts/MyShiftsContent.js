import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import SearchInput, { createFilter } from 'react-search-input'
import _, { filter, isEmpty } from 'lodash'
import autoBind from 'react-autobind'
import ShiftItem from './MyShiftItem'
import shiftActions from '../../../../actions/ShiftsActions'
import Filter from '../Filter'

class MyShiftsContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          searchTerm : '',
      }
      autoBind(this);
    }

    render() {
      const
        _styles = {
          searchBox: {
            width: '130px',
            marginRight: '8px',
          },
          filterLink: {
            fontSize : '25px',
            color : '#b0bec5'
          },
          tools: {
            padding: '10px 15px',
            borderBottom: '1px solid #cfd8dc'
          },
          emptyContent: {
            textAlign: 'center',
            width: '100%',
            height: '700px',
          },
          noMatch: {
            width: '100%',
            color:'#20a8d8',
            paddingTop: '40px',
            textAlign:'center'
          },
        },
        { list, applyFilter } = this.props,
        { searchTerm } = this.state,
        filteredItems = filter(list, createFilter(searchTerm, ['shift_name']));

      return (
        <div>
            <div className='d-flex justify-content-between align-items-center' style={_styles.tools}>
              <h5 style={{fontSize: '15px'}}>Shifts</h5>
              <div className='d-flex align-items-center'>
                <SearchInput className='list-search' style={_styles.searchBox}  onChange={this._searchUpdated} />
                <Filter applyFilter={applyFilter} />
              </div>
            </div>
            <div style={{overflow: 'auto', height:'620px'}} id='list-scrollbar'>
              {
                isEmpty(filteredItems) ?
                <div style={_styles.noMatch}>No match found</div>
              : filteredItems.map((item, index) => (
                  <ShiftItem key={index} item={item} index={index+1}/>
                ))
              }
            </div>
          </div>
      )
    }

    _searchUpdated (term) {
      this.setState({ searchTerm: term })
    }

    _toggleFilter() {
      this.setState({ showFilter: !this.state.showFilter })
    }

    _showShiftDetails(values) {
      const { getShiftsDetails } = this.props.actions
      values ? getShiftsDetails(values,
        (shiftDetails) => this.setState({
          showDetails: true,
          shiftDetails
        }))
      : this.setState({ showDetails: false })
    }
}

const
  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...shiftActions
        }, dispatch)
    };
  }

export default connect(null, mapDispatchToProps)(MyShiftsContent);
