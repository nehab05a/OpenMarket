import React, { Component } from 'react'
import autoBind from 'react-autobind'
import ShiftDetails from '../ShiftDetails'
import shiftActions from '../../../../actions/ShiftsActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from 'moment'
import { Button } from 'reactstrap'
import TradeShiftModal from '../TradeShiftModal'

class ShiftContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          showDetails : false,
          showShiftTrade : false
      }
      autoBind(this);
    }

    render() {
      const
        { item, index, refreshView } = this.props,
        { shift_accepted, shift_requested, isPermitted } = item,
        { showDetails, selectedRecord, showShiftTrade } = this.state,
        actionButtonText = shift_requested === 'false' ?
                      shift_accepted === 'true' ? 'Trade for Shift' : 'Request Shift'
                    : shift_accepted === 'true' ? 'Trade for Shift' : 'Request Pending',
        actionMethod = actionButtonText === 'Request Shift' ? this._requestForShift
                    : actionButtonText === 'Trade for Shift' ? this._toggleTradeShift
                    : null,
        bandColor = shift_requested === 'false' ?
                      shift_accepted === 'true' ? '#20a8d8' : '#dfe3e9'
                    : shift_accepted === 'true' ?  '#20a8d8' : '#fdc83f',
        _styles = {
          listContainer: {
            padding: '5px 10px 5px 0px',
            borderBottom: '1px solid #cfd8dc'
          },
          statusBandStyle: {
            width: '6px',
            height: '45px',
            background: bandColor,
            marginRight: '10px'
          },
          button: {
            margin: '2px 5px',
            width: '100%'
          }
        }

      return (
        <div>
          <div style={_styles.listContainer} className='d-flex' onClick={() => this._setSelectedItem(item)}>
            <span style={_styles.statusBandStyle}></span>
            <div className='d-flex flex-column' style={{flex: '10'}}>
              <span className='d-flex justify-content-between'>
                <span style={{fontWeight: 500}}>{`${index}. ${item.shift_name}`}</span>
                <span>
                  <span style={{marginRight: '10px'}}>{item.staff_type_name}</span>
                  <span>{item.branch_name}</span>
                </span>
              </span>
              <span className='d-flex justify-content-between'>
                <span>{`${item.shift_start_time} - ${item.shift_end_time}`}</span>
                <span style={{fontWeight: 500}}>{moment(item.shift_date, 'DD/MM/YYYY').format('DD MMM')}</span>
              </span>
            </div>
          </div>
          {showDetails && selectedRecord.shift_id === item.shift_id &&
            <div>
              <ShiftDetails
                shiftDetails={selectedRecord}
              />
              <div className='d-flex justify-content-around align-items-center flex-column' style={_styles.button}>
                <Button color='success' onClick={() => actionMethod(item.shift_id)}
                  style={_styles.button} disabled={actionButtonText === 'Request Pending' ? true
                  : actionButtonText === 'Trade for Shift' ? !isPermitted : undefined}>{actionButtonText}</Button>
                <Button color='secondary' onClick={this._toggleShiftDetails} style={_styles.button}>Cancel</Button>
              </div>
            </div>
          }
          {showShiftTrade &&
            <TradeShiftModal
              isModalOpen={showShiftTrade}
              toggleModal={this._toggleTradeShift}
              shifttime_id={item.shift_id}
              branch_id={item.branch_id}
              refreshView={refreshView}
            />
          }
        </div>
      )
    }

    _toggleShiftDetails() {
      this.setState({ showDetails: !this.state.showDetails })
    }

    _setSelectedItem(selectedRecord) {
      this.setState({
        selectedRecord
      }, this._toggleShiftDetails)
    }

    _requestForShift(shift_id) {
      this.props.actions.requestShift(shift_id, this._toggleShiftDetails)
    }

    _toggleTradeShift() {
      this.setState({ showShiftTrade: !this.state.showShiftTrade })
    }
}

const
  mapDispatchToProps = (dispatch) => {
    const { getShiftsDetails, getShiftTimeDetails, requestShift } = shiftActions
    return {
        actions: bindActionCreators( {
          getShiftsDetails,
          getShiftTimeDetails,
          requestShift
        }, dispatch)
    };
  }

export default connect(null, mapDispatchToProps)(ShiftContent);
