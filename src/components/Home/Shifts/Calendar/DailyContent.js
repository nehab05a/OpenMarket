import React, { Component } from 'react'
import SearchInput, {createFilter} from 'react-search-input'
import _, { filter, isEmpty } from 'lodash'
import autoBind from 'react-autobind'
import ShiftItem from './ShiftItem'

class ShiftContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          searchTerm : '',
      }
      autoBind(this);
    }

    render() {
      const
        _styles = {
          searchBox: {
            width: '130px',
            marginRight: '8px',
          },
          filterLink: {
            fontSize : '25px',
            color : '#b0bec5'
          },
          tools: {
            padding: '10px 15px',
            borderBottom: '1px solid #cfd8dc'
          },
          emptyContent: {
            textAlign: 'center',
            width: '100%',
            height: '700px',
          },
          noMatch: {
            width: '100%',
            color:'#20a8d8',
            paddingTop: '40px',
            textAlign:'center'
          },
        },
        { list, refreshView } = this.props,
        { searchTerm } = this.state,
        filteredItems = filter(list, createFilter(searchTerm, ['shift_name']));

      return (
        <div>
          { isEmpty(list) ?
              <div style={_styles.emptyContent}
                className='text-muted d-flex flex-column justify-content-center align-items-center'>
                <span>Select Day To View Shifts</span>
              </div>
            : <div>
              <div className='d-flex justify-content-between align-items-center' style={_styles.tools}>
                <h5 style={{fontSize: '15px'}}>Shifts</h5>
                <div className='d-flex align-items-center'>
                  <SearchInput className='list-search' style={_styles.searchBox}  onChange={this._searchUpdated} />
                </div>
              </div>
              <div style={{overflow: 'auto', height:'620px'}} id='list-scrollbar'>
                {
                  isEmpty(filteredItems) ?
                  <div style={_styles.noMatch}>No match found</div>
                : filteredItems.map((item, index) => (
                    <ShiftItem key={index} item={item} index={index+1} refreshView={refreshView}/>
                  ))
                }
              </div>
            </div>
          }
        </div>
      )
    }

    _searchUpdated (term) {
      this.setState({ searchTerm: term })
    }

    _toggleFilter() {
      this.setState({ showFilter: !this.state.showFilter })
    }

    _applyFilter(filterValues) {
      //Filter Records
      const
      { list } = this.props,
      { shiftFilters } = filterValues,

      filtered = (collection, key, filters) =>
        _.filter(collection, item => filters.length ? _.includes(filters, item[key]) : item);

      this.setState({
        items: filtered(list, 'shift_id', shiftFilters)
      })
    }
}

export default ShiftContent;
