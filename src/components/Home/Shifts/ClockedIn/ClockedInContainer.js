import React, { Component } from 'react';
import _ from 'lodash';
import autoBind from 'react-autobind';


class ShiftsClockedInContainer extends Component {
    constructor(props) {
     super(props)
      this.state = {
        addViewOne : false,
        addViewTwo : false,
      }
      autoBind(this);
    }

    render() {
      const
        _styles = {
          cards : {
            flex:1,
            marginRight:'20px',
            minHeight: '700px',
          },
          placeholder: {
            textAlign: 'center',
            width: '100%',
            minHeight: '700px',
          },
        },
        { addViewOne, addViewTwo } = this.state

      return (
        <div>
          <div className='d-flex justify-content-between'>
            <div className='card card-default'style={_styles.cards}>

            </div>
            <div className='card card-default' style={_styles.cards}>
            { addViewOne ?
                  <h3>Content</h3>
              : <div style={_styles.placeholder} onClick={() => this.setState({addViewOne: true})}
                className='text-muted d-flex flex-column justify-content-center align-items-center'>
                  <i className='fa fa-calendar' style={{fontSize: '25px'}} />
                  <span>Add Shift To View</span>
                </div>
            }
            </div>
            <div className='card card-default' style={_styles.cards}>
            { addViewTwo ?
                <h3>Content</h3>
              : <div style={_styles.placeholder} onClick={() => this.setState({addViewTwo: true})}
                className='text-muted d-flex flex-column justify-content-center align-items-center'>
                  <i className='fa fa-calendar' style={{fontSize: '25px'}} />
                  <span>Add Shift To View</span>
                </div>
            }
            </div>
          </div>
        </div>
      )
    }
}

export default ShiftsClockedInContainer;
