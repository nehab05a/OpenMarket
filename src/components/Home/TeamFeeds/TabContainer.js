import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

const Tabs = ({
  tabItems,
  setPage,
  currentPage
}) => {
  const styles = {
    badge : {
        display         : 'flex',
        position        : 'absolute',
        top             : 0,
        right           : 2,
        color           : 'crimson',
        fontWeight      : 500,
        borderRadius    : '50%',
        alignItems      : 'center',
        justifyContent  : 'center',
      },

      badgeIcon : {
        backgroundColor : 'transparent',
      },

      icon : {
        position        : 'relative',
        cursor          : 'pointer'
      },
  },
  currentContent = tabItems[currentPage].tabContent

  return (
  <div>
  <div className='breadcrumb wizard d-flex justify-content-between align-items-center'>
    <div style={{width: '100%'}}>
      { tabItems.map((item, index) => (
        <span key={index} style={styles.icon} onClick={() => setPage(index)}
          className={classnames('wizard-item', { 'active': currentPage === index })}>
          <span className='wizard-text'>
            {item.title}
          </span>
          <span style={styles.badge}>
            <span style={styles.badgeIcon}>{item.badgeValue || ''}</span>
          </span>
        </span>
      ))}
    </div>
    <div>

    </div>
  </div>
  <div className='container-fluid mt-2'>
    <div className='animated fadeIn'>
        <div>
          {currentContent}
        </div>
    </div>
  </div>
  </div>
)};

Tabs.propTypes = {
  tabItems: PropTypes.arrayOf(PropTypes.shape({
              title: PropTypes.string.isRequired,
              badgeValue: PropTypes.oneOfType([
                            PropTypes.string,
                            PropTypes.number,
                          ]),
              tabContent: PropTypes.element.isRequired,
           })).isRequired,
  setPage:  PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired
}

export default Tabs
