import React, { Component } from 'react';
import FeedsContent from './AllFeedsContent';
import _ from 'lodash';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import feedsActions from '../../../../actions/TeamFeeds'
import {isEqual} from 'lodash'
import ChatContent from './ChatContent'
import Wampy from '../../../../lib/wampy/wampy'

const ws = new Wampy('ws://app.work.management:8081/ws', {
  realm: 'notifications',
  onConnect: () => console.log('Connected'),
  onClose: () => console.log('Disonnected'),
  onError: (error) => console.log(error),
});

class MyFeedsContainer extends Component {
    constructor(props) {
      super(props);

      this.state = {
        feedsList: props.feedsList,
        selectedItem: undefined,
        showPicker: false,
        newMessage: ''
      }
      autoBind(this);
    }

    componentWillMount() {
      ws.connect()
      const feedType = 'ALL'
      this.props.actions.fetchFeeds(feedType)
    }

    componentWillUnmount() {
      ws.disconnect()
    }

    componentWillReceiveProps(nextProps) {
      !isEqual(nextProps.feedsList, this.state.feedsList) && this.setState({feedsList: nextProps.feedsList})
    }

    render() {
      const
        _styles = {
          listCard : {
            flex:1,
            marginRight:'20px',
            minHeight: '700px',
          },
          chatCard : {
            flex:2,
            minHeight: '700px',
          },
          title: {
            display: 'flex',
            aligItems: 'center',
            justifyContent: 'center',
            padding: '15px',
            borderBottom: '1px solid #cfd8dc'
          },
        },
        { feedsList, selectedItem } = this.state,
        { selectedFeed, userId } = this.props

      return (
        <div>
          <div className='d-flex justify-content-between'>
            <div className='card card-default'style={_styles.listCard}>
              <FeedsContent list={feedsList} getAllActivities={this._getAllActivities} />
            </div>
            <div className='card card-default' style={_styles.chatCard}>
              <ChatContent item={selectedFeed}
                           userId={userId}
                           chatTitle={selectedItem && selectedItem.name}
                           sendMessage={this._postActivity}
                           togglePicker={this._togglePicker}
                           showPicker={this.state.showPicker}
                           handleMessageChange={this._handleMessageChange}
                           newMessage={this.state.newMessage}
                           addEmoji={this._appendEmojiToMessage}
              />
            </div>
          </div>
        </div>
      )
    }

    _getAllActivities(selectedItem) {
      const
        { slug, name } = selectedItem,
        data = {
          type: 'ALL',
          slug
        }
      this.setState({
        selectedItem
      }, () => {
        ws.subscribe(slug, {
           onSuccess: () => this.props.actions.getAllActivity(data),
           onError: (err) => { console.log('Subscription error:' + err.error); },
           onEvent: (result) => {
             const { argsList } = result
             this.props.actions.addNewActivity('ALL', argsList[0])
           }
        })
      })
    }

    _postActivity() {
      const { newMessage } = this.state
      console.log(newMessage)
      // const payload = { feedId: '127',
      //                   objectName: 'Hello !!',
      //                   score: 5,
      //                   slug: 'Project:56',
      //                   foreignId: '67',
      //                   verb: 'POST',
      //                   activityType: 'TEXT' }
      // this.props.actions.postActivity(payload, 'ALL', () => {})
    }

    _handleMessageChange(newMessage) {
      this.setState({newMessage})
    }

    _togglePicker() {
      this.setState({showPicker: !this.state.showPicker})
    }

    _appendEmojiToMessage(emoji) {
      console.log(emoji);
      this.setState({newMessage: this.state.newMessage+emoji.native})
    }
}

const
  mapStateToProps = (state) => {
    const
      { id } = state.session.user,
      { feedsList, selectedFeed } = state.feeds.allFeeds
    return {
      feedsList,
      selectedFeed,
      userId: id
    };
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...feedsActions,
        }, dispatch)
    };
  }

export default connect(mapStateToProps, mapDispatchToProps)(MyFeedsContainer)
