import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import SearchInput, { createFilter } from 'react-search-input'
import _, { filter, isEmpty } from 'lodash'
import autoBind from 'react-autobind'
import MyFeedItem from './MyFeedItem'

class MyShiftsContent extends Component {
    constructor(props) {
      super(props);

      this.state = {
          searchTerm : '',
      }
      autoBind(this);
    }

    render() {
      const
        _styles = {
          searchBox: {
            width: '200px',
            marginRight: '8px',
          },
          filterLink: {
            fontSize : '25px',
            color : '#b0bec5'
          },
          tools: {
            padding: '10px 15px',
            borderBottom: '1px solid #cfd8dc'
          },
          emptyContent: {
            textAlign: 'center',
            width: '100%',
            height: '700px',
          },
          noMatch: {
            width: '100%',
            color:'#20a8d8',
            paddingTop: '40px',
            textAlign:'center'
          },
        },
        { list, getAllActivities } = this.props,
        { searchTerm } = this.state,
        filteredItems = filter(list, createFilter(searchTerm, ['name']));

      return (
        <div>
            <div className='d-flex justify-content-between align-items-center' style={_styles.tools}>
              <h5 style={{fontSize: '15px'}}>Messages</h5>
              <div className='d-flex align-items-center'>
                <SearchInput className='list-search' style={_styles.searchBox}  onChange={this._searchUpdated} />
              </div>
            </div>
            <div style={{overflow: 'auto', height:'620px'}} id='list-scrollbar'>
              {
                isEmpty(filteredItems) ?
                <div style={_styles.noMatch}>No match found</div>
              : filteredItems.map((item, index) => (
                  <MyFeedItem key={index} item={item} index={index+1} getAllActivities={getAllActivities}/>
                ))
              }
            </div>
          </div>
      )
    }

    _searchUpdated (term) {
      this.setState({ searchTerm: term })
    }
}

const
  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {

        }, dispatch)
    };
  }

export default connect(null, mapDispatchToProps)(MyShiftsContent);
