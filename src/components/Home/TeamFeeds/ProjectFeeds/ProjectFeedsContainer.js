import React, { Component } from 'react';
import FeedsContent from './ProjectFeedsContent';
import _ from 'lodash';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import feedsActions from '../../../../actions/TeamFeeds'
import {isEqual} from 'lodash'
import ChatContent from './ChatContent'

class ProjectShiftsContainer extends Component {
    constructor(props) {
      super(props);

      this.state = {
        feedsList: props.feedsList,
        selectedChat: undefined
      }
      autoBind(this);
    }

    componentWillMount() {
      const feedType = 'PROJECT'
      this.props.actions.fetchFeeds(feedType)
    }

    componentWillReceiveProps(nextProps) {
      !isEqual(nextProps.feedsList, this.state.feedsList) && this.setState({feedsList: nextProps.feedsList})
    }

    render() {
      const
        _styles = {
          listCard : {
            flex:1,
            marginRight:'20px',
            minHeight: '700px',
          },
          chatCard : {
            flex:2,
            minHeight: '700px',
          },
        },
        { feedsList, selectedChat } = this.state,
        { selectedFeed, userId } = this.props

      return (
        <div>
          <div className='d-flex justify-content-between'>
            <div className='card card-default'style={_styles.listCard}>
              <FeedsContent list={feedsList} getAllActivities={this._getAllActivities} />
            </div>
            <div className='card card-default' style={_styles.chatCard}>
              <ChatContent item={selectedFeed} userId={userId} chatTitle={selectedChat}/>
            </div>
          </div>
        </div>
      )
    }

    _getAllActivities(selectedItem) {
      const
        { slug, name } = selectedItem,
        data = {
          type: 'PROJECT',
          slug
        }
      this.setState({
        selectedChat: name
      }, () => this.props.actions.getAllActivity(data))
    }
}

const
  mapStateToProps = (state) => {
    const
      { id } = state.session.user,
      { feedsList, selectedFeed } = state.feeds.projectFeeds
    return {
      feedsList,
      selectedFeed,
      userId: id
    };
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...feedsActions,
        }, dispatch)
    };
  }

export default connect(mapStateToProps, mapDispatchToProps)(ProjectShiftsContainer)
