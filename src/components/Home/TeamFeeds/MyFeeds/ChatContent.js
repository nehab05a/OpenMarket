import React from 'react';
import GoogleMapReact from 'google-map-react';
import { InputGroup, InputGroupAddon, Input } from 'reactstrap';
import { Picker } from 'emoji-mart'
import 'emoji-mart/css/emoji-mart.css'

const Location = ({ iconClass }) => <i className={iconClass} style={{fontSize: '35px', color: '#20a8d8'}}></i>;

const ChatContent = ({
  item,
  userId,
  chatTitle
}) => {

  const styles = {
                  title: {
                    display: 'flex',
                    aligItems: 'center',
                    justifyContent: 'center',
                    padding: '15px',
                    borderBottom: '1px solid #cfd8dc'
                  },
                }
   return (
    <div>
      <div style={styles.title}>
        <h5>{chatTitle || 'Selected Item'}</h5>
      </div>
      <div style={{overflow: 'auto', height:'620px', padding:'20px 0px'}} id='list-scrollbar'>
        {item && item.map((activity, index) =>{
            const isSender = Number(activity.userId) === Number(userId)
            return(
                  <div key={index}>
                  <li>
                    <div className={isSender ?  'self' : 'other'}>
                    { activity.activityType === 'TEXT' && <span className='content'></span>}
                          <div className='box' >
                            {
                              activity.activityType === 'IMAGE' ?
                              <div className='feed-image'>
                                <img src={activity.mediaUrl} alt='Not found' width="100%" height="100%"/>
                              </div>
                            : activity.activityType === 'LOCATION' ?
                              <div className='map' style={{width: '210px'}}>
                                <GoogleMapReact
                                  bootstrapURLKeys={{
                                    key: 'AIzaSyDLkbQy-AsAZBvioJBhpv39vQkDEViB3O8',
                                    language: 'en',
                                  }}
                                  defaultCenter={{ lat: Number(activity.latitude), lng: Number(activity.longitude)}}
                                  defaultZoom={15}
                                >
                                  <Location
                                    lat={Number(activity.latitude)}
                                    lng={Number(activity.longitude)}
                                    iconClass='fa fa-map-marker'/>
                                </GoogleMapReact>
                              </div>
                            : activity.activityType==='VIDEO'?
                              <center>
                                {activity.mediaUrl ?
                                  <video className='video' src={activity.mediaUrl} controls />
                                : <p>Video Not found</p>}
                              </center>
                            : activity.activityType==='AUDIO'?
                              <center>
                              {activity.mediaUrl ?
                                <audio className='audio' src={activity.mediaUrl} controls />
                              : <p>Audio not found</p>}
                              </center>
                            : <div className='innerbox'>
                                <div className='text'>
                                    {activity.objectName}
                                </div>
                              </div>                          }
                            <div className='timeBox'>
                                <time>
                                    {activity.published}
                                </time>
                            </div>
                          </div>
                    </div>
                  </li>
              </div>
          )})
          }
     </div>
     <InputGroup>
        <InputGroupAddon >
          &#x1f600;
        </InputGroupAddon>
        <Input placeholder='Type a message' />
      </InputGroup>
     <Picker  emojiSize={24}
              perLine={9}
              skin={1}
              set='google'
              showPreview={false}
              style={{width: '100%'}}
              onClick={(a, b) => console.log(a, a.native, b.target)}
      />
   </div>
 )
}
export default ChatContent;
