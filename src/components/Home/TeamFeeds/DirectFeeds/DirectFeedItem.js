import React from 'react'

const FeedContent = (props) =>
{
  const
    { item, index, getAllActivities } = props,
    { activities, name, updateDate, thumbnailUrl, slug } = item,
    { objectName } = activities.length > 0 && activities[0],
    _styles = {
      listContainer: {
        padding: '10px',
        borderBottom: '1px solid #cfd8dc'
      },
      actionContainer: {
        padding: '8px',
        fontWeight: 600,
        background: '#20a8d8',
        color: 'white'
      },
      userIcon: {
        textAlign: 'center'
      },
      messageContent: {
        color: '#7F8FA4',
        fontWeight: 500,
        width: '200px',
        wordWrap: 'break-word'
      }
    },
    { env } = window.teamapp,
    getEndPoint =   env === 'dev'   ?  'http://dev.work.management'
                  : env === 'stage' ?  'http://stage.work.management'
                  : env === 'prod'  ?  'http://api.work.management'
                                    :  'http://10.11.13.132'


  return (
    <div key={index} style={_styles.listContainer} className='d-flex justify-content-between' onClick={() => slug ? getAllActivities(item) : null}>
      <div className='user-avatar' style={{flex:1}}>
      {
        thumbnailUrl ?
          <img src={getEndPoint+thumbnailUrl} className='img-avatar' alt='FL' height={40} width={40} style={_styles.userIcon}/>
        : <p className='profile' >{name.match(/\b(\w)/g).join('').substring(0,2)}</p>
      }
      </div>
      <div className='d-flex flex-column' style={{flex:5}}>
        <span style={{fontWeight: 500}}>{name}</span>
        <span style={_styles.messageContent}>{objectName}</span>
      </div>
      <div style={{color: '#7F8FA4', flex:3}}>{updateDate}</div>
    </div>
  )
}

export default FeedContent
