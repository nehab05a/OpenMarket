import React,{Component} from 'react'
import autobind from 'react-autobind'
import TabContainer from './TabContainer'
import feedsActions from '../../../actions/TeamFeeds'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import MyFeeds from './MyFeeds'
import AllFeeds from './AllFeeds'
import ProjectFeeds from './ProjectFeeds'
import DirectFeeds from './DirectFeeds'
import TeamFeeds from './TeamFeeds'

class Feeds extends Component{
  constructor(props) {
    super(props)
    this.state = {
      page: 0,
    }
    autobind(this)
  }

  setPage(page) {
    this.setState({page})
  }

  render(){
    const
      { page } = this.state,
      tabItems = [
        {
          title: 'All Feeds',
          tabContent: <AllFeeds />
        },
        {
          title: 'My Messages',
          tabContent: <MyFeeds />
        },
        {
          title: 'Project Feeds',
          tabContent: <ProjectFeeds />
        },
        {
          title: 'Team Feeds',
          tabContent: <TeamFeeds />
        },
        {
          title: 'Direct Feeds',
          tabContent: <DirectFeeds />
        },
      ]
    return(
      <TabContainer currentPage={page} setPage={this.setPage} tabItems={tabItems}/>
    )
  }
}

const
  mapStateToProps = (state, ownProps) => {
    return {

    };
  },

  mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( {
          ...feedsActions
        }, dispatch)
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(Feeds)
