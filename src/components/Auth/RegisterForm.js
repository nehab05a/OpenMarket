import React from 'react'
import PropTypes from 'prop-types'
import Public from '../Public'
import SectorsOptions from '../../config/data/Sectors'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../Commons/FormGroup'
import {required, emailValid, confirmEmail} from '../Commons/Validators'
import TeamSize from '../../config/data/TeamSize'
import SelectRedux from '../Commons/SelectRedux'
import Captcha from './Recaptcha'

const RegisterForm = (props) => {
    const { handleSubmit, submitting, invalid, errorMessage, toggleModal, recoverAccount } = props,
    _styles = {
      terms: {
        color: '#20a8d8',
        cursor: 'pointer'
      }
    }
  return (
    <Public>
        <form className='form-register form-pb p-0' onSubmit={handleSubmit}>
            <h3 className='mb-2'>Register</h3>
            <div className='row'>
                <Field className='col-md-6' name='firstName' type='text' component={FormGroup} label='First Name' validate={required}/>
                <Field className='col-md-6' name='lastName' type='text' component={FormGroup} label='Last Name' validate={required}/>
            </div>
            <div className='row'>
                <Field className='col-md-6' name='email' type='email' component={FormGroup} label='Email'
                  validate={[ required, emailValid ]} />
                <Field className='col-md-6' name='confirmEmail' type='email' component={FormGroup} label='Email confirmation'
                  validate={[ required, emailValid, confirmEmail ]} />
            </div>
            <div className='row'>
              <Field className='col-md-6' name='companyName' type='text' component={FormGroup} label='Organisation' validate={required} />
              <Field className='col-md-6' name='webAddress' type='text' component={FormGroup} label='Web address' validate={required} />
            </div>
            <div className='row'>
              <Field className='col-md-6' name='fullCompanyName' type='text' options={SectorsOptions} component={SelectRedux} label='Sector' validate={required}/>
              <Field className='col-md-6' name='teamSize' options={TeamSize}  label='Team Size?' component={SelectRedux} validate={required}/>
            </div>
            <div className='row'>
                <div className='col-sm-12'>
                    <div className='checkbox'>
                        <label htmlFor='checkbox1'>
                            <Field name='tyc' id='tyc' component='input' type='checkbox' validate={required}/>
                            I agree to the Work.Management
                            <span onClick={toggleModal} style={_styles.terms}> Terms and Privacy</span>
                        </label>
                    </div>
                </div>
            </div>
            <div className='row ml-0'>
              <Field className='col-md-6' name='captcharesponse' component={Captcha} validate={required}/>
            </div>
            <div className='has-danger'>
              {errorMessage &&
                <div className='mt-2'>
                  <p className='form-control-feedback d-flex justify-content-center'>{errorMessage}</p>
                  {errorMessage === 'User already exists with provided email.' &&
                    <p className="txt-msg mt-2">
                      It seems like you are already a member. <span
                      style={{
                        color: '#20a8d8',
                        cursor: 'pointer'
                      }} onClick={recoverAccount}>Continue</span> to recover your account
                    </p>}
                </div>
              }
            </div>
            <div className='row mt-2'>
                <div className='offset-sm-2  col-sm-8 '>
                    <button className='btn btn-lg btn-primary btn-block' type='submit'
                      disabled={submitting || invalid}>
                      Register
                    </button>
                </div>
            </div>
        </form>
    </Public>
)}

RegisterForm.propTypes= {
   errorMessage: PropTypes.string
}

export default reduxForm({
  form: 'registerForm' // a unique identifier for this form
})(RegisterForm)
