import React from 'react'
import PropTypes from 'prop-types'
import Public from '../Public'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../Commons/FormGroup'
import {required, emailValid} from '../Commons/Validators'
import {Link} from 'react-router-dom';

const LoginForm = (props) => {
    const { errorLogin, handleSubmit, submitting, invalid, isFetching} = props;
    return (
        <Public>
            <form className="form-pb form-signin" onSubmit={handleSubmit}>
                <h3 className="mb-2">Sign In</h3>
                <div className="row">
                    <Field className="col-md-12" name="email" type="text" component={FormGroup} label="Email Address" validate={[required, emailValid]}/>
                </div>
                <div className="row">
                    <Field className="col-md-12" name="password" type="password" component={FormGroup} label="Password" validate={required}/>
                </div>
					<input type="hidden" defaultValue={process.env.NODE_ENV} />
                    <button className="btn btn-primary btn-block" type="submit" disabled={submitting || invalid}>
                        Sign in
                    </button>
                    {errorLogin &&
                      <div className="alert alert-danger mt-2" role="alert">
                         <i className="fa fa-exclamation" aria-hidden="true"></i>
                        <span className="sr-only">Error:</span>
                        &nbsp;{errorLogin}
                      </div>
                    }
                    <hr/>
                    <Link to="/register" className="btn btn-outline-primary btn-block mt-2">
                        Create Account
                    </Link>
                    <Link to="/verify-account" className="btn btn-link mt-2">
                        Forgotten your Password?
                    </Link>
            </form>
            {isFetching && <div className='loading-spinner' />}
        </Public>
    )
}

LoginForm.propTypes= {
   errorMessage: PropTypes.string
}

export default reduxForm({
    form: 'loginForm' // a unique identifier for this form
})(LoginForm)
