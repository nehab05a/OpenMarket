import React from 'react';
import Public from '../Public';
import {withRouter} from 'react-router-dom'
const TxtMsg = ({txt}) => (
  <p className="txt-msg lead">{txt}</p>
)
const Mail = (props) => {
  return(
  <Public>
    <div className="container">
      <div className="row">
        <div className="col align-self-center">
          <div className="text-center mail">
            <i className="fa fa-envelope-o"></i>
            <h2 className="mb-2">Thank you</h2>
          </div>
        </div>
      </div>
      {props.location.search === '?forgot'?
          <div>
            <TxtMsg txt="We have sent you an email to recover your Password"/>
            <TxtMsg txt="please follow instructions within the email"/>
          </div>
        : props.location.search === '?verify'?
          <div>
            <TxtMsg txt="You will now be sent an email containing"/>
            <TxtMsg txt="details on how to login into your account"/>
          </div>
        : <div>
            <TxtMsg txt="Your application has been successfully submitted"/>
            <TxtMsg txt="An email will be sent once your account is Approved"/>
            <TxtMsg txt="Please follow instruction within the email"/>
          </div>
      }
    </div>

  </Public>
)}

export default withRouter(Mail);
