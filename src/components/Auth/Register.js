import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RegisterForm from './RegisterForm'
import * as sessionActions from '../../actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter} from 'react-router-dom'
import autobind from 'react-autobind'
import {Modal, ModalBody, ModalHeader} from 'reactstrap'

class Register extends Component {

    constructor(props, context) {
      super(props, context);
      this.state = {
        isOpen: false
      }
      autobind(this)
    }
    onSubmit = (values) => {
      const {register} = this.props.actions;
      register(values, this.registerSuccess, this.registerFailure)
    }
    registerSuccess() {
      this.props.history.push('/mail?register');
    }
    registerFailure(errorMessage) {
      this.setState({errorMessage})
    }
    showTerms() {
      this.setState({isOpen: !this.state.isOpen})
    }
    recoverAccount() {
      this.props.history.push('/verify-account');
    }
    render() {
      const {isOpen} = this.state
      return (
        <div>
          <RegisterForm {...this.state}
                        onSubmit={this.onSubmit}
                        toggleModal={this.showTerms}
                        recoverAccount={this.recoverAccount}
                        />
          <Modal isOpen={isOpen} toggle={this.showTerms}>
            <ModalHeader toggle={this.showTerms}>Terms And Privacy</ModalHeader>
            <ModalBody style={{color: '#607d8b', height: '600px'}}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
              Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore
              et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum
              fugiat quo voluptas nulla pariatur
            </ModalBody>
          </Modal>
        </div>
      )
    }
}
const {object} = PropTypes;

Register.propTypes = {
    actions: object.isRequired
};

const mapDispatch = (dispatch) => {
    return {
        actions: bindActionCreators(sessionActions, dispatch)
    };
};
export default withRouter(connect(null, mapDispatch)(Register));
