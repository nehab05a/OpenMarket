import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {withRouter} from 'react-router-dom'
import * as sessionActions from '../../actions'
import LoginForm from './LoginForm'
import autobind from 'react-autobind'
// should work in any browser without browserify

class Login extends Component {

    constructor(props) {
        super(props);
        autobind(this)
    }
    state = {
        errorLogin: ''
    }

    submit(user) {
        const {login} = this.props.actions;
        const {history} = this.props;
        login(user, history, this.loginSuccess, this.loginFailure)
    }

    loginSuccess() {

    }

    loginFailure(errorLogin) {
      this.setState({errorLogin})
    }

    render() {
        return (<LoginForm errorLogin={this.state.errorLogin} isFetching={this.props.isFetching} onSubmit={this.submit}/>);
    }
}

const {object} = PropTypes;

Login.propTypes = {
    actions: object.isRequired
};

const
mapState = (state) => {
  const {login} = state,
    isFetching = login && login.fetch
    return {
      isFetching
    };
},
mapDispatch = (dispatch) => {
    return {
        actions: bindActionCreators(sessionActions, dispatch)
    };
};

export default withRouter(connect(mapState, mapDispatch)(Login));
