import React from 'react'
import PropTypes from 'prop-types'
import Public from '../Public'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../Commons/FormGroup'
import {required} from '../Commons/Validators'
import {Link} from 'react-router-dom'

const VerifyCodeForm = (props) => {
  const {handleSubmit, submitting, invalid, errorMessage} = props;
  return (
    <Public>
      <form className='form-pb form-signin' onSubmit={handleSubmit}>
        <h3 className='mb-2'>Verify Your Account</h3>
        <p className='txt-msg'>Enter verification code received on your registered email id.</p>
        <div className='row'>
          <Field className='col-md-12' name='verificationCode' type='text' component={FormGroup} label='Verification Code' validate={[required]}/>
        </div>
        <div className='has-danger'>
          {errorMessage && <div className='form-control-feedback'>{errorMessage}</div>}
        </div>
        <button className='btn btn-primary btn-block mt-2' type='submit' disabled={submitting || invalid}>Verify</button>
        <hr/>
        <Link to='/login' className='btn btn-outline-primary btn-block mt-2'>Cancel</Link>
      </form>
    </Public>
  )
}

VerifyCodeForm.propTypes= {
   errorMessage: PropTypes.string
}

export default reduxForm({form: 'verifyCodeForm'})(VerifyCodeForm)
