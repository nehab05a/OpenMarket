import React, {Component} from 'react'
import PropTypes from 'prop-types'
import * as sessionActions from '../../actions'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {withRouter} from 'react-router-dom'
import VerifyUserForm from './VerifyUserForm'
import VerifyCodeForm from './VerifyCodeForm'
import autobind from 'react-autobind'
import queryString from 'query-string'

class VerifyAccount extends Component {
  constructor(props, context) {
    super(props, context);

    const
      { location } = this.props,
      { mode, hash } = queryString.parse(location.search)

    this.state = {
      mode,
      username: encodeURIComponent(hash)
    }
    autobind(this)
  }

  onSubmit = (values) => {
    const {forgotPassword} = this.props.actions;
    this.setState({
      userName: values.email
    }, () => forgotPassword(values, this.changeSuccess, this.changeFailure))
  }

  verifyCode(values) {
    const
      { username, mode } = this.state,
      { location, actions: {verifyCode} } = this.props,
      parsed = queryString.parse(location.search),
      payload = {
        userName: parsed.username || username,
        verificationCode: values.verificationCode,
        mode,
      }
      verifyCode(payload, this.verifySuccess, this.changeFailure)
  }

  verifySuccess() {
    const { mode } = this.state,
    path = mode === "1" ? '/mail?verify' : '/mail?forgot'
    this.props.history.push(path);
  }

  changeSuccess() {
    this.props.history.push('/mail?forgot');
  }

  changeFailure(errorMessage) {
    this.setState({errorMessage})
  }

  render() {
    const { mode, username } = this.state
    return (
      <div>
      {
        mode && username ?
          <VerifyCodeForm onSubmit={this.verifyCode} {...this.state} />
        : <VerifyUserForm onSubmit={this.onSubmit} {...this.state} />
      }
      </div>
    )
  }
}

VerifyAccount.propTypes = {
  actions: PropTypes.object
}

const mapDispatch = (dispatch) => {
  return {
    actions: bindActionCreators(sessionActions, dispatch)
  };
};
export default withRouter(connect(null, mapDispatch)(VerifyAccount));
