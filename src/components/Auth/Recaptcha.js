import React from 'react'
import PropTypes from 'prop-types'
import ReCAPTCHA from 'react-recaptcha'

const RECAPTCHA_SITE_KEY = '6LdqVjAUAAAAAFCypeMgVEj-T4j58p40rhpaODYO';

const Captcha = (props) => (
  <div>
    {props.meta.touched && props.meta.error}
    <ReCAPTCHA
          render='explicit'
          sitekey={RECAPTCHA_SITE_KEY}
          verifyCallback={response => props.input.onChange(response)}
        />
    </div>
);

Captcha.propTypes = {
  input: PropTypes.object.isRequired
};

export default Captcha;
