import React, {Component} from 'react'
import PropTypes from 'prop-types'
import * as sessionActions from '../../actions'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {withRouter} from 'react-router-dom'
import ChangePasswordForm from './ChangePasswordForm'
import queryString from 'query-string'
import autoBind from 'react-autobind'

class ChangePassword extends Component {
  constructor(props, context) {
    super(props, context);
    const
      { hash } = queryString.parse(location.search),
      username = encodeURIComponent(hash)
      
    this.state = {
      username
    }
    autoBind(this)
  }

  resetPassword(values)  {
    const
      { username } = this.state,
      { newPassword, confirmPassword } = values,
      payload = {
        newPassword,
        username
      }
    if(newPassword !== confirmPassword) this.setState({errorMessage: 'Password does not match'})
    else this.props.actions.newPassword(payload, this.resetSuccess, this.resetFailure)
  }

  resetSuccess() {
    this.props.history.push('/login');
  }

  resetFailure(errorMessage) {
    this.setState({errorMessage})
  }

  render() {
    return (<ChangePasswordForm onSubmit={this.resetPassword} {...this.state}/>)
  }
}

ChangePassword.propTypes = {
  actions: PropTypes.object
}

const mapDispatch = (dispatch) => {
  return {
    actions: bindActionCreators(sessionActions, dispatch)
  };
};

export default withRouter(connect(null, mapDispatch)(ChangePassword));
