import React from 'react'
import PropTypes from 'prop-types'
import Public from '../Public'
import {Field, reduxForm} from 'redux-form'
import FormGroup from '../Commons/FormGroup'
import {required, emailValid} from '../Commons/Validators'
import {Link} from 'react-router-dom'

const VerifyAccountForm = (props) => {
  const {handleSubmit, submitting, invalid, errorMessage} = props;
  return (
    <Public>
      <form className="form-pb form-signin" onSubmit={handleSubmit}>
        <h3 className="mb-2">Forgot your Password?</h3>
        <p className="txt-msg">Enter your email address and we’ll send a link to reset your password.</p>
        <div className="row">
          <Field className="col-md-12" name="email" type="text" component={FormGroup} label="Email Address" validate={[required, emailValid]}/>
        </div>
        <div className='has-danger'>
          {errorMessage && <div className='form-control-feedback'>{errorMessage}</div>}
        </div>
        <button className="btn btn-primary btn-block mt-2" type="submit" disabled={submitting || invalid}>Reset</button>
        <hr/>
        <Link to="/login" className="btn btn-outline-primary btn-block mt-2">Cancel</Link>
      </form>
    </Public>
  )
}

VerifyAccountForm.propTypes= {
   errorMessage: PropTypes.string
}

export default reduxForm({form: 'forgotPasswordForm'})(VerifyAccountForm)
