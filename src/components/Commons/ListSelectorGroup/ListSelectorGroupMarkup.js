import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

const ListSelectorGroup = ({
  input,
  placeholder,
  disabled,
  label,
  type,
  className,
  toggleSelection,
  meta: {  touched,  error  }
}) => {
  return(
     <div className={classnames({ 'has-danger': touched && error })}>
        {label && <label className="form-control-label">{label}</label>}
        <input {...input} onFocus={toggleSelection} disabled={disabled} placeholder={placeholder?placeholder:label} type={type} className="form-control"/>
        {touched && ((error && <div className="form-control-feedback">{error}</div>))}
    </div>
  )}


const { string, object, func } = PropTypes;

ListSelectorGroup.propTypes = {
  label: string,
  type: string.isRequired,
  meta: object.isRequired,
  toggleSelection: func,
};

export default ListSelectorGroup;
