import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ListSelectorGroupMarkup from './ListSelectorGroupMarkup'
import { Modal,ModalHeader,ModalBody } from 'reactstrap'
import OrderedList from '../OrderedList'
import autoBind from 'react-autobind'
import classnames from 'classnames'

class ListSelector extends Component{
  constructor(props){
    super(props)
    this.state = {
      showSelection: false,
      selectedValue: {},
      createEntry: false,
    }
    autoBind(this)
  }

  _toggleSelection() {
    this.setState({showSelection: !this.state.showSelection})
  }

  _toggleCreation() {
    this.setState({createEntry: !this.state.createEntry})
  }

  _handleSelection(selectedValue) {
    const { onAnswered } = this.props
    this.setState({selectedValue}, () => onAnswered && onAnswered(selectedValue))
    this._toggleSelection()
  }

  render(){
    const { createEntry, showSelection } = this.state,
      { label, placeholder, className, addItem } = this.props;
    return (
      <div className={classnames(`form-group ${className}`)}>
        <ListSelectorGroupMarkup
          {...this.state}
          {...this.props}
          toggleSelection={this._toggleSelection}
        />
        <Modal style={{ width: '400px'}} isOpen={showSelection}>
          <ModalHeader toggle={this._toggleSelection}>
            <div className='d-flex  justify-content-between align-items-center' style={{width: '100%'}}>
              <span>{createEntry ? `Create ${label || placeholder}`  : `Select ${label || placeholder}`}</span>
              {addItem &&
                <i className="icon-plus" style={{fontSize: '20px', color: '#20a8d8', marginRight: '20px'}} onClick={this._toggleCreation}></i>
              }
            </div>
          </ModalHeader>
          <ModalBody>
            {createEntry && addItem ?
              this.props.createComponent && <this.props.createComponent onSubmit={this._onCreateEntry} onCancel={this._toggleCreation}/>
            :<OrderedList
              {...this.props}
              onAnswered={this._handleSelection}
              onCancel={this._toggleSelection}
            />}
          </ModalBody>
        </Modal>
      </div>
    )
  }

  _onCreateEntry(values) {
    this.props.onCreateEntry(values, this._toggleCreation)
  }
}

ListSelector.propTypes = {
  list: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  onAnswered: PropTypes.func,
  className: PropTypes.string,
  addItem: PropTypes.bool,
  createComponent: PropTypes.func,
}

ListSelector.defaultProps = {
  addItem: false
}

export default ListSelector
