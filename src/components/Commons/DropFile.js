import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone'
import { Tooltip } from 'reactstrap';
const DropFile = (props) => (
    <div>
        {props.image || props.picUrl
            ? <div className="row">
                    <div className=" form-group col-md-12">
                        <label className="form-control-label">{props.label}</label>
                        <div className="form-control">
                          <button id="tooltipImg" type="button" onClick={props.deleteImg} className="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <Tooltip placement="right" isOpen={props.tooltipImg} target="tooltipImg" toggle={props.toggle}>
                            Delete Image
                          </Tooltip>
                            { props.picUrl
                              ? <img className="img-fluid" src={props.picUrl} role="presentation" alt=''/>
                              : props.image.map((file,index) =>
                              <img key={index} className="img-fluid" src={file.preview} role="presentation"/>
                            )}
                        </div>
                    </div>
                </div>
            : <div className="row">
                <div className="col-md-12">
                    <Dropzone className="drop-zone"
                      onDrop={props.onDrop}
                      multiple={false}
                       accept="image/*">
                        <div>
                            <h5 className="text-muted">Drag Image Here</h5>
                        </div>
                    </Dropzone>
                    <label className="text-muted">Only images e.g .jpeg,.png, only. Image size must be less than 2 mb</label>
                </div>
            </div>
          }
    </div>
)
DropFile.propTypes = {
    image: PropTypes.array,
    onDrop: PropTypes.func,
    deleteImg: PropTypes.func,
    toggle: PropTypes.func,
    tooltipImg: PropTypes.bool,
    label: PropTypes.string
};
export default DropFile;
