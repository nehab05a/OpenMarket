import moment from 'moment'

export const required = value => value ? undefined : 'Required'

export const isNumber = value => value && !/^\d+$/.test(value) ? 'Enter Number Format' : undefined;

export const confirmEmail = ( value, allValues) => value === allValues.email ? undefined :  'Confirm email does not match'

export const validateStartEndTime = ( value, allValues ) => {
  const
    { start_time, end_time } = allValues,
    startTime = start_time && start_time.format('hh:mm a'),
    endTime = end_time && end_time.format('hh:mm a'),
    error = (endTime && startTime) && moment(endTime).isAfter(startTime) ? undefined : 'Select End Time Post Start Time'
    return error
}

export const emailValid = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]+$/i.test(value) ?
  'Invalid email address' : undefined

export default {
  confirmEmail,
}
