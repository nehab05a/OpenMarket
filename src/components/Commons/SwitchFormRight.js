import React from 'react';
import PropTypes from 'prop-types';

const SwitchFormRight = (props) => {
  const {label, message, input, disabled} = props
  return (
    <div className="form-group col-md-6 ">
      <label className="form-control-label">{label}</label>
      <div className="form-control control-switch">
        <table>
          <tbody>
            <tr>
              <td>
                <label className="switch switch-3d switch-primary">
                  <input type="checkbox" {...input} checked={input.value || false} className="switch-input" disabled={disabled}/>
                  <span className="switch-label"></span>
                  <span className="switch-handle"></span>
                </label>
              </td>
              <td>
                <label className="control-switch-label-right">
                  {message}
                </label>
              </td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>
  )
}

const {string, object} = PropTypes;

SwitchFormRight.propTypes = {
  label: string,
  message: string.isRequired,
  input: object.isRequired
};

export default SwitchFormRight;
