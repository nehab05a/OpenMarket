import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Dropdown, DropdownMenu, DropdownItem, Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap'
import SearchInput, {createFilter} from 'react-search-input'
import Item from './Item'
import autobind from 'react-autobind'

class Container extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchTerm : '',
      showFilter : false
    }
    autobind(this)
  }
  render() {
    const
      { items, selected, filters, isAddItem, toggleAddItem, addEntry, styles } = this.props,
      { showFilter } = this.state,
      filteredItems = items.filter(createFilter(this.state.searchTerm, ['name'])),
      _styles = {
        container: {
          height: '100%',
          flex: 1,
          border: '1px solid #DFE3E9'
        },
        searchBox: {
          width: '130px',
          marginRight: '8px'
        },
        filterLink: {
          fontSize : '25px',
          color : '#b0bec5'
        },
        items: {
          overflow: 'auto',
          maxHeight: '350px',
          ...styles.items
        },
        tools: {
          padding: '10px',
        },
        image: {
          fontSize: '25px',
          marginRight: '10px'
        },
        addItem: {
          borderBottom: '1px solid #DFE3E9',
          borderTop: '1px solid #DFE3E9',
          fontSize: 14,
          fontWeight: 500,
          color: '#607d8b',
          cursor: 'default',
          padding: '5px'
        },
      };
    return(
      <div style={_styles.container}>
        <div className='d-flex align-items-center' style={_styles.tools}>
          <SearchInput className='list-search' style={_styles.searchBox}  onChange={this.searchUpdated} />

          {filters && <Dropdown isOpen={showFilter} toggle={this.toggleFilter}>
              <a onClick={this.toggleFilter} data-toggle='dropdown' href='#' role='button'>
                  <i className='fa fa-filter' style={_styles.filterLink}></i>
              </a>
              <DropdownMenu className='dropdown-menu-right'>
                {filters && filters.map((item, key) => (
                  <DropdownItem key={key} onClick={item.behavior}>
                      {item.component}
                  </DropdownItem>
                ))}
              </DropdownMenu>
          </Dropdown>}

        </div>
        <div>
          {addEntry && <div style={_styles.addItem} onClick={toggleAddItem} className='d-flex align-items-center'>
            <i style={_styles.image} className='icon-user-follow'/>
            Add Shift Worker
          </div>}
          <div style={_styles.items} id='list-scrollbar'>
            {
              filteredItems.map(item => <Item
                key={item.id}
                item={item}
                selected={selected}/>)
            }
          </div>
        </div>
        <Modal isOpen={isAddItem} id='modal'>
          <ModalHeader toggle={toggleAddItem}>{this.props.addItemLabel}</ModalHeader>
          <ModalBody>
            {this.props.addComponent && <this.props.addComponent />}
          </ModalBody>
          <ModalFooter className='d-flex justify-content-center'>
            <Button className='col-md-4' color='primary' onClick={() => {}}>Cancel</Button>
            <Button className='col-md-4' color='success' onClick={() => {}}>Add</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }

  searchUpdated (term) {
    this.setState({searchTerm: term})
  }

  toggleFilter() {
    this.setState({showFilter: !this.state.showFilter})
  }
}

Container.propTypes = {
  items: PropTypes.array,
  selected: PropTypes.array,
  filters: PropTypes.array,
  isAddItem: PropTypes.bool,
  toggleAddItem: PropTypes.func,
  addEntry: PropTypes.bool,
  styles: PropTypes.object
}

Container.defaultProps = {
  addEntry: true,
  styles: {}
}

export default Container;
