import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { DropTarget } from 'react-dnd'
import autobind from 'react-autobind'

const dropTarget = {
  drop(props, monitor, component) {
    const { items } = props,
      item = items.find(item => item.id === monitor.getItemType())
    props.updateSelected(item);
  },

  canDrop(props, monitor) {
    return !props.selected.some(item => item.id === monitor.getItemType())
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

class Container extends Component {
  constructor(props) {
    super(props)

    autobind(this);
  }

  removeItem(value) {
    this.props.updateSelected(value)
  }

  render() {
    const { connectDropTarget } = this.props,
      { selected } = this.props,
      _styles = {
        container: {
          position: 'relative',
          height: '100%',
          flex: 2,
          marginRight: '3%',
          border: '1px solid #DFE3E9',
          overflow: 'auto'
        },
        item: {
          borderBottom: '1px solid #DFE3E9',
          fontSize: 14,
          fontWeight: 500,
          color: '#607d8b',
          padding: '5px',
          cursor: 'default',
        },
        placeholder: {
          textAlign: 'center',
          width: '100%',
          height: '100%',
        },
        image: {
          height: '25px',
          width: '25px',
          marginRight: '10px'
        },
      };

    return connectDropTarget(
      <div className='d-flex flex-column' style={_styles.container} id='list-scrollbar'>
        {selected.length ?
          selected.map((element,index) => (
            <div style={_styles.item} key={index} onClick={() => this.removeItem(element)}>
              <img src="/img/avatars/6.jpg" style={_styles.image} className="img-avatar" alt="admin@bootstrapmaster.com"/>
              {element.name}
            </div>
          ))
        : <div style={_styles.placeholder} className='text-muted d-flex justify-content-center align-items-center'>
            Drag Shift Workers Here
          </div>
        }
      </div>
    );
  }
}

Container.propTypes = {
  isOver: PropTypes.bool.isRequired,
  items: PropTypes.array
};

export default DropTarget(props => props.typeIds, dropTarget, collect)(Container);
