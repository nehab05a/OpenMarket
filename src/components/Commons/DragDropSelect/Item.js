import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { DragSource } from 'react-dnd'

const dragSource = {
  beginDrag(props, monitor, component) {
    return { name: props.item.name };
  },
  endDrag() {

  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
  }
}

class Item extends Component {

  render() {
    const
      { connectDragSource, isDragging, selected, item } = this.props,
      isSelected = selected.some(element => element.id === item.id),
      _styles = {
        item: {
          borderBottom: '1px solid #DFE3E9',
          opacity: isDragging ? 0.5 : 1,
          fontSize: 14,
          fontWeight: 500,
          padding: '5px',
          cursor: 'move',
          color: isSelected ? '#20a8d8' : '#607d8b'
        },
        image: {
          height: '25px',
          width: '25px',
          marginRight: '10px'
        },
      };

    return connectDragSource(
      <div style={_styles.item}>
        <img src="/img/avatars/6.jpg" style={_styles.image} className="img-avatar" alt="admin@bootstrapmaster.com"/>
        {item.name}
      </div>
    );
  }
}

Item.propTypes = {
  connectDragSource: PropTypes.func.isRequired,
  isDragging: PropTypes.bool.isRequired,
  item: PropTypes.object,
  selected: PropTypes.array
};

export default DragSource((props) => props.item.id, dragSource, collect)(Item);
