import React, { Component } from 'react'
import PropTypes from 'prop-types'
import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd'
import ItemsContainer from './List'
import DropContainer from './DropContainer'
import autobind from 'react-autobind'

class Container extends Component {
  constructor(props){
    super(props)
    this.state = {
      items : [
        {
          id: '1',
          name: 'Jane Doe'
        }
      ],
      selected : [],
      isAddItem : false,
    }
    autobind(this)
  }

  updateItem(value) {
    // const {items} = this.state,
    //   updatedList = items.filter(item => item.id !== value);
    // this.setState({items: updatedList});
  }

  updateSelected(value) {
    const { selected } = this.state,
      updatedList = selected.some(item => item.id === value.id) ?
        selected.filter(item => item.id !== value.id ) : [...selected, value];
    this.setState({selected: updatedList})
  }

  toggleAddItem() {
    this.setState({isAddItem: !this.state.isAddItem})
  }

  render() {
    const
      _styles = {
        count :{
          fontSize: '14px',
          fontWeight: 500,
          color: '#607d8b',
          width: '100%'
        }
      },
      { items, selected, isAddItem } = this.state,
      { label, filters, addItemComponent, addItemLabel } = this.props,
      typeIds = items.map(item => item.id);

    return(
      <div className='row form-group d-flex flex-column' style={{padding: '0px 15px'}}>
        <label className='form-control-label'>{label}</label>
        <div className='d-flex' style={{height: '450px'}}>
          <DropContainer
            items={items}
            selected={selected}
            typeIds={typeIds}
            updateList={this.updateItem}
            updateSelected={this.updateSelected}
          />
          <ItemsContainer
            items={items}
            filters={filters}
            selected={selected}
            isAddItem={isAddItem}
            updateList={this.updateItem}
            toggleAddItem={this.toggleAddItem}
            addComponent={() => <addItemComponent/>}
            addItemLabel={addItemLabel}
          />
        </div>
        <div style={_styles.count} className='d-flex'>
          <span style={{flex:1.2}}>Shift Workers</span>
          <span style={{flex:1}}>{selected.length}</span>
        </div>
      </div>
    )
  }
}

Container.propTypes = {
  label: PropTypes.string,
  filters: PropTypes.array,
  addItemComponent: PropTypes.func,
  addItemLabel: PropTypes.string,
}

export default DragDropContext(HTML5Backend)(Container);
