import React from 'react'
import PropTypes from 'prop-types'
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color'

class Picker extends React.Component {
  state = {
    displayColorPicker: false,
    color: {},
  };

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false })
  };

  handleChange = (color) => {
    const { rgb, hex } = color;
    this.setState({ color: rgb})
    this.props.onAnswered({rgb, hex})
  };

  render() {

    const {input, placeholder, label, meta: {  touched,  error  }} = this.props,
    {color} = this.state,
    styles = reactCSS({
      'default': {
        color: {
          width: '36px',
          height: '14px',
          borderRadius: '2px',
          background: `rgba(${color.r }, ${ color.g }, ${ color.b }, ${ color.a })`,
        },
        swatch: {
          padding: '5px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          position: 'inherit',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

    return (
      <div style={{paddingLeft: '15px'}}>
        <div className='form-group'>
          {label && <label className="form-control-label">{label}</label>}
          <input {...input} onFocus={ this.handleClick }
           style={ styles.swatch } placeholder={placeholder?placeholder:label} className="form-control"/>
          {touched && ((error &&   <div className="form-control-feedback">{error}</div>))}
        </div>
        { this.state.displayColorPicker ? <div style={ styles.popover }>
          <div style={ styles.cover } onClick={ this.handleClose }/>
          <SketchPicker color={ this.state.color } onChange={ this.handleChange } />
        </div> : null }
      </div>
    )
  }
}

Picker.propTypes = {
  placeholder: PropTypes.string,
  label: PropTypes.string,
}

export default Picker
