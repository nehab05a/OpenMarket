import React from 'react'
import PropTypes from 'prop-types'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import classnames from 'classnames'

const DatePickerGroup = ({
  label,
  input,
  className,
  placeholder,
  disabled,
  minDate,
  onAnswered,
  meta,
  includeDates,
  required
}) => {
  const onDateChange = input ? input.onChange : onAnswered
  
  return (
      <div className={classnames(`form-group date-picker-group ${className}`, { 'has-danger': meta && (meta.touched && meta.error) })}>
         {label &&
           <span className='d-flex align-items-center'>
             <label className='form-control-label'>
               {label}
             </label>
             {required && <span style={{color: 'crimson', fontSize: '20px', fontWeight: 600, paddingLeft: '5px'}}> * </span>}
           </span>
         }
         <div className="controls">
               <div className="input-group">
                 <DatePicker
                   disabled={disabled}
                   dateForm="MM/DD/YYYY"
                   onChange={(value) => onDateChange(value)}
                   placeholderText={placeholder || label}
                   selected={(input && input.value) ? moment(input.value, "MM/DD/YYYY") : null}
                   showMonthDropdown
                   showYearDropdown
                   minDate={minDate}
                   includeDates={includeDates}
                   className="form-control"
                   calendarClassName="date-picker-calendar"/>
                 <span className="input-group-addon"><i className="fa fa-calendar"></i> </span>
               </div>
         </div>
        {meta && (meta.touched && ((meta.error && <div className="form-control-feedback">{meta.error}</div>)))}
      </div>
    )}

DatePickerGroup.propTypes = {
  label: PropTypes.string
}

export default DatePickerGroup;
