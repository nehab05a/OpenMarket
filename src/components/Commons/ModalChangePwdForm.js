import React from 'react';
import {Field, reduxForm} from 'redux-form'
import {ModalBody, ModalFooter} from 'reactstrap';
import FormGroup from '../Commons/FormGroup'
import {required} from '../Commons/Validators'

const ModalChangePwdForm =(props) => {
    const {handleSubmit, submitting, invalid, submitMessage} = props
    return (
        <form onSubmit={handleSubmit} className="form-pb">
            <ModalBody>
                <Field className="col-md-12" name="oldPassword" type="password" component={FormGroup} label="Current Password" validate={[required]}/>
                <Field className="col-md-12" name="newPassword" type="password" component={FormGroup} label="New Password" validate={[required]}/>
                <Field className="col-md-12" name="confirmPassword" type="password" component={FormGroup} label="Renter Password" validate={[required]}/>
            </ModalBody>
            <ModalFooter>
              {submitMessage && <div className="form-control-feedback" style={{color: 'green'}}>{submitMessage}</div>}
              <button className="btn btn-danger btn-block" type="submit"
                disabled={submitting || invalid}>
                Update
              </button>
            </ModalFooter>
        </form>
    )
}
export default reduxForm({
    form: 'modalChangePwdForm',
})(ModalChangePwdForm)
