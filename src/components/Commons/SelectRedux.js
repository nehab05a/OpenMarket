import React from 'react';
import Select from 'react-select';
import classnames from 'classnames'

const SelectRedux = (props) => {
  const {input, label, className,classLabel, options,  meta: {  touched,  error  }, required} = props
  return (
      <div className={classnames(`form-group ${className}`, { 'has-danger': touched && error })}>
            {label &&
              <span className='d-flex align-items-center'>
                <label className={classnames(`form-control-label ${classLabel}`)}>
                  {label}
                </label>
                {required && <span style={{color: 'crimson', fontSize: '20px', fontWeight: 600, paddingLeft: '5px'}}> * </span>}
              </span>
            }

            <Select value={input.value} options={options} placeholder={label}
              onChange={(value) => input.onChange(value.value)}  onBlur={() => input.onBlur(input.value)} />

          {touched && ((error &&   <div className="form-control-feedback">{error}</div>))}
        </div>
  )};
export default SelectRedux;
