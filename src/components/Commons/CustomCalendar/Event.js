import React from 'react'

const CustomEvent = ({
  event
}) => {
  const
    colorCodes = {
      working  : '#37cbfd',
      available: '#c8c8c8',
      awaiting : '#fdc83f',
      full     : '#1eac74'
    },
    isAvailable = event.available > 0 ? true : false,
    _styles = {
      eventData : {
        display: 'flex',
        justifyContent: 'space-between',
      },
      eventContainer: {
        background: colorCodes[event.shiftStatus],
        color: 'black',
        padding: '2px',
        borderRadius: '5px',
        border: event.hotShift ? '1px solid crimson' : null
      }
    }
  return (
    <div style={_styles.eventContainer}>
      <div style={_styles.eventData}>
        <span>{ event.title }</span>
        {isAvailable &&
          <div>
            <span style={{color: 'crimson'}}>{ event.available }</span>
            <span style={{fontWeight: 500}}>/</span>
            <span style={{color: 'black'}}>{ event.totalRequirement }</span>
          </div>
        }
      </div>
      { event.desc && (':  ' + event.desc) }
    </div>
  )
}

CustomEvent.propTypes = {

}

export default CustomEvent
