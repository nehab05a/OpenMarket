import React, { Component } from 'react'
import PropTypes from 'prop-types'
import autobind from 'react-autobind'
import BigCalendar from 'react-big-calendar'
import { CustomToolbar } from './Toolbar'
import CustomEvent from './Event'
import moment from 'moment'

BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

class CalendarViews extends Component{
  constructor(props){
    super(props)

    this.state = {
      current : props.selectedMonth
    }
    autobind(this)
  }

  componentWillMount() {
    const { current } = this.state
    this._handleCurrentMonth(current)
  }

  render(){
    const { viewBy, selectedMonth, eventLists, onDaySelection } = this.props,
    Get_Toolbar = toolbar => (
      <CustomToolbar
        {...toolbar}
        {...this.props}
        handleCurrent={this._handleCurrentMonth}
      />);

    return(
      <BigCalendar
        selectable
        views={['week', 'month']}
        dayFormat={'ddd d/MM'}
        ref={c => { this.bigCalendar = c } }
        culture="en-GB"
        defaultView={viewBy}
        style={{height:'720px'}}
        events={eventLists}
        defaultDate={selectedMonth || new Date()}
        onSelectEvent={event => onDaySelection(event.start)}
        onSelectSlot={slotInfo => onDaySelection(slotInfo.start)}
        components={{
          event: CustomEvent,
          toolbar: Get_Toolbar
        }}
      />
    )
  }

  _handleCurrentMonth(current) {
    this.setState({
      current,
    }, () => this._handleSelectedOffset(current))
  }

  _handleSelectedOffset(current) {
    const offset =  moment(current).format('MM') - moment(Date.now()).format('MM')
    this.props.getData({
      viewType : 'month',
      offset
    })
  }
}

CalendarViews.propTypes = {
  viewBy: PropTypes.string.isRequired,
}

export default CalendarViews
