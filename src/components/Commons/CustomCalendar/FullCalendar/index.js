import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Month from './Month'
import moment from 'moment'
import autoBind from 'react-autobind'
import { Toolbar } from '../Toolbar'
import _ from 'lodash'

class FullCalendar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      monthArray: [],
      current: Date.now()
    }
    autoBind(this)
  }

  componentWillMount() {
    const { current } = this.state
    this._setMonthRange(current)
  }

  render() {
    const
      { onAnswered, toggleCalendar, searchUpdated,
        eventLists, filters, selectedBranch, branchSelection, resetFilter } = this.props,
      { monthArray, current } = this.state,
      goToYearView = () => toggleCalendar('year'),
      goToMonthView = () => toggleCalendar('month'),
      goToWeekView = () => toggleCalendar('week')

    return (
      <div>
        <Toolbar
          searchUpdated={searchUpdated}
          goToBack={this._goToBack}
          goToNext={this._goToNext}
          goToCurrent={this._goToCurrent}
          goToYearView={goToYearView}
          goToMonthView={goToMonthView}
          goToWeekView={goToWeekView}
          getData={this._getCalendarData}
          date={current}
          filters={filters}
          selectedBranch={selectedBranch}
          branchSelection={branchSelection}
          resetFilter={resetFilter}
        />
        {
          monthArray.map((monthRow, i) => (
            <div key={`row-${i}`} style={{paddingTop: '20px'}}
            className='d-flex justify-content-center align-items-center'>
            {monthRow.map((date, i) => (
              <Month
                key={ `month-${i}` }
                date={ date } mods={[
                  {
                    component: 'month',
                    events: {
                      onClick: (date, e) => onAnswered(date)
                    }
                  },
                  ...eventLists]}
                monthNameFormat='MMM'
              />
            ))}
            </div>
          ))
        }
      </div>
    )
  }

  _goToNext() {
    const
      { current } = this.state,
      nextYear = moment(current).add(1, 'year')
    this._setMonthRange(nextYear.toDate())
  }

  _goToBack() {
    const
      { current } = this.state,
      prevYear = moment(current).subtract(1, 'year')
    this._setMonthRange(prevYear.toDate())
  }

  _goToCurrent() {
    this._setMonthRange(Date.now())
  }

  _setMonthRange(current) {
    this.setState({
      current,
      monthArray: _.chunk(this._getMonthRange(current), 3)
    }, () => this._getCalendarData(current))
  }

  _getCalendarData(current) {
    const offset =  moment(current || this.state.current).format('YYYY') - moment(Date.now()).format('YYYY')
    this.props.getData({
      viewType : 'year',
      offset
    })
  }

  _getMonthRange (current) {
    const
      start = moment(current).startOf('year'),
      end = moment(current).endOf('year'),

      focus = moment(start).startOf('month'),
      size = end.diff(start, 'month') + 1;

    return Array(size).fill(0).map((n, i) => focus.clone().add(n + i, 'months'));
  }
}

FullCalendar.propTypes = {
  eventLists : PropTypes.array,
  onAnswered : PropTypes.func,
  toggleCalendar : PropTypes.func,
  toggleFilter : PropTypes.func,
  showFilter : PropTypes.bool,
  searchUpdated : PropTypes.func,
  startDate : PropTypes.instanceOf(Date),
  endDate :  PropTypes.instanceOf(Date),
}

export default FullCalendar
