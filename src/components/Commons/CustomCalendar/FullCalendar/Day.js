import React, { PropTypes } from 'react'
import classnames from 'classnames'
import _ from 'lodash'
import { getMods } from './util'

const clsPrefix = 'rc-Day'

const renderHeader = (props) => {
  if (!props.dayHeader) {
    return null;
  }

  return (
    <header className={`${clsPrefix}-Day-header`}>
      { props.date.format(props.dayHeaderFormat) }
    </header>
  );
};

const renderAgenda = (props) => {
  if (!props.dayAgenda) {
    return null;
  }

  return (
    <div key="agenda" className={`${clsPrefix}-Day-agenda`}>
      { props.children }
    </div>
  );
};

const Day = (props) => {
  const clsPrefix = 'rc-Day';
  const { date, mods, outside } = props;
  const modifiers = getMods(mods, date, clsPrefix, 'day');

  let clsMods, events;

  if (modifiers) {
    clsMods = modifiers.clsMods;
    events = modifiers.events;
  }

  const clsDay = classnames(clsPrefix, { 'rc-Day--outside': outside }, clsMods);
  const styles = {
    badge : {
        display         : 'flex',
        position        : 'absolute',
        top             : -5,
        right           : -5,
        color           : 'crimson',
        fontWeight      : 400,
        height          : '14px',
        width           : '14px',
        borderRadius    : '50%',
        alignItems      : 'center',
        justifyContent  : 'center',
      },

      badgeIcon : {
        backgroundColor : 'transparent',
      },

      icon : {
        position        : 'relative',
        display         : 'flex',
        borderRadius    : '50%',
        alignItems      : 'center',
        justifyContent  : 'center',
      },

  }
  const
    selected = _.filter(mods, item => item.date.format('DD/MM/YYYY') === date.format('DD/MM/YYYY')),
    badgeValue = !_.isEmpty(selected) && _.sumBy(selected, 'badgeValue')

  return (
    <div style={styles.icon}>
      <div className={ clsDay } { ...events }>
        { renderHeader(props) }
        { date.format(props.dayFormat) }
        { renderAgenda(props) }
      </div>
      {
        _.includes(clsMods, `${clsPrefix}--available`) &&
        <div style={styles.badge}>
          <span style={styles.badgeIcon}>{badgeValue}</span>
        </div>
      }
    </div>
  );
};

Day.propTypes = {
  date: React.PropTypes.object.isRequired,
  dayAgenda: React.PropTypes.bool,
  dayHeader: React.PropTypes.bool,
  dayHeaderFormat: React.PropTypes.string,
  dayFormat: React.PropTypes.string,
  mods: PropTypes.array
};

Day.defaultProps = {
  dayAgenda: false,
  dayHeader: false,
  dayHeaderFormat: 'MMM Do',
  dayFormat: 'D'
};

export default Day;
