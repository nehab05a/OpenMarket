import React, { Component } from 'react'
import FilterContent from './FilterContent'
import autoBind from 'react-autobind'

class CalendarFilter extends Component {
  constructor() {
    super();

    this.state = {
      showFilter : false,
    }
    autoBind(this)
  }

  toggleFilter() {
    this.setState({ showFilter : !this.state.showFilter})
  }

  render() {
    const { showFilter } = this.state
    return(
      <FilterContent
        {...this.props}
        showFilter={showFilter}
        toggleFilter={this.toggleFilter}
      />
    )
  }
}

export default CalendarFilter
