import React from 'react'
import { Dropdown, DropdownMenu, DropdownItem, Modal, ModalHeader, ModalBody } from 'reactstrap'
import OrderedList from '../../OrderedList'

const FilterContent = ({
  filters,
  resetFilter,
  showFilter,
  toggleFilter,
  styles,
  getData,
  selectedBranch,
}) => {
  const
    getFilterComponent = (item) => {
      switch (item.component) {
        case 'radioButton':
          return (
            <span className="control-switch">
              <span className="switch switch-3d switch-primary">
                <input type="checkbox" onChange={e => item.setFilter(e.target.value)} checked={item.isSelected || false} className="switch-input"/>
                <span className="switch-label" onClick={() => item.setFilter(!item.isSelected)}></span>
                <span className="switch-handle" onClick={() => item.setFilter(!item.isSelected)}></span>
              </span>
            </span>
          )
        case 'listSelector':
          return (<span onClick={item.toggleSelection} style={{color: '#20a8d8'}}>
            {selectedBranch ?
              <span onClick={item.toggleSelection}>
                {selectedBranch}
              </span>
            : 'All'}
            </span>
          )
        default:

      }
    };

  return (
    <div>
      <Dropdown isOpen={showFilter} toggle={toggleFilter}>
          <a onClick={toggleFilter}>
              <i className='fa fa-filter' style={styles.navItems}></i>
          </a>
          <DropdownMenu className='dropdown-menu-right' style={{width: '200px'}}>
            {filters && filters.map((item, key) => (
              <DropdownItem key={key}>
                 <span className='d-flex justify-content-between'>
                   <span style={{color: 'black'}}>{item.filterLabel}</span>
                   <div>{getFilterComponent(item)}</div>
                 </span>
                 {item.component === 'listSelector' &&
                 <Modal isOpen={item.showSelection} style={{width: '400px'}}>
                   <ModalHeader toggle={item.toggleSelection}>{item.filterLabel}</ModalHeader>
                   <ModalBody>
                     <OrderedList
                       list={item.filterData}
                       values={item.selectedValue}
                       multiSelect={item.multiSelect}
                       onAnswered={(value) => item.setFilter(value, () => {
                         item.toggleSelection()
                         getData()
                       })}
                     />
                   </ModalBody>
                 </Modal>}
              </DropdownItem>
            ))}
            {resetFilter &&
            <DropdownItem>
              <center style={{color: '#20a8d8'}} onClick={resetFilter}>Reset Filter</center>
            </DropdownItem>}
          </DropdownMenu>
      </Dropdown>
    </div>
  )
}

export default FilterContent
