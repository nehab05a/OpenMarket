import React from 'react'
import PropTypes from 'prop-types'
import { Button, ButtonGroup, Modal, ModalHeader, ModalBody } from 'reactstrap'
import SearchInput from 'react-search-input'
import moment from 'moment'
import OrderedList from '../OrderedList'
import Filter from './Filter'

export const Toolbar = ({
  searchUpdated,
  goToYearView,
  goToWeekView,
  goToMonthView,
  goToBack,
  goToNext,
  goToCurrent,
  label,
  filters,
  date,
  getData,
  selectedBranch,
  branchSelection,
  resetFilter
}) => {
  const
    _styles = {
      navItems : {
        paddingRight : '20px',
        fontSize : '25px',
        color : '#b0bec5'
      },
      searchBox : {
        width: '200px',
        marginRight: '15px'
      },
      header : {
        borderBottom: '1px solid #cfd8dc',
        padding: '10px'
      },
    }

  return (
    <div style={_styles.toolBar}>
      <div className='d-flex justify-content-between align-items-center'  style={_styles.header}>
        <ButtonGroup>
          <Button color='secondary' style={{borderTopLeftRadius: '20px', borderBottomLeftRadius: '20px'}} onClick={goToYearView}>Year</Button>
          <Button color='secondary' onClick={goToMonthView}>Month</Button>
          <Button color='secondary' style={{borderTopRightRadius: '20px', borderBottomRightRadius: '20px'}} onClick={goToWeekView}>Week</Button>
        </ButtonGroup>
        <div className='d-flex align-items-center'>
          <SearchInput className='list-search' style={_styles.searchBox} onChange={searchUpdated} />
          {filters && <Filter  filters={filters}
                               resetFilter={resetFilter}
                               getData={getData}
                               selectedBranch={selectedBranch}
                               styles={_styles}/>}
        </div>
      </div>
      <div className='d-flex justify-content-between align-items-center' style={_styles.header}>
        {selectedBranch && <div>
          <span style={{color: '#20a8d8',fontSize: '16px'}} onClick={branchSelection ? branchSelection.toggleSelection : null}>
            {selectedBranch}
          </span>
          {branchSelection && <Modal isOpen={branchSelection.showSelection} style={{width: '400px'}}>
            <ModalHeader toggle={branchSelection.toggleSelection}>Branch</ModalHeader>
            <ModalBody>
              <OrderedList
                list={branchSelection.filterData}
                values={branchSelection.selectedValue}
                multiSelect={branchSelection.multiSelect}
                onAnswered={(value) => branchSelection.setFilter(value, () => {
                  branchSelection.toggleSelection()
                  getData()
                })}
              />
            </ModalBody>
          </Modal>}
        </div>}
        <span style={{color: '#20a8d8',fontSize: '20px'}}>{label || moment(date).format('YYYY')}</span>
        <ButtonGroup>
          <Button color='secondary' onClick={goToBack}>
            <i className='fa fa-caret-left'/>
          </Button>
          <Button color='secondary' onClick={goToCurrent}
            style={{margin: '0px 10px'}}
          >Today</Button>
          <Button color='secondary' onClick={goToNext}>
            <i className='fa fa-caret-right'/>
          </Button>
        </ButtonGroup>
      </div>
    </div>
  )
}

Toolbar.propTypes = {
  searchUpdated : PropTypes.func,
  toggleFilter : PropTypes.func,
  goToYearView : PropTypes.func,
  goToWeekView : PropTypes.func,
  goToMonthView : PropTypes.func,
  showFilter : PropTypes.bool,
  goToBack : PropTypes.func,
  goToNext : PropTypes.func,
  goToCurrent : PropTypes.func,
}

export const CustomToolbar = (toolbar) => {
  const
    goToWeekView = () => {
      toolbar.onViewChange('week');
    },

    goToMonthView = () => {
      toolbar.onViewChange('month');
    },

    goToYearView = () => {
      toolbar.onToggleYearView('year')
    },

    goToBack = () => {
      const newDate = moment(toolbar.date).subtract(1, toolbar.view)
      toolbar.onNavigate('PREV');
      toolbar.handleCurrent(newDate.toDate())
    },

    goToNext = () => {
      const newDate = moment(toolbar.date).add(1, toolbar.view)
      toolbar.onNavigate('NEXT');
      toolbar.handleCurrent(newDate.toDate())
    },

    goToCurrent = () => {
      toolbar.onNavigate('TODAY');
      toolbar.handleCurrent(new Date())
    }

  return (
    <Toolbar
      searchUpdated={toolbar.searchUpdated}
      filters={toolbar.filters}
      label={toolbar.label}
      goToYearView={goToYearView}
      goToWeekView={goToWeekView}
      goToMonthView={goToMonthView}
      goToBack={goToBack}
      goToNext={goToNext}
      goToCurrent={goToCurrent}
      selectedBranch={toolbar.selectedBranch}
      branchSelection={toolbar.branchSelection}
      getData={() => toolbar.handleCurrent(toolbar.date)}
      resetFilter={toolbar.resetFilter}
    />
  )
}
