import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal } from 'reactstrap'
import CalendarViews from './CalendarViews'
import FullCalendar from './FullCalendar'
import autoBind from 'react-autobind'
import moment from 'moment'

class CustomCalendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      viewCalendarBy: 'year'
    }
    autoBind(this)
  }

  toggleCalendar(viewBy) {
    this.setState({ viewCalendarBy: viewBy })
  }

  render() {
    const
      { viewCalendarBy, selectedMonth } = this.state,
      { eventList, filters, selectedBranch, getShiftsForDay, sideComponentList, branchSelection, resetFilter } = this.props

    return (
      <div>
        <div>
          {
            viewCalendarBy === 'year'
            ? <div className='card card-default'>
                <FullCalendar
                  toggleCalendar={this.toggleCalendar}
                  filters={filters}
                  onAnswered={this._goToSelectedMonth}
                  searchUpdated={this._searchUpdated}
                  eventLists={eventList}
                  getData={this._getData}
                  viewBy={viewCalendarBy}
                  selectedBranch={selectedBranch}
                  branchSelection={branchSelection}
                  resetFilter={resetFilter}
                />
              </div>
            : <div className='d-flex'>
                <div style={{flex: 3}} className='card card-default'>
                  <CalendarViews
                      onToggleYearView={this.toggleCalendar}
                      viewBy={viewCalendarBy}
                      filters={filters}
                      eventLists={eventList}
                      onAnswered={() => {}}
                      selectedMonth={selectedMonth}
                      searchUpdated={this._searchUpdated}
                      getData={this._getData}
                      selectedBranch={selectedBranch}
                      branchSelection={branchSelection}
                      onDaySelection={getShiftsForDay}
                      resetFilter={resetFilter}
                  />
                </div>
                {
                  this.props.sideComponent &&
                  <div style={{marginLeft: '20px', flex: 1}} className='card card-default'>
                    {<this.props.sideComponent list={sideComponentList} refreshView={() => this.toggleCalendar('year')}/>}
                  </div>
                }
              </div>
          }
        </div>
        <Modal style={{ width: '420px'}} isOpen={false}>
          {this.props.component && <this.props.component />}
        </Modal>
      </div>
    )
  }

  _goToSelectedMonth(selectedMonth){
    this.setState({
      selectedMonth: moment(selectedMonth).toDate()
    }, () => this.toggleCalendar('month'))
  }

  _getData(values) {
    this.props.getData(values)
  }

  _searchUpdated (term) {
    this.setState({searchTerm: term})
  }
}

CustomCalendar.propTypes = {
  viewCalendarBy: PropTypes.string,
  eventList: PropTypes.array,
  getData: PropTypes.func
}

export default CustomCalendar;
