import React from 'react'
import PropTypes from 'prop-types'
import {ModalFooter, Button} from 'reactstrap'
import _ from 'lodash'

const OrderedListMarkup = ({
  list,
  onSelectElement,
  selectedValues,
  selectedIcon,
  nonSelectedIcon,
  showIcons,
  listInitialIndex,
  onChangeIndex,
  onConfirm,
  onItemClicked,
  onCancel,
  cancelText,
  confirmText,
  multiSelect,
  markFavourite
}) => {
  let listRef = null;
  const
    _styles = {
      container : {
        height : '380px',
      },
      content : {
        padding: '10px',
        overflow: 'auto',
        height: '380px',
        width: '100%'
      },
      sideSelector : {
        fontFamily: 'monospace',
        fontSize: '12px',
        lineHeight: '1',
        marginLeft: '-18px',
        zIndex: '100',
      },
      listHeader : {
        backgroundColor: '#f2f2f2',
        fontSize: '16px',
        fontWeight: '500',
        margin: '5px 0',
      },
      listElement : {
        fontSize: '16px',
        padding: '5px 0',
      },
      favIcon : {
        color : '#20a8d8',
        fontSize: '20px',
        marginRight: '20px'
      },
      selectedItem : {
        color : '#e6e6e6',
        fontSize: '20px',
        marginRight: '20px'
      },
      noMatch: {
        width: '100%',
        color:'#20a8d8',
        paddingTop: '40px',
        textAlign:'center'
      },
    },
    scrollTo = (index) => {
      if (index != null && listRef != null ) listRef.setScroll(listRef.getSpaceBefore(index));
    },
    alphas = _.range(
      'a'.charCodeAt(0),
      'z'.charCodeAt(0)+1
    ),
    letters = _.map(alphas, a => String.fromCharCode(a)),
    letterSideBar = (
      <div className='d-flex flex-column justify-content-center' style={_styles.sideSelector}>
        {_.map(letters, (alphabet, index) => (<div style={{cursor: 'pointer'}}
          className='text-primary' key={alphabet} onClick={() => scrollTo(index)}>
            {_.capitalize(alphabet)}
          </div>))}
      </div>
    );

  return (
      <div>
        <div className='d-flex'>
          {_.isEmpty(list) ?
            <div style={_styles.noMatch}>No record found</div>
          : <div style={_styles.content} ref={c => listRef = c} id='list-scrollbar'>
              {
                list.map((value, key) => (
                  <div  key={key} style={_styles.listContainer}>
                    <div style={_styles.listHeader}>{_.capitalize(value.key)}</div>
                    {value.items.map((element,index) => {
                      const isSelected = selectedValues.some(item => Number(item.id) === Number(element.id))
                      return (<div key={index} className='d-flex justify-content-between align-items-center' style={_styles.listElement} >
                        <span onClick={() => markFavourite ? onItemClicked(element) : onSelectElement(element)} style={{cursor:'auto', width: '100%'}}>{_.startCase(element.displayName)}</span>
                        {
                          showIcons ?
                          <i className={isSelected ? selectedIcon : nonSelectedIcon}
                          style={isSelected ? _styles.favIcon : _styles.selectedItem} onClick={() => onSelectElement(element)}></i>
                        : isSelected ?
                          <i className='fa fa-check' style={isSelected ? _styles.favIcon : _styles.selectedItem}/>
                        : null
                        }
                      </div>)
                    })}
                  </div>
                ))
              }
          </div>}
          {letterSideBar || null}
        </div>
        {(multiSelect && !markFavourite) &&
          <ModalFooter className='d-flex  justify-content-center'>
            <Button className='col-md-3' onClick={onCancel} color="secondary">{cancelText || `Cancel`}</Button>
            <Button type='submit' className='col-md-3' onClick={onConfirm} color="primary">{confirmText || `Apply`}</Button>
          </ModalFooter>}
      </div>
  )
};

OrderedListMarkup.propTypes = {
  list : PropTypes.array.isRequired,
  listInitialIndex : PropTypes.number,
  showIcons : PropTypes.bool,
  selectedIcon : PropTypes.string,
  nonSelectedIcon : PropTypes.string,
  onSelectElement : PropTypes.func.isRequired,
  onChangeIndex : PropTypes.func.isRequired,
  selectedValues : PropTypes.array,
  onItemClicked : PropTypes.func,
  confirmText : PropTypes.string,
  cancelText : PropTypes.string,
}

export default OrderedListMarkup;
