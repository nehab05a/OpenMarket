import React, { Component } from 'react'
import PropTypes from 'prop-types'
import OrderedListMarkup from './OrderedListMarkup'
import SearchInput, {createFilter} from 'react-search-input'
import autoBind from 'react-autobind'
import {filter} from 'lodash'

class OrderedList extends Component {
  constructor(props) {
    super(props);

    this.state = {
        values : this.props.values || [],
        listIndex : 0,
        searchTerm : ''
    }
    autoBind(this);
  }

  componentWillReceiveProps(nextProps) {
    (this.props.values !== nextProps.values) && this.setState({values: nextProps.values})
  }

  render(){
    const
      { values, listIndex } = this.state,
      {
        list,
        selectedIconClass,
        nonSelectedIconClass,
        showIcons,
        cancelText,
        confirmText,
        multiSelect,
        markFavourite
      } = this.props,
      filteredItems = filter(list, createFilter(this.state.searchTerm, ['key', 'items.displayName']));

    return(
        <div>
          <SearchInput className='list-search' style={{marginBottom: '10px'}} onChange={this._searchUpdated} />
          <OrderedListMarkup
            selectedValues={values}
            listInitialIndex={listIndex}
            list={filteredItems}
            multiSelect={multiSelect}
            markFavourite={markFavourite}
            showIcons={showIcons}
            selectedIcon={selectedIconClass}
            nonSelectedIcon={nonSelectedIconClass}
            onSelectElement={this._onOptionSelected}
            onChangeIndex={this._onChangeIndex}
            onItemClicked={this._onExclusiveSelect}
            onConfirm={this._onConfirm}
            onCancel={this._onCancel}
            cancelText={cancelText}
            confirmText={confirmText}
          />
        </div>
      )
  }

  _onExclusiveSelect(value) {
    this.props.exclusiveSelect(value)
  }

  _onOptionSelected(value) {
    const
      {multiSelect, onAnswered, markFavourite, updateFavourite} = this.props,
      existingValues = this.state.values;
    let values = [];
    if(multiSelect) {
      if(markFavourite) {
        value.isSelected = !value.isSelected
      }
      values = existingValues.some(item => item.id === value.id) ?
        existingValues.filter((val) => val.id !== value.id) : [...existingValues, value]

      if(updateFavourite) updateFavourite(value);

    } else {
      values = [value];
      if(onAnswered) onAnswered(values);
    }
    this.setState({values});
  }

  _onConfirm() {
    const
      {multiSelect, onAnswered} = this.props;

    if(onAnswered && multiSelect) onAnswered(this.state.values);
  }

  _onCancel() {
    this.props.onCancel();
  }

  _onChangeIndex(listIndex) {
    this.setState({listIndex})
  }

  _searchUpdated (term) {
    this.setState({searchTerm: term})
  }
}

OrderedList.propTypes = {
  list : PropTypes.object,
  selectedIconClass : PropTypes.string,
  nonSelectedIconClass : PropTypes.string,
  values : PropTypes.array,
  multiSelect : PropTypes.bool,
  markFavourite : PropTypes.bool,
  onAnswered : PropTypes.func,
  updateFavourite : PropTypes.func,
  exclusiveSelect : PropTypes.func,
  showIcons : PropTypes.bool,
}

OrderedList.defaultProps = {
  markFavourite : false
}

export default OrderedList;
