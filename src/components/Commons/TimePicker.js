import React from 'react';
import TimePicker from 'rc-time-picker';
import classnames from 'classnames'
import 'rc-time-picker/assets/index.css'
import moment from 'moment'

const SelectRedux = (props) => {
  const {input, label, className, disabled, classLabel, meta: {  touched,  error  }} = props
  return (
      <div className={classnames(`form-group ${className}`, { 'has-danger': touched && error })}>
            {label && <label className={classnames(`form-control-label ${classLabel}`)}>{label}</label>}

            <TimePicker {...input}
              format='h:mm a'
              use12Hours
              showSecond={false}
              style={{width: '260px'}}
              placeholder={label}
              defaultOpenValue={moment()}
              disabled={disabled}
              hideDisabledOptions
              onBlur={(e) => input.onBlur(e.target.value)}
            />

          {input.value && ((error && <div className="form-control-feedback" style={{color: 'red'}}>{error}</div>))}
        </div>
  )};
export default SelectRedux;
