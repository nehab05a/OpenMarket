// This component handles the App template used on every page.
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { Switch } from 'react-router'
import Menu from '../components/Home/Menu'
import MenuCompany from '../components/Company/MenuCompany'
import Login from '../components/Auth/Login';
import Register from '../components/Auth/Register';
import Mail from '../components/Auth/Mail';
import VerifyAccount from '../components/Auth/VerifyAccount';
import ChangePassword from '../components/Auth/ChangePassword';
import Page404 from '../components/Simple/Page404'
import PrivateRoute from '../components/Auth/PrivateRoute';
import PublicRoute from '../components/Auth/PublicRoute';
import SetupUser from '../components/Company/SetupUser'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import _ from 'lodash'

const App = ({authenticated, checked, user}) => {
  const
    isShiftManager = user.roles && _.includes(user.roles, 'ROLE_CREATE_SHIFT'),
    isAdmin = user.roles && _.includes(user.roles, 'ROLE_ORG_ADMIN', 'ROLE_ADMIN')
  if(isShiftManager === false) _.remove(Menu, (item) => item.path === '/shiftsmanager')

  return (
    <Router>
        {checked &&
        <div>
            <Switch>
               {Menu.map((route, index) => (
              <PrivateRoute exact={route.exact} authenticated={authenticated} path={route.path} component={route.main} key={index}/>
               ))}
               <PublicRoute authenticated={authenticated} path="/login" component={Login}/>
              <Route path="/register" component={Register}/>
              <Route path="/mail" component={Mail}/>
              <Route path="/verify-account" component={VerifyAccount}/>
              <Route path="/change" component={ChangePassword}/>
              {isAdmin && <Route path="/setup/user/:id" component={SetupUser}/>}
              {isAdmin && MenuCompany.map((route, index) => (
                <PrivateRoute exact={route.exact} authenticated={authenticated} path={route.path} component={route.main} key={index}/>
              ))}
              <Route component={Page404}/>
            </Switch>
        </div>
}
    </Router>
)};

const {bool} = PropTypes;

App.propTypes = {
    authenticated: bool.isRequired,
    checked: bool.isRequired
};

const mapState = ({session}) => ({
  checked: session.checked,
  authenticated: session.authenticated,
  user: session.user
});

export default connect(mapState)(App);
