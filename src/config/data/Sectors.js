export default [{
    "value": "Accountancy, Financial and Legal",
    "label": "Accountancy, Financial and Legal"
}, {
    "value": "Architecture and Design",
    "label": "Architecture and Design"
}, {
    "value": "Business Services",
    "label": "Business Services"
}, {
    "value": "Charity",
    "label": "Charity"
}, {
    "value": "Coaching, Training & Education",
    "label": "Coaching, Training & Education"
}, {
    "value": "Education",
    "label": "Education"
}, {
    "value": "Engineers, Trades and Construction",
    "label": "Engineers, Trades and Construction"
}, {
    "value": "Health and Safety Consultancy",
    "label": "Health and Safety Consultancy"
}, {
    "value": "IT and Technology",
    "label": "IT and Technology"
}, {
    "value": "Management Consultancy",
    "label": "Management Consultancy"
}, {
    "value": "Marketing, Sales and Events",
    "label": "Marketing, Sales and Events"
}, {
    "value": "Medical Professionals and Services",
    "label": "Medical Professionals and Services"
}, {
    "value": "Photography and Creative Services",
    "label": "Photography and Creative Services"
}, {
    "value": "Recruitment",
    "label": "Recruitment"
}, {
    "value": "Retail and Services",
    "label": "Retail and Services"
}, {
    "value": "Surveying & Estate Agency",
    "label": "Surveying & Estate Agency"
}, {
    "value": "Other",
    "label": "Other"
}]
