export default [{
    "value": "1,234.56",
    "label": "1,234.56"
}, {
    "value": "1 234,56",
    "label": "1 234,56"
}, {
    "value": "1’234,56",
    "label": "1’234,56"
}, {
    "value": "1,234,56",
    "label": "1,234,56"
}]
