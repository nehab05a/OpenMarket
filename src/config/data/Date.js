export const Days = [
    {
        value: 'Sun',
        label: 'Sunday'
    }, {
        value: 'Mon',
        label: 'Monday'
    }, {
        value: 'Tue',
        label: 'Tuesday'
    }, {
        value: 'Wed',
        label: 'Wednesday'
    }, {
        value: 'Thu',
        label: 'Thursday'
    }, {
        value: 'Fri',
        label: 'Friday'
    }, {
        value: 'Sat',
        label: 'Saturday'
    }
];
export const Months = [
    {
        value: 'Jan',
        label: 'January'
    }, {
        value: 'Feb',
        label: 'February'
    }, {
        value: 'Mar',
        label: 'March'
    }, {
        value: 'Apr',
        label: 'April'
    }, {
        value: 'May',
        label: 'May'
    }, {
        value: 'Jun',
        label: 'June'
    }, {
        value: 'Jul',
        label: 'July'
    }, {
        value: 'Aug',
        label: 'August'
    }, {
        value: 'Sep',
        label: 'September'
    }, {
        value: 'Oct',
        label: 'October'
    }, {
        value: 'Nov',
        label: 'November'
    }, {
        value: 'Dec',
        label: 'December'
    }
];
export const FormatDates = [
    {
        label: 'dd-MM-yyyy',
        value: 'dd-MM-yyyy'
    },
    {
        label: 'dd/MM/yyyy',
        value: 'dd/MM/yyyy'
    },
    {
        label: 'dd.MM.yyyy',
        value: 'dd.MM.yyyy'
    },
    {
        label: 'yy/MM/dd',
        value: 'yy/MM/dd'
    },
    {
        label: 'd.M.yyyy',
        value: 'd.M.yyyy'
    },
    {
        label: 'MMMM dd, yyyy',
        value: 'MMMM dd, yyyy'
    },
    {
        label: 'yyyy/MM/dd',
        value: 'yyyy/MM/dd'
    },
    {
        label: 'yyyy-MM-dd',
        value: 'yyyy-MM-dd'
    },
    {
        label: 'd/M/yy',
        value: 'd/M/yy'
    }
  ];
