import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import App from './config/App';
import configureStore from './store/configureStore';
import { sessionService } from 'redux-react-session';
import { ConnectedRouter } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

const store = configureStore();
const history = createHistory();

sessionService.initSessionService(store);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)
