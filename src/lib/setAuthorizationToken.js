import axios from 'axios';
import jwtDecode from 'jwt-decode';
import configureStore from '../store/configureStore';
import { logout } from '../actions'

export default function setAuthorizationToken(token) {
    const accessToken = token || sessionStorage.getItem('team-token')
    accessToken ?
      validateToken(accessToken)
      ? axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`
      : configureStore().dispatch(logout())
    : delete axios.defaults.headers.common['Authorization'];
}

export function validateToken (accessToken) {
  const
    decode = accessToken && jwtDecode(accessToken),
    isExpired = decode && decode.exp < new Date() / 1000;

  return !isExpired;
};
