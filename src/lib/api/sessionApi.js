import axios from 'axios';
import setAuthorizationToken from '../setAuthorizationToken'

//axios.defaults.baseURL = 'http://dev.work.management/';
// axios.defaults.baseURL = 'http://10.11.13.132/';
// axios.defaults.baseURL = 'http://stage.work.management/';

// const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));
axios.defaults.baseURL = (() => {
    console.log('window.teamapp => ', window.teamapp);
    switch (window.teamapp.env) {
          case 'dev-local':
          {
              return 'http://localhost:9677/';
          }
          case 'local':
            {
                return 'http://10.11.13.132/';
            }
          case 'local-nitor':
            {
                return 'http://10.11.13.132:8091/';
            }
        case 'dev':
            {
                return 'http://dev.work.management/';
            }
        case 'stage':
            {
                return 'http://stage.work.management/';
            }
        case 'prod':
            {
                return 'http://api.work.management/';
            }
        default:
            {
                return 'http://10.11.13.132:8091/';
            }
    }
})();

export const login = (username, password) => {
    return axios.post('api/login', { username, password });
}

export const getUser = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/loginForAPI/auth')
}

export const fetchDetails = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/customerREST/accountSettings')
}

export const getCompanyLogo = () => {
    //return axios.get('image/showCustomerLogo')
}

export const uploadLogo = (data, token) => {
  const
    { id, customerLogo } = data,
    url = 'api/CustomerREST/uploadCustomerLogo',
    config = {
        headers: {
            'Authorization': `Bearer ${token}`,
            'content-type': 'multipart/form-data'
        }
    },
    formData = new FormData();
  formData.append('customerLogo', customerLogo)
  formData.append('id', id)

  return  axios.post(url, formData, config)
}

export const uploadUserPhoto = (data, token) => {
  const
    { id, userPhoto } = data,
    url = 'api/UserREST/uploadUserPhoto',
    config = {
        headers: {
            'Authorization': `Bearer ${token}`,
            'content-type': 'multipart/form-data'
        }
    },
    formData = new FormData();
  formData.append('userPhoto', userPhoto)
  formData.append('id', id)

  return  axios.post(url, formData, config)
}

export const uploadBranchPhoto = (data, token) => {
  const
    { id, branchPhoto } = data,
    url = 'api/BranchREST/uploadBranchPhoto',
    config = {
        headers: {
            'Authorization': `Bearer ${token}`,
            'content-type': 'multipart/form-data'
        }
    },
    formData = new FormData();
  formData.append('branchPhoto', branchPhoto)
  formData.append('id', id)

  return  axios.post(url, formData, config)
}

export const uploadDepartmentPhoto = (data, token) => {
  const
    { id, departmentPhoto } = data,
    url = 'api/DepartmentREST/uploadDepartmentPhoto',
    config = {
        headers: {
            'Authorization': `Bearer ${token}`,
            'content-type': 'multipart/form-data'
        }
    },
    formData = new FormData();
  formData.append('departmentPhoto', departmentPhoto)
  formData.append('id', id)

  return  axios.post(url, formData, config)
}

export const updateDetails = (values, token) => {
    setAuthorizationToken(token);
    return axios.post('api/customerREST/createAccountSettings', values)
}

export const updateSettings = (values, token) => {
    setAuthorizationToken(token);
    return axios.post('api/customerREST/createAppSettings', values)
}

export const register = (values, token) => {
    setAuthorizationToken(token);
    return axios.post('register', values);
};

//Change/Forgot Password
export const forgotPassword = (mail) => {
    return axios.get(`forgot?username=${mail}`);
};

export const changePassword = (passwords, token) => {
    setAuthorizationToken(token);
    return axios.post('api/userREST/changePassword', passwords);
};

export const newPassword = (values, token) => {
    const { username, newPassword } = values
    return axios.get(`/new-password?username=${username}&&newPassword=${newPassword}`);
};

export const verifyCode = (data) => {
    const { userName, verificationCode, mode } = data,
    url = `verify-email?mailId=${userName}&&verificationCode=${verificationCode}&&mode=${mode}`
    return axios.get(url);
};

export const fetchUserDetails = (id, token) => {
    setAuthorizationToken(token);
    return axios.get(`api/userREST/getUserById?id=${id}`)
};

export const fetchUsersList = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/userREST/getAllUser')
};

export const fetchManagersList = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/userREST/getManagersList')
};

export const fetchAllManagers = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/userREST/getAllManagers')
};

export const fetchLeaveManagersList = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/UserREST/getManagers?managerType=ROLE_AUTHORIZE_LEAVE')
};

export const fetchExpenseManagersList = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/UserREST/getManagers?managerType=ROLE_AUTHORISE_EXPENSES')
};

export const fetchShiftManagersList = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/UserREST/getManagers?managerType=ROLE_MODULE_SHIFT_MANAGER')
};

//Locations/Branch
export const fetchLocations = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/branchREST/getAllBranch')
};

export const updateLocation = (values, token) => {
    setAuthorizationToken(token);
    const
        url = values.id ?
        `api/branchREST/updateBranch?id=${values.id}` : 'api/branchREST/addBranch'
    values.id && delete values['id']
    return axios.post(url, [values])
}

export const findAddress = (postalOrZipCode) => {
    const address = postalOrZipCode.replace(/[^\w\s]/g, '')
    delete axios.defaults.headers.common['Authorization'];
    return axios.get(`http://maps.googleapis.com/maps/api/geocode/json?address=${address}&sensor=false`)
}

export const fetchOrganisation = (token) => {
    setAuthorizationToken(token)
    return axios.post('api/OrgHierarchy/orgChart')
};

export const addOrgMember = (values, token) => {
    setAuthorizationToken(token)
    const { firstName, lastName, manager, designation, email, leaveManager, expenseManager, staffType } = values,
      url = `api/orgHierarchy/quickAdd`,
      form = new FormData();

    form.append("firstName", firstName);
    form.append("lastName", lastName);
    form.append("manager", manager);
    form.append("designation", designation);
    form.append("email", email);
    leaveManager && form.append("leaveManager", leaveManager);
    expenseManager && form.append("expenseManager", expenseManager);
    staffType && form.append("staffType", staffType);

    return axios.post(url, form)
};

export const updateOrg = (values, token) => {
    setAuthorizationToken(token)
    const { newManagerId, currentUserId } = values
    return axios.get(`api/orgHierarchy/userMove?newManager=${newManagerId}&currentUserId=${currentUserId}`)
};

export const logout = () => {
    return new Promise(resolve => setTimeout(resolve, 1000))
};


//ExpenseTypesAPIs
export const fetchExpenseTypes = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/crudApi/getAllCategories')
};

export const updateExpenseTypes = (values, token) => {
    setAuthorizationToken(token);
    return axios.post('api/crudApi/createCategory', values)
};

export const deleteExpenseTypes = (values, token) => {
    setAuthorizationToken(token);
    return axios.post('api/crudApi/deleteCategory', values)
};

//LeaveTypesAPIs
export const fetchLeaveTypes = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/leaveREST/getAllLeaveType')
};

export const updateLeaveTypes = (values, token) => {
    setAuthorizationToken(token);
    const { type, description, colorCode, shortCode, nonDeductible } = values,
    data = [{ type, description, colorCode, shortCode, nonDeductible: nonDeductible || false }],
        url = values.id ? `api/leaveREST/updateLeaveType?id=${values.id}` : `api/leaveREST/createLeaveType`
    return axios.post(url, data)
};

export const deleteLeaveTypes = (values, token) => {
    setAuthorizationToken(token);
    return axios.post('api/leaveREST/deleteLeaveType', values)
};

//DepartmentsAPIs
export const fetchDepartments = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/departmentREST/getAllDepartment')
};

export const updateDepartment = (values, token) => {
    setAuthorizationToken(token);
    const
        url = values.departmentId ?
        `api/departmentREST/updateDepartment?id=${values.departmentId}` : 'api/departmentREST/createDepartment'
    values.departmentId && delete values['departmentId']
    return axios.post(url, [values])
}

//users
export const addUser = (values, token) => {
    setAuthorizationToken(token);
    return axios.post('api/userREST/createUser', values)
};

export const updateCurrentUserStatus = (values, token) => {
    setAuthorizationToken(token)
    const { id, status } = values,
    data = { status }
    return axios.post(`api/userREST/editUserById?id=${id}`, data)
};

export const updateCurrentUser = (values, token) => {
    setAuthorizationToken(token)
    const url = `api/userREST/editUserById?id=${values.id}`
    values.id && delete values['id']
    values.userPhoto && delete values['userPhoto']
    return axios.post(url, values)
};

export const getUserSettings = (token) => {
    setAuthorizationToken(token)
    return axios.get('api/userREST/getUserSetting')
};

export const createUserSetting = (data, token) => {
    setAuthorizationToken(token)
    return axios.post(`api/userREST/createUserSetting`, data)
};

//Staff Types
export const fetchStaffTypes = (token) => {
    setAuthorizationToken(token);
    return axios.get('api/StaffTypeRest/getAllStaffType')
};

export const updateStaffTypes = (values, token) => {
    setAuthorizationToken(token);
    const url = values.id ? `api/StaffTypeRest/update?id=${values.id}` : `api/StaffTypeRest/save`
    return axios.post(url, values)
};

export const deleteStaffType = (id, token) => {
    setAuthorizationToken(token);
    return axios.get(`api/StaffTypeRest/delete?id=${id}`)
};

//Shifts
export const getShiftTemplates = (token) => {
    setAuthorizationToken(token);
    return axios.get(`api/TemplateREST/getAllTemplate`)
};

export const getShiftTypes = (token) => {
    setAuthorizationToken(token);
    return axios.get(`api/ShiftRest/getAllShiftType`)
}

export const getShiftManagerResponses = (token) => {
    setAuthorizationToken(token);
    return axios.get(`api/NotificationREST/shiftsResponse`)
}

export const getShiftManagerRequests = (token) => {
    setAuthorizationToken(token);
    return axios.get(`api/NotificationREST/notificationRequest/?isMyShift=false`)
}

export const createShiftType = (values, token) => {
    setAuthorizationToken(token);
    return axios.post(`api/ShiftRest/addShiftType`, values)
};

export const getDepartmentByBranch = (branchId, token) => {
    const payload = {
      branchId
    }
    setAuthorizationToken(token);
    return axios.post(`api/DepartmentREST/getAllDepartmentByBranch`, payload)
}

export const getStaffByStaffType = (staff_type, token) => {
    const payload = [
      {
        staff_type
      }
    ]
    setAuthorizationToken(token);
    return axios.post(`api/StaffTypeRest/getStaffByStaffType`, payload)
}

export const createShift = (values, token) => {
    const
      shiftUrl = values.shiftTimeId ? 'api/ShiftTimeRest/updateShiftTime' : 'api/shiftTimeRest/addShiftTime',
      templateUrl = 'api/TemplateREST/createTemplate',
      url = values.saveAs === 'template' ? templateUrl : shiftUrl,
      payload = [{
        ...values
      }]
    values.shiftTimeId === undefined && delete payload[0]['shiftTimeId']
    payload[0].saveAs && delete payload[0]['saveAs']
    setAuthorizationToken(token);
    return axios.post(url, payload)
}

export const getShiftsOfShiftManager = (token) => {
  setAuthorizationToken(token)
  return axios.get('api/shiftTimeRest/getAllshiftTime')
}

export const getShiftDetails = (values, token) => {
  setAuthorizationToken(token)
  return axios.post('api/ShiftUserREST/getUserShiftDetails', values)
}

export const updateFavorite = (value, token) => {
  const payload = {
    is_favorite : `${value.isSelected}`
  }
  setAuthorizationToken(token)
  return axios.post(`api/TemplateREST/updateTemplatesFav?id=${value.id}`, payload)
}

export const getShiftTemplateDetails = (templateId, token) => {
  setAuthorizationToken(token)
  return axios.get(`api/TemplateREST/getTemplate?id=${templateId}`)
}

export const removeFromShift = (shiftId, token) => {
  const payload = {
    assigned_id : `${shiftId}`
  }
  setAuthorizationToken(token)
  return axios.post(`api/ShiftUserREST/removeFromShift`, payload)
}

export const reAllocateShift = (values, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/ShiftUserREST/reallocate`, values)
}

export const getUserShiftsRequests = (token) => {
  setAuthorizationToken(token)
  return axios.get(`api/NotificationREST/notificationRequest/?isMyShift=true`)
}

export const getUserShifts = (token) => {
  setAuthorizationToken(token)
  return axios.get(`api/shiftTimeRest/getAllshiftTimeByUser`)
}

export const normalShiftAccept = (values, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/ShiftRequestResponseREST/saveShiftRequest`, values)
}

export const normalShiftDecline = (values, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/ShiftRequestResponseREST/declineTradeRequest`, values)
}

export const getShiftsForCalendar = (values, token) => {
  setAuthorizationToken(token)
  return axios.post('api/shiftTimeRest/getBranchShiftAllDetails', values)
}

export const getShiftsForUserCalendar = (values, token) => {
  setAuthorizationToken(token)
  return axios.post('api/shiftTimeRest/getUserBranchShiftDetails', values)
}

export const getShiftsForDay = (values, token) => {
  setAuthorizationToken(token)
  return axios.post('api/shiftTimeRest/getAllShifts', values)
}

export const getShiftTimeDetails = (shiftId, token) => {
  setAuthorizationToken(token)
  return axios.get(`api/ShiftTimeRest/getShiftTime?id=${shiftId}`)
}

export const requestShift = (values, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/ShiftUserREST/saveShiftAssignedToUser`, values)
}

export const tradeShift = (values, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/ShiftRequestResponseREST/tradeShift`, values)
}

export const cancelShift = (values, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/ShiftUserREST/declineAssignedShift`, values)
}

export const approveTradeShiftRequest = (values, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/ShiftRequestResponseREST/approveTradeShiftRequest`, values)
}

export const saveAutoriseTradeShiftRequest = (values, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/ShiftRequestResponseREST/saveAutoriseTradeShiftRequest`, values)
}

//Feeds APIs
export const fetchFeeds = (type, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/FeedREST/getMyFeeds`, type)
}

export const getAllActivity = (slug, token) => {
  setAuthorizationToken(token)
  return axios.post(`api/FeedREST/getAllActivityBySlug`, slug)
}

export const postActivity = (message, token) => {
  setAuthorizationToken(token)
  return axios.post(`api//FeedREST/postActivity`, message)
}
