import types from '../actions/types';
import InitialState from './initialState';
import _ from 'lodash'

const
  myFeeds = (state, action) => {
    switch (action.type) {
        case types.LOAD_FEEDS :
            return {
              ...state,
              feedsList: action.payload
            }
        case types.LOAD_SELECTED_FEED_ITEM :
            return {
              ...state,
              selectedFeed: action.payload
            }
        default:
            return state;
    }
  },

  allFeeds = (state, action) => {
    switch (action.type) {
        case types.LOAD_ALL_FEEDS :
            return {
              ...state,
              feedsList: action.payload
            }
        case types.LOAD_SELECTED_ALL_FEEDS :
            return {
              ...state,
              selectedFeed: action.payload
            }
        case types.ADD_ALL_NEW_ACTIVITY :
            return {
              ...state,
              selectedFeed: [
                ...state.selectedFeed,
                action.newActivity
              ]
            }
        default:
            return state;
    }
  },

  projectFeeds = (state, action) => {
    switch (action.type) {
        case types.LOAD_PROJECT_FEEDS :
            return {
              ...state,
              feedsList: action.payload
            }
        case types.LOAD_SELECTED_PROJECT_FEEDS :
            return {
              ...state,
              selectedFeed: action.payload
            }
        default:
            return state;
    }
  },

  teamFeeds = (state, action) => {
    switch (action.type) {
        case types.LOAD_TEAM_FEEDS :
            return {
              ...state,
              feedsList: action.payload
            }
        case types.LOAD_SELECTED_TEAM_FEEDS :
            return {
              ...state,
              selectedFeed: action.payload
            }
        default:
            return state;
    }
  },

  directFeeds = (state, action) => {
    switch (action.type) {
        case types.LOAD_INDIVIDUAL_FEEDS :
            return {
              ...state,
              feedsList: action.payload
            }
        case types.LOAD_SELECTED_INDIVIDUAL_FEEDS :
            return {
              ...state,
              selectedFeed: action.payload
            }
        default:
            return state;
    }
  },

  feeds = (state = InitialState.teamFeeds, action) => {
    switch(action.type) {
      default: {
        return {
          myFeeds: myFeeds(state.myFeeds, action),
          allFeeds: allFeeds(state.allFeeds, action),
          projectFeeds: projectFeeds(state.projectFeeds, action),
          teamFeeds: teamFeeds(state.teamFeeds, action),
          directFeeds: directFeeds(state.directFeeds, action),
        };
      }
    }
  };

export default {
  feeds,
};
