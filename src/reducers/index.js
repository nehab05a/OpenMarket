import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import { sessionReducer } from 'redux-react-session';
import { routerReducer } from 'react-router-redux'
import { reducer as sematable } from 'sematable';
import dropdowns from './dropdowns'
import files from './files'
import tooltips from './tooltips'
import companySettings from './companySettings'
import users from './users'
import shifts from './shifts'
import managers from './managers'
import login from './login'
import teamFeeds from './teamFeeds'

const appReducer  = combineReducers({
  form,
  router: routerReducer,
  sematable,
  session : sessionReducer,
  dropdowns,
  files,
  tooltips,
  users,
  ...managers,
  shifts,
  login,
  ...companySettings,
  ...teamFeeds,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    state = undefined
  }

  return appReducer(state, action)
}

export default rootReducer;
