import types from '../actions/types';

export default(state = {}, action) => {
    switch (action.type) {
        case types.LOGIN_BEGIN :
            return {...state, fetch: true};
        case types.LOGIN_END :
            return {...state, fetch: false};
        default:
            return state;
    }
}
