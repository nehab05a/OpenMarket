import types from '../actions/types';
import InitialState from './initialState';
import _ from 'lodash'

const
  toCamelCase = (record) => {
    _.map(record, (item, oldKey) => {
      const newKey = _.camelCase(oldKey).replace(/[^\w\s]/g, '');
      record[newKey] = record[oldKey]
      delete record[oldKey];
    })
    return record
  },

  accountDetails = (state, action) => {
    switch (action.type) {
        case types.LOAD_ACCOUNT_DETAILS :
            return {...action.payload}
        default:
            return state;
    }
  },

  expenseTypes = (state, action) => {
      switch (action.type) {
          case types.LOAD_EXPENSE_TYPES :
              return action.payload
          default:
              return state;
      }
  },

  leaveTypes = (state, action) => {
      switch (action.type) {
          case types.LOAD_LEAVE_TYPES :
              return action.payload
          default:
              return state;
      }
  },

  departments = (state, action) => {
      switch (action.type) {
          case types.LOAD_DEPARTMENTS :
              return action.payload.map(record => toCamelCase(record))
          default:
              return state;
      }
  },

  locations = (state, action) => {
      switch (action.type) {
          case types.LOAD_LOCATIONS :
              return action.payload
          default:
              return state;
      }
  },

  organisation = (state, action) => {
      switch (action.type) {
          case types.LOAD_ORGANISATION:
              return {
                  ...action.users
              };
          default:
              return state;
      }
  },

  currentUser = (state, action) => {
      switch (action.type) {
          case types.GET_CURRENT_USER :
              return action.payload
          default:
              return state;
      }
  },

  staffTypes = (state, action) => {
    switch (action.type) {
        case types.LOAD_STAFF_TYPES :
            return action.payload
        default:
            return state;
    }
  },

  companySettings = (state = InitialState.companySettings, action) => {
    switch(action.type) {
      default: {
        return {
          accountDetails: accountDetails(state.accountDetails, action),
          staffTypes: staffTypes(state.staffTypes, action),
          expenseTypes: expenseTypes(state.expenseTypes, action),
          leaveTypes: leaveTypes(state.leaveTypes, action),
          departments: departments(state.departments, action),
          locations: locations(state.locations, action),
          currentUser: currentUser(state.currentUser, action),
          organisation: organisation(state.organisation, action),
        };
      }
    }
  };

export default {
  companySettings,
};
