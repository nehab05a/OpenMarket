import types from '../actions/types';
import _ from 'lodash'

const toCamelCase = (record) => {
  _.map(record, (item, oldKey) => {
    const newKey = _.camelCase(oldKey).replace(/[^\w\s]/g, '');
    record[newKey] = record[oldKey]
    delete record[oldKey];
  })
  return record
},

initialState = {
  templates: {
    favorites: false
  }
}

export default(state = initialState, action) => {
    switch (action.type) {
        case types.GET_SHIFT_TEMPLATE_LIST_SUCCESS :
            const data = {
              ...state.templates,
              list: action.payload,
            }
            return {...state, templates: data};
        case types.GET_FAVORITE_TEMPLATES_SUCCESS :
            const favTemplates = {
              ...state.templates,
              favorites: !state.templates.favorites
            }
            return {...state, templates: favTemplates};
        case types.GET_SHIFT_TYPES_SUCCESS :
            return {...state, shiftTypes: action.payload};
        case types.GET_MANAGERS_LIST_SUCCESS :
            return {...state, shiftManagers: action.payload};
        case types.GET_SHIFT_MANAGER_RESPONSES_SUCCESS :
            return {...state, shiftManagerResponses: action.payload};
        case types.GET_SHIFT_MANAGER_REQUESTS_SUCCESS :
            return {...state, shiftManagerRequests: action.payload};
        case types.GET_STAFF_BY_TYPE_SUCCESS :
            return {...state, shiftStaff: action.payload};
        case types.GET_DEPARTMENT_BY_BRANCH_SUCCESS :
            return {...state, shiftDepartments: action.payload.map(record => toCamelCase(record))};
        case types.GET_SHIFTS_OF_MANAGER_SUCCESS :
            return {...state, shifts: action.payload};
        case types.GET_USER_SHIFTS_SUCCESS :
            return {...state, userShifts: action.payload}
        case types.GET_USER_SHIFTS_REQUEST_SUCCESS :
            return {...state, userShiftRequests: action.payload}
        case types.GET_CALENDAR_SHIFTS_SUCCESS :
            const calendarData = {
              ...state.shiftManagerCalender,
              ...action.payload
            }
            return {...state, shiftManagerCalender: calendarData}
        case types.GET_DAY_SHIFTS_SUCCESS :
            return {...state, shiftsForDay: action.payload}
        case types.GET_USER_SHIFTS_CALENDAR :
            const userCalendarData = {
              ...state.userShiftsCalender,
              ...action.payload
            }
            return {...state, userShiftsCalender: userCalendarData}
        case types.SET_USER_CALENDAR_FILTERS :
            const filtersAddedData = {
              ...state.userShiftsCalender,
              filters: action.filters
            }
            return {...state, userShiftsCalender: filtersAddedData}
        default:
            return state;
    }
}
