import types from '../actions/types'
import InitialState from './initialState';

const
  allManagers = (state, action) => {
    switch (action.type) {
        case types.FETCH_MANAGERS:
            return action.payload
        default:
            return state;
    }
  },

  leaveManagers = (state, action) => {
    switch (action.type) {
        case types.FETCH_LEAVE_MANAGERS:
            return action.payload
        default:
            return state;
    }
  },

  expenseManagers = (state, action) => {
    switch (action.type) {
        case types.FETCH_EXPENSE_MANAGERS:
            return action.payload
        default:
            return state;
    }
  },

  managers = (state = InitialState.managers, action) => {
    switch(action.type) {
      default: {
        return {
          allManagers: allManagers(state.allManagers, action),
          expenseManagers: expenseManagers(state.expenseManagers, action),
          leaveManagers: leaveManagers(state.leaveManagers, action),
        };
      }
    }
  };


export default {
  managers
}
