export default {
    auth: {
        isAuthenticated: false,
        user: {}
    },
    companySettings: {
        accountDetails: {},
        expenseTypes: [],
        leaveTypes: [],
        departments: [],
        locations: [],
        organisation: {},
        currentUser: {},
        staffTypes: []
    },
    managers: {
      allManagers: [],
      leaveManagers: [],
      expenseManagers: []
    },
    teamFeeds: {
      myFeeds: {},
      allFeeds: {},
      projectFeeds: {},
      teamFeeds: {},
      directFeeds: {}
    }
};
