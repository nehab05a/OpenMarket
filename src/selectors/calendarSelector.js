import { createSelector } from 'reselect';
import _ from 'lodash';
import moment from 'moment'

const
  getUserShiftsCalendar = (state) => state.shifts && state.shifts.userShiftsCalender,
  getManagerShiftsCalendar = (state) => state.shifts && state.shifts.shiftManagerCalender,

  userShiftsCalendarSelector = createSelector(
      getUserShiftsCalendar,
      userCalendarData => {
        const viewBy = userCalendarData && userCalendarData.viewBy,
              data = userCalendarData && userCalendarData.data,
              filters = userCalendarData && userCalendarData.filters

        return data && _.chain(data)
                        .filter(shiftRecord => filters && filters.hot_shift ? shiftRecord.hot_shift === true : shiftRecord)
                        .filter(shiftRecord => filters && filters.available ?
                          shiftRecord.shift_requested === 'false' && shiftRecord.shift_accepted === 'false' : shiftRecord)
                        .filter(shiftRecord => filters && filters.pending ?
                          shiftRecord.shift_requested === 'true' && shiftRecord.shift_accepted === 'false' : shiftRecord)
                        .map(shiftRecord => {
                            let classNames = []
                            const
                              shiftDate = moment(shiftRecord.shift_date, 'DD/MM/YYYY'),
                              className = shiftRecord.shift_requested === 'false' ?
                                          shiftRecord.shift_accepted === 'true' ? 'working' : 'available'
                                          : shiftRecord.shift_accepted === 'true' ? 'working' : 'awaiting'

                            classNames.push(className)
                            shiftRecord.hot_shift && classNames.push('hotData')

                          return viewBy === 'year' ? {
                            date: moment(shiftRecord.shift_date, 'DD/MM/YYYY'),
                            component: [ 'day' ],
                            badgeValue: shiftRecord.remaining_emps,
                            classNames,
                          } : {
                            title: shiftRecord.shift_name,
                            start: new Date(shiftDate.year(), shiftDate.month(), shiftDate.date()),
                            end: new Date(shiftDate.year(), shiftDate.month(), shiftDate.date()),
                            available: shiftRecord.remaining_emps,
                            totalRequirement: shiftRecord.required_emps,
                            hotShift: shiftRecord.hot_shift,
                            shiftStatus: className
                          }
                        })
      .value()
  }),

  managersShiftsCalendarSelector = createSelector(
      getManagerShiftsCalendar,
      managersCalendarData => {
        const viewBy = managersCalendarData && managersCalendarData.viewBy,
              data = managersCalendarData && managersCalendarData.data

        return data && data.map(shiftRecord => {
          let classNames = []
          const
            shiftDate = moment(shiftRecord.shift_date, 'DD/MM/YYYY'),
            selected = data.filter(item => shiftDate.format('DD/MM/YYYY') === item.shift_date),
            anyAvailable = selected.some(shiftData => shiftData.shift_remaining_empCount > 0)

          classNames.push(anyAvailable ? 'available' : 'full')
          shiftRecord.hot_shift && classNames.push('hotData')

          return viewBy === 'year' ? {
            date: shiftDate,
            classNames:  classNames,
            component: [ 'day' ],
            badgeValue: shiftRecord.shift_remaining_empCount,
          } : {
            title: shiftRecord.shift_name,
            start: new Date(shiftDate.year(), shiftDate.month(), shiftDate.date()),
            end: new Date(shiftDate.year(), shiftDate.month(), shiftDate.date()),
            available: shiftRecord.shift_remaining_empCount,
            totalRequirement: shiftRecord.shift_total_emp,
            hotShift: shiftRecord.hot_shift,
            shiftStatus: shiftRecord.shift_remaining_empCount > 0 ? 'available' : 'full'
          }
        })
  });

export {
  userShiftsCalendarSelector,
  managersShiftsCalendarSelector
};
