import { createSelector } from 'reselect';
import { transform, sortBy, capitalize } from 'lodash';

const
  getShiftTemplates = (state) => state.shifts && state.shifts.templates.list,
  showFavorites = (state) => state.shifts && state.shifts.templates.favorites,
  getShiftTypes = (state) => state && state.shifts.shiftTypes,
  getLocations = (state) => state && state.companySettings.locations,
  getDepartments = (state) => state && state.companySettings.departments,
  getShiftManagers = (state) => state && state.shifts.shiftManagers,
  getManagers = (state) => state && state.managers.allManagers,
  getLeaveManagers = (state) => state && state.managers.leaveManagers,
  getExpenseManagers = (state) => state && state.managers.expenseManagers,
  getStaffTypes = (state) => state && state.companySettings.staffTypes,
  getUsers = (state) => state && state.users,
  getStaffByStaffType = (state) => state && state.shifts.shiftStaff,
  getDepartmentsByBranch = (state) => state && state.shifts.shiftDepartments,

  getIndexedList = list => {
    let index = 0;
    return transform(list, (result, value, key) => {
    result[index++] = { key, items:value };
  }, {})},

  templateListSelector = createSelector(
      getShiftTemplates,
      showFavorites,
      (shiftsTemplates, isFav) => {
        const
         filteredItems = isFav ? shiftsTemplates.filter(item => item.isFavorite) : shiftsTemplates,
         sortedList = sortBy(filteredItems, template => capitalize(template.templateName)),
         groupedList = transform(sortedList, (result, value) => {
            const
              { templateName, isFavorite, templateId } = value,
              key = templateName && templateName.charAt(0);
            ((result[key]) || (result[key] = [])).push({ id: templateId, displayName : templateName, isSelected : isFavorite });
        },{});
      return getIndexedList(groupedList);
  }),

  locationListSelector = createSelector(
    getLocations,
    locations => {
      const sortedList = sortBy(locations, location => capitalize(location.branch_name)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { branch_name, branch_location, id } = value,
            key = branch_name && branch_name.charAt(0);
          ((result[key]) || (result[key] = [])).push({ displayName : branch_name, id, details: branch_location});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  shiftTypesListSelector = createSelector(
    getShiftTypes,
    shiftTypes => {
      const sortedList = sortBy(shiftTypes, shiftType => capitalize(shiftType.shift_type_name)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { shift_type_name, id } = value,
            key = shift_type_name && shift_type_name.charAt(0);
          ((result[key]) || (result[key] = [])).push({ displayName : shift_type_name, id});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  shiftManagersListSelector = createSelector(
    getShiftManagers,
    managers => {
      const sortedList = sortBy(managers, manager => capitalize(manager.firstName)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { firstName, lastName, id } = value,
            key = firstName && firstName.charAt(0);
          ((result[key]) || (result[key] = [])).push({ displayName : `${firstName} ${lastName}`, id});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  allManagersListSelector = createSelector(
    getManagers,
    managers => {
      const sortedList = sortBy(managers, manager => capitalize(manager.firstName)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { firstName, lastName, id } = value,
            key = firstName && firstName.charAt(0);
          ((result[key]) || (result[key] = [])).push({ displayName : `${firstName} ${lastName}`, id});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  leaveManagersListSelector = createSelector(
    getLeaveManagers,
    managers => {
      const sortedList = sortBy(managers, manager => capitalize(manager.firstName)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { firstName, lastName, id } = value,
            key = firstName && firstName.charAt(0);
          ((result[key]) || (result[key] = [])).push({ displayName : `${firstName} ${lastName}`, id});
      },{});
      return getIndexedList(groupedList);
    }
  ),


  expenseManagersListSelector = createSelector(
    getExpenseManagers,
    managers => {
      const sortedList = sortBy(managers, manager => capitalize(manager.firstName)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { firstName, lastName, id } = value,
            key = firstName && firstName.charAt(0);
          ((result[key]) || (result[key] = [])).push({ displayName : `${firstName} ${lastName}`, id});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  shiftUsersListSelector = createSelector(
    getUsers,
    users => {
      const sortedList = sortBy(users, user => capitalize(user.userFullName)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { userFullName, id } = value,
            key = userFullName && userFullName.charAt(0);
          ((result[key]) || (result[key] = [])).push({ displayName : userFullName, id});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  departmentsListSelector = createSelector(
    getDepartments,
    departments => {
      const sortedList = sortBy(departments, department => capitalize(department.departmentName)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { departmentName, departmentId } = value,
            key = departmentName && departmentName.charAt(0);
          ((result[capitalize(key)]) || (result[capitalize(key)] = [])).push({ displayName : departmentName, id: departmentId});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  departmentsByBranchListSelector = createSelector(
    getDepartmentsByBranch,
    departments => {
      const sortedList = sortBy(departments, department => capitalize(department.departmentName)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { departmentName, departmentId } = value,
            key = departmentName && departmentName.charAt(0);
          ((result[capitalize(key)]) || (result[capitalize(key)] = [])).push({ displayName : departmentName, id: departmentId});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  staffTypesListSelector = createSelector(
    getStaffTypes,
    staffTypes => {
      const sortedList = sortBy(staffTypes, staffType => capitalize(staffType.staff_type)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { staff_type, id } = value,
            key = staff_type && staff_type.charAt(0);
          ((result[capitalize(key)]) || (result[capitalize(key)] = [])).push({ displayName : staff_type, id});
      },{});
      return getIndexedList(groupedList);
    }
  ),

  staffByStaffTypeListSelector = createSelector(
    getStaffByStaffType,
    staffByStaffTypes => {
      const sortedList = sortBy(staffByStaffTypes, staffType => capitalize(staffType.firstname)),
       groupedList = transform(sortedList, (result, value) => {
          const
            { firstname, LastName, staffId } = value,
            key = firstname && firstname.charAt(0);
          ((result[capitalize(key)]) || (result[capitalize(key)] = [])).push({ displayName : `${firstname} ${LastName}`, id: staffId});
      },{});
      return getIndexedList(groupedList);
    }
  );

export {
  templateListSelector,
  locationListSelector,
  shiftTypesListSelector,
  shiftManagersListSelector,
  shiftUsersListSelector,
  departmentsListSelector,
  allManagersListSelector,
  expenseManagersListSelector,
  leaveManagersListSelector,
  staffTypesListSelector,
  staffByStaffTypeListSelector,
  departmentsByBranchListSelector
};
