import types from './types'
import * as api from '../lib/api/sessionApi'

const loadDepartments = (payload) => ({
  type: types.LOAD_DEPARTMENTS,
  payload
});

export const fetchDepartments = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
     api.fetchDepartments(access_token)
     .then(response => {
        const { serverResponse } = response.data[0]
        return dispatch(loadDepartments(serverResponse))
     })
   )}
)

export const updateDepartment = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user,
    departmentPhoto     = state.files.department

  return(
     api.updateDepartment(values, access_token)
     .then(response => {
       const
         { id } = response.data,
         logoPayload = {
           id,
           departmentPhoto: departmentPhoto && departmentPhoto[0]
         }
       departmentPhoto && api.uploadDepartmentPhoto(logoPayload, access_token)
       .then(data => console.log(data))
        if(callbackSuccess) callbackSuccess()
     })
     .catch(error => {
        if(callbackFailure) callbackFailure()
     }
   ))
 }
)

export const deleteDepartment = (id, callbackSuccess, callbackFailure) => ( dispatch => {
 //  const data = [{id}];
 //  return(
 //     api.deleteDepartment(data, user.access_token)
 //     .then(response => {
 //        if(callbackSuccess) callbackSuccess()
 //     }
 //   ))
 }
)
