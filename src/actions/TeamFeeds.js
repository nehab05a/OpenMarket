import types from './types'
import * as api from '../lib/api/sessionApi'

const loadFeedTypes      = {
                              MY: types.LOAD_FEEDS,
                              ALL: types.LOAD_ALL_FEEDS,
                              PROJECT: types.LOAD_PROJECT_FEEDS,
                              TEAM: types.LOAD_TEAM_FEEDS,
                              INDIVIDUAL: types.LOAD_INDIVIDUAL_FEEDS
                           },
      selectedFeedTypes  = {
                              MY: types.LOAD_SELECTED_FEED_ITEM,
                              ALL: types.LOAD_SELECTED_ALL_FEEDS,
                              PROJECT: types.LOAD_SELECTED_PROJECT_FEEDS,
                              TEAM: types.LOAD_SELECTED_TEAM_FEEDS,
                              INDIVIDUAL: types.LOAD_SELECTED_INDIVIDUAL_FEEDS
                           },
       newActivityTypes  = {
                               MY: types.LOAD_SELECTED_FEED_ITEM,
                               ALL: types.ADD_ALL_NEW_ACTIVITY,
                               PROJECT: types.LOAD_SELECTED_PROJECT_FEEDS,
                               TEAM: types.LOAD_SELECTED_TEAM_FEEDS,
                               INDIVIDUAL: types.LOAD_SELECTED_INDIVIDUAL_FEEDS
                            };

const loadFeeds = (feedType, payload) => ({
  type: loadFeedTypes[feedType],
  payload
});

const loadSelected = (feedType, payload) => ({
  type: selectedFeedTypes[feedType],
  payload
});

const addNewActivity = (feedType, newActivity) => ({
  type: newActivityTypes[feedType],
  newActivity
});

export const fetchFeeds = (type, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user,
    payload             = {
      type
    }
  return (
     api.fetchFeeds(payload, access_token)
     .then(response => {
        const { data } = response
        return dispatch(loadFeeds(type, data))
     })
   )}
)

export const getAllActivity = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user,
    { type, slug }      = values,
    payload             = {
      slug
    }
  return (
     api.getAllActivity(payload, access_token)
     .then(response => {
        const { data } = response
        return dispatch(loadSelected(type, data))
     })
   )}
)

export const postActivity = (message, feedType, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user

  return (
     api.postActivity(message, access_token)
     .then(response => {
        if(callbackSuccess) callbackSuccess()
     })
     .catch(err => {
       if(callbackFailure) callbackFailure()
     })
   )}
)

export default {
  fetchFeeds,
  getAllActivity,
  postActivity,
  addNewActivity
}
