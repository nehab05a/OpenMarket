import types, {TOOGLE_MDL_CHANGE_PWD} from './types'
import * as api from '../lib/api/sessionApi'

export const toggleChangePwd = () => ({
  type: TOOGLE_MDL_CHANGE_PWD
})

const loadAccountDetails = (payload) => ({
  type: types.LOAD_ACCOUNT_DETAILS,
  payload
});

export const fetchAccountDetails = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
     api.fetchDetails(access_token)
     .then(response => {
        // api.getCompanyLogo()
        // .then(res => {
        //   response.data.companyLogo = res.data;
        //   return dispatch(loadAccountDetails(response.data))
        // })
        return dispatch(loadAccountDetails(response.data))
     })
   )}
)

export const updateAccountDetails = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
     api.updateDetails(values, access_token)
     .then(response => {
        dispatch(loadAccountDetails(response.data))
        if(callbackSuccess) callbackSuccess()
     })
   )}
)

export const updateAppSettings = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token, userTenantId}  = state.session.user,
    customerLogo        = state.files.logo,
    logoPayload = {
      id: userTenantId,
      customerLogo: customerLogo && customerLogo[0]
    }
  return (
     api.updateSettings(values, access_token)
     .then(response => {
        customerLogo && api.uploadLogo(logoPayload, access_token)
        .then(data => console.log(data))
        dispatch(loadAccountDetails(response.data))
        if(callbackSuccess) callbackSuccess()
     })
   )}
)
