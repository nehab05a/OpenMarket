import types, {FETCH_USERS,DELETE_USER,CHANGE_STATUS_USER} from './types'
import * as api from '../lib/api/sessionApi'

export const loadUsers = (users) => ({
  type: FETCH_USERS,
  users,
});

export const loadCurrentUser = (payload) => ({
  type: types.GET_CURRENT_USER,
  payload,
});

export const loadUserSettings = (payload) => ({
  type: types.LOAD_USER_SETTINGS,
  payload,
});

export const deleteUser = (userId) => ({
  type: DELETE_USER,
  userId,
});

export const changeStatusUser = (userId,status) => ({
  type: CHANGE_STATUS_USER,
  userId,
  status
});

export const loadManagers = (payload) => ({
  type: types.FETCH_MANAGERS,
  payload,
});

export const loadLeaveManagers = (payload) => ({
  type: types.FETCH_LEAVE_MANAGERS,
  payload,
});

export const loadExpenseManagers = (payload) => ({
  type: types.FETCH_EXPENSE_MANAGERS,
  payload,
});

export const fetchCurrentUserDetails = (id, existingUser = {}, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.fetchUserDetails(id, access_token)
    .then(response => {
      const
        { data: { UserResponse, UserRoles } } = response,
        payload = {
          userDetails: {
            ...UserResponse,
            ...existingUser
          },
          userPermissions: UserRoles,
        }
      if(callbackSuccess) callbackSuccess(payload)
      return dispatch(loadCurrentUser(payload))
    })
  )}
)

export const updateCurrentUserStatus = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.updateCurrentUserStatus(values, access_token)
      .then(response => {
        if(callbackSuccess) callbackSuccess(values.id)
      })
    )
  }
)

export const fetchUsersList = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
  api.fetchUsersList(access_token)
    .then(response => {
      const { serverResponse } = response.data[0]
      return dispatch(loadUsers(serverResponse))
    })
  )}
)

export const fetchManagersList = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.fetchManagersList(access_token)
    .then(response => {
      const { data } = response
      return dispatch(loadManagers(data))
    })
  )}
)

export const fetchExpenseManagersList = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.fetchExpenseManagersList(access_token)
    .then(response => {
      const { data } = response
      return dispatch(loadExpenseManagers(data))
    })
  )}
)

export const fetchLeaveManagersList = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.fetchLeaveManagersList(access_token)
    .then(response => {
      const { data } = response
      return dispatch(loadLeaveManagers(data))
    })
  )}
)


export const addUser = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
  api.addUser([values], access_token)
  .then(response => {
    const { ErrorResponse } = response.data
     if(ErrorResponse) throw(ErrorResponse)
     else if(callbackSuccess) callbackSuccess()
  })
  .catch(error => {
    const ERROR_MESSAGE = error || true;
    if(callbackFailure) callbackFailure(ERROR_MESSAGE)
  })
  )}
)

export const updateCurrentUser = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.updateCurrentUser(values, access_token)
      .then(response => {
        if(callbackSuccess) callbackSuccess(values.id)
      })
    )
  }
)

export const getUserSettings = (callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.getUserSettings(access_token)
      .then(response => {
        const { data } = response
        dispatch(loadUserSettings(data))
        if(callbackSuccess) callbackSuccess(data)
      })
    )
  }
)

export const updateUserSetting = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.createUserSetting(values, access_token)
      .then(response => {
        if(callbackSuccess) callbackSuccess()
      })
      .catch(error => {
        if(callbackFailure) callbackFailure()
      })
    )
  }
)
