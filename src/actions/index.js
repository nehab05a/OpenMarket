import types from './types'
import {sessionService} from 'redux-react-session'
import * as sessionApi from '../lib/api/sessionApi'
import { push } from 'react-router-redux'
import _ from 'lodash'

const saveUserSession = (user, history) => {
  sessionService.saveUser(user).then(() => {
    const { accountActivated } = user,
      isAdmin = user.roles && _.includes(user.roles, 'ROLE_ORG_ADMIN', 'ROLE_ADMIN'),
      route = accountActivated ? '/' : isAdmin ? '/company/getstart' : '/profile'
    history.push(route);
  }).catch(err => history.push('/login'));
},

loginBegin = () => ({
  type: types.LOGIN_BEGIN
}),

loginEnd = () => ({
  type: types.LOGIN_END
})

export const login = (user, history, callbackSuccess, callbackFailure) => {
  return (dispatch) => {
    dispatch(loginBegin())
    return sessionApi.login(user.email, user.password)
    .then(response => {
        sessionApi.getUser(response.data.access_token)
        .then(res => {
          const
            { data } = res,
            { access_token, roles } = response.data;
            sessionStorage.setItem('team-token', access_token)
            sessionService.saveSession(access_token)
            .then(() => saveUserSession({...data, roles, access_token}, history))
            dispatch(loginEnd())
         })
        .catch(err => {
            const ERROR_MESSAGE = 'Something went wrong. Please try in some time.';
            dispatch(loginEnd())
            if(callbackFailure) callbackFailure(ERROR_MESSAGE)
          }
        )
      })
    .catch((error, reject) => {
      const ERROR_MESSAGE = 'Something went wrong. Please try in some time.';
      dispatch(loginEnd())
      if(callbackFailure) callbackFailure(ERROR_MESSAGE)
    });
  };
};

export const register = (customer, callbackSuccess, callbackFailure) => {
  return () => {
    return sessionApi.register(customer).then((response) => {
      const { data } = response,
        { ErrorMessage } = data;
        if(ErrorMessage) throw(ErrorMessage);
        else {
          if(callbackSuccess) callbackSuccess()
        };
    }).catch((error, reject) => {
      const ERROR_MESSAGE = error || 'Something went wrong. Try again in some time';
      if(callbackFailure) callbackFailure(ERROR_MESSAGE)
    });
  };
};

export const forgotPassword = (values, callbackSuccess, callbackFailure) => {
  return () => {
    return sessionApi.forgotPassword(values.email).then((response) => {
      const { data } = response,
        { ErrorMessage } = data;
        if(ErrorMessage) throw(ErrorMessage);
        else {
          if(callbackSuccess) callbackSuccess()
        };
    }).catch((error, reject) => {
      const ERROR_MESSAGE = error || 'Something went wrong. Try again in some time';
      if(callbackFailure) callbackFailure(ERROR_MESSAGE)
    });
  };
};

export const changePassword = (passwords, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
      sessionApi.changePassword(passwords, access_token)
      .then((response) => {
        const data = response.data,
          { serverResponse } = data
          if(serverResponse === 'Your existing password does not match') throw(serverResponse);
          if(callbackSuccess) callbackSuccess(serverResponse)
      })
      .catch((error, reject) => {
        const ERROR_MESSAGE = error || 'Something went wrong. Try again in some time';
        if(callbackFailure) callbackFailure(ERROR_MESSAGE)
      })
    )
});

export const verifyCode = (data, callbackSuccess, callbackFailure) => {
  return () => {
    return sessionApi.verifyCode(data)
    .then((response) => {
      const { ErrorResponse } = response.data;
      if(ErrorResponse) throw(ErrorResponse);
      else {
        if(callbackSuccess) callbackSuccess()
      };
    })
    .catch((error, reject) => {
      const ERROR_MESSAGE = error || 'Something went wrong. Please check the verification code sent over email';
      if(callbackFailure) callbackFailure(ERROR_MESSAGE)
    });
  };
};

export const newPassword = (data, callbackSuccess, callbackFailure) => {
  return () => {
    return sessionApi.newPassword(data)
    .then((response) => {
      const { data } = response,
        { ErrorResponse } = data;
        if(ErrorResponse) throw(ErrorResponse);
        else {
          if(callbackSuccess) callbackSuccess()
        };
    }).catch((error, reject) => {
      const ERROR_MESSAGE = error || 'Something went wrong. Try again in some time';
      if(callbackFailure) callbackFailure(ERROR_MESSAGE)
    });
  };
};

export const logout = () => ( dispatch => {
  return sessionApi.logout().then(() => {
    sessionService.deleteSession();
    sessionService.deleteUser();
    sessionStorage.removeItem('team-token')
    dispatch(push('/login'));
  }).catch(err => {
    throw(err);
  });
});
