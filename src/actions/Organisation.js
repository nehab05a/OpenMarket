import types from './types'
import * as api from '../lib/api/sessionApi'
import _ from 'lodash'

export const loadOrganisation = (users) => ({
  type: types.LOAD_ORGANISATION,
  users
});

export const fetchOrganisation = () => (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
    api.fetchOrganisation(access_token)
    .then(response => {
      const data = response.data,
        orgChartData = {
          ...data,
          title: 'Organisation'
        }
      return dispatch(loadOrganisation(orgChartData));
    })
  )
}

export const addOrganisationMember = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
     api.addOrgMember(values, access_token)
     .then(response => {
       const { ErrorResponse } = response.data
        if(ErrorResponse) throw(ErrorResponse)
        else if(callbackSuccess) callbackSuccess()
     })
     .catch(error => {
       const ERROR_MESSAGE = error || true;
       if(callbackFailure) callbackFailure(ERROR_MESSAGE)
     })
   )}
)

export const updateOrganisation = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
     api.updateOrg(values, access_token)
     .then(response => {
       if(callbackSuccess) callbackSuccess()
     })
     .catch(error => {
       if(callbackFailure) callbackFailure()
     })
   )}
)
