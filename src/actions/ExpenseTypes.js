import types from './types'
import * as api from '../lib/api/sessionApi'

const loadExpenseDetails = (payload) => ({
  type: types.LOAD_EXPENSE_TYPES,
  payload
});

export const fetchExpenseTypes = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
     api.fetchExpenseTypes(access_token)
     .then(response => {
        const { serverResponse } = response.data[0]
        return dispatch(loadExpenseDetails(serverResponse))
     })
   )}
)

export const updateExpenseTypes = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user,
    { id, name, description } = values,
    data = id ? [{id, name, description}] : [{name, description}];
  return(
     api.updateExpenseTypes(data, access_token)
     .then(response => {
        if(callbackSuccess) callbackSuccess()
     }
   ))
 }
)

export const deleteExpenseType = (id, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user,
    data = [{id}];
  return(
     api.deleteExpenseTypes(data, access_token)
     .then(response => {
        if(callbackSuccess) callbackSuccess()
     }
   ))
 }
)
