import types from './types'
import * as api from '../lib/api/sessionApi'

const loadLeaveDetails = (payload) => ({
  type: types.LOAD_LEAVE_TYPES,
  payload
});

export const fetchLeaveTypes = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
     api.fetchLeaveTypes(access_token)
     .then(response => {
       const { serverResponse } = response.data[0]
        return dispatch(loadLeaveDetails(serverResponse))
     })
   )}
)

export const updateLeaveTypes = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return(
     api.updateLeaveTypes(values, access_token)
     .then(response => {
        if(callbackSuccess) callbackSuccess()
     }
   ))
 }
)

export const deleteLeaveType = (id, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user,
    data = [{id}];
  return(
     api.deleteLeaveTypes(data, access_token)
     .then(response => {
        if(callbackSuccess) callbackSuccess()
     }
   ))
 }
)
