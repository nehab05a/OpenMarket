import types from './types'
import * as api from '../lib/api/sessionApi'

const loadStaffTypes = (payload) => ({
  type: types.LOAD_STAFF_TYPES,
  payload
});

export const fetchStaffTypes = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
     api.fetchStaffTypes(access_token)
     .then(response => {
        const { data } = response
        return dispatch(loadStaffTypes(data))
     })
   )}
)

export const updateStaffTypes = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user,
    { id, staff_type, description } = values,
    data = id ? {id, staff_type, description} : {staff_type, description};
  return(
     api.updateStaffTypes(data, access_token)
     .then(response => {
        if(callbackSuccess) callbackSuccess()
     }
   ))
 }
)

export const deleteStaffType = (id, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return(
     api.deleteStaffType(id, access_token)
     .then(response => {
        if(callbackSuccess) callbackSuccess()
     }
   ))
 }
)
