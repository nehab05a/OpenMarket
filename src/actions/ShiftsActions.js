import types from './types'
import * as api from '../lib/api/sessionApi'

const
  getShiftTemplateListSuccess = (payload) => ({
    type: types.GET_SHIFT_TEMPLATE_LIST_SUCCESS,
    payload
  }),

  getShiftManagerListSuccess = (payload) => ({
    type: types.GET_MANAGERS_LIST_SUCCESS,
    payload
  }),

  getShiftTypesSuccess = (payload) => ({
    type: types.GET_SHIFT_TYPES_SUCCESS,
    payload
  }),

  getShiftManagerResponsesSuccess = (payload) => ({
    type: types.GET_SHIFT_MANAGER_RESPONSES_SUCCESS,
    payload
  }),

  getShiftManagerRequestsSuccess = (payload) => ({
    type: types.GET_SHIFT_MANAGER_REQUESTS_SUCCESS,
    payload
  }),

  getStaffByStaffTypeSuccess = (payload) => ({
    type: types.GET_STAFF_BY_TYPE_SUCCESS,
    payload
  }),

  getDepartmentByBranchSuccess = (payload) => ({
    type: types.GET_DEPARTMENT_BY_BRANCH_SUCCESS,
    payload
  }),

  getShiftsOfShiftManagerSuccess = (payload) => ({
    type: types.GET_SHIFTS_OF_MANAGER_SUCCESS,
    payload
  }),

  getFavoriteTemplatesSuccess = () => ({
    type: types.GET_FAVORITE_TEMPLATES_SUCCESS
  }),

  getUserShiftsSuccess = (payload) => ({
    type: types.GET_USER_SHIFTS_SUCCESS,
    payload
  }),

  getUserShiftsRequestsSuccess = (payload) => ({
    type: types.GET_USER_SHIFTS_REQUEST_SUCCESS,
    payload
  }),

  getShiftsForCalendarSuccess = (values) => ({
    type: types.GET_CALENDAR_SHIFTS_SUCCESS,
    payload: {
      data: values.serverResponse,
      viewBy: values.view
    }
  }),

  getUserCalendarSuccess = (values) => ({
    type: types.GET_USER_SHIFTS_CALENDAR,
    payload: {
      data: values.serverResponse,
      viewBy: values.view
    }
  }),

  getShiftsForDaySuccess = (payload) => ({
    type: types.GET_DAY_SHIFTS_SUCCESS,
    payload
  }),

  setUserCalendarFilters = (filters) => ({
    type: types.SET_USER_CALENDAR_FILTERS,
    filters
  }),

  getShiftTemplateList = () => ( (dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.getShiftTemplates(access_token)
       .then(response => {
         const serverResponse = response.data[0]
          return dispatch(getShiftTemplateListSuccess(serverResponse))
       })
     )}
  ),

  getShiftManagerList = () => ( (dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.fetchShiftManagersList(access_token)
       .then(response => {
         const { data } = response
          return dispatch(getShiftManagerListSuccess(data))
       })
     )}
  ),

  getShiftTypes = () => ( (dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.getShiftTypes(access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
          return dispatch(getShiftTypesSuccess(serverResponse))
       })
     )}
  ),

  getShiftManagerResponses = () => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.getShiftManagerResponses(access_token)
       .then(response => {
         const { data } = response
          return dispatch(getShiftManagerResponsesSuccess(data))
       })
     )}
  ),

  getShiftManagerRequests = () => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.getShiftManagerRequests(access_token)
       .then(response => {
         const { data } = response
          return dispatch(getShiftManagerRequestsSuccess(data))
       })
     )}
  ),

  createShiftType = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user,
      data                = [{
        ...values
      }]
    return (
       api.createShiftType(data, access_token)
       .then(response => {
         dispatch(getShiftTypes())
         callBackSuccess && callBackSuccess()
       })
       .catch(error => {
         callBackFailure && callBackFailure()
       })
     )}
  ),

  getStaffByStaffType = (type) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.getStaffByStaffType(type, access_token)
       .then(response => {
         const { data } = response
          return dispatch(getStaffByStaffTypeSuccess(data))
       })
     )}
  ),

  getDepartmentByBranch = (type) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.getDepartmentByBranch(type, access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
          return dispatch(getDepartmentByBranchSuccess(serverResponse))
       })
     )}
  ),

  createShift = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.createShift(values, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getShiftsOfShiftManager = (callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token}      = state.session.user
    return (
       api.getShiftsOfShiftManager(access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
         if(callBackSuccess) callBackSuccess()
         return dispatch(getShiftsOfShiftManagerSuccess(serverResponse))
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getShiftsDetails = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token, id}      = state.session.user,
      data = {
        branch_id: values.branch_id,
        shift_date: values.shift_date,
        user_id: id
      }

    return (
       api.getShiftDetails(data, access_token)
       .then(response => {
         const { serverResponse } = response.data[0],
          selectedShift = serverResponse.find(record => record.shift_id === Number(values.shift_id || values.id))
         if(callBackSuccess) callBackSuccess(selectedShift)
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  updateFavorite = (value, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.updateFavorite(value, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getTemplateDetails = (value, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.getShiftTemplateDetails(value, access_token)
       .then(response => {
         const { data } = response
         if(callBackSuccess) callBackSuccess(data)
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getFavoriteTemplates = () => (dispatch => dispatch(getFavoriteTemplatesSuccess())),

  removeFromShift = (shiftId, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.removeFromShift(shiftId, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  reAllocateShift = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.reAllocateShift(values, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getUserShifts = (callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.getUserShifts(access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
         dispatch(getUserShiftsSuccess(serverResponse))
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getUserShiftsRequests = (callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.getUserShiftsRequests(access_token)
       .then(response => {
         const { data } = response
         dispatch(getUserShiftsRequestsSuccess(data))
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  normalShiftAccept = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }    = state.session.user,
      acceptUrl = values.level === '2' ? 'approveTradeShiftRequest'
                : values.level === '3' ? 'saveAutoriseTradeShiftRequest'
                : 'normalShiftAccept'

    return (
       api[acceptUrl](values.data, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  normalShiftDecline = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.normalShiftDecline(values, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getShiftsForCalendar = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.getShiftsForCalendar(values, access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
         dispatch(getShiftsForCalendarSuccess({serverResponse, view: values.viewType}))
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getShiftsForDay = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user,
      payload = [{
        ...values
      }]
    return (
       api.getShiftsForDay(payload, access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
         dispatch(getShiftsForDaySuccess(serverResponse))
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getShiftTimeDetails = (shiftId, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user

    return (
       api.getShiftTimeDetails(shiftId, access_token)
       .then(response => {
         const { data } = response
         if(callBackSuccess) callBackSuccess(data)
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getShiftsForUserCalendar = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token, id }      = state.session.user,
      data = {
        ...values,
        user_id : id
      }

    return (
       api.getShiftsForUserCalendar(data, access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
         dispatch(getUserCalendarSuccess({serverResponse, view: values.viewType}))
         if(callBackSuccess) callBackSuccess(serverResponse)
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getCalendarForTrade = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token, id }      = state.session.user,
      data = {
        ...values,
        user_id : id
      }

    return (
       api.getShiftsForUserCalendar(data, access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
         if(callBackSuccess) callBackSuccess(serverResponse)
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getUserDayShifts = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token, id}      = state.session.user,
      data = {
        branch_id: values.branch_id,
        shift_date: values.shift_date,
        user_id: id
      }

    return (
       api.getShiftDetails(data, access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
         dispatch(getShiftsForDaySuccess(serverResponse))
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  getTradeShifts = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token, id}      = state.session.user,
      data = {
        branch_id: values.branch_id,
        shift_date: values.shift_date,
        user_id: id
      }

    return (
       api.getShiftDetails(data, access_token)
       .then(response => {
         const { serverResponse } = response.data[0]
         if(callBackSuccess) callBackSuccess(serverResponse)
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  requestShift = (shift_time_id, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token, id}      = state.session.user,
      data = [{
        shift_time_id,
        user_id: id
      }]

    return (
       api.requestShift(data, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  tradeShift = (values, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      {access_token, id}      = state.session.user,
      data = {
        ...values,
        user_id : id,
      }


    return (
       api.tradeShift(data, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  ),

  cancelShift = (shiftTimeId, callBackSuccess, callBackFailure) => ((dispatch, getState) => {
    const
      state               = getState(),
      { access_token }      = state.session.user,
      data = {
        shiftTimeId,
      }

    return (
       api.cancelShift(data, access_token)
       .then(response => {
         if(callBackSuccess) callBackSuccess()
       })
       .catch(error => {
         if(callBackFailure) callBackFailure()
       })
     )}
  )

export default {
  getShiftTemplateList,
  getShiftManagerList,
  getShiftTypes,
  getShiftManagerResponses,
  getShiftManagerRequests,
  getStaffByStaffType,
  getDepartmentByBranch,
  createShift,
  getShiftsOfShiftManager,
  getShiftsDetails,
  updateFavorite,
  getTemplateDetails,
  getFavoriteTemplates,
  removeFromShift,
  reAllocateShift,
  getUserShifts,
  getUserShiftsRequests,
  normalShiftAccept,
  normalShiftDecline,
  getShiftsForCalendar,
  getShiftsForDay,
  getShiftTimeDetails,
  getShiftsForUserCalendar,
  getUserDayShifts,
  requestShift,
  tradeShift,
  cancelShift,
  createShiftType,
  getCalendarForTrade,
  getTradeShifts,
  setUserCalendarFilters
}
