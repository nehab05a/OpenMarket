import types from './types'
import * as api from '../lib/api/sessionApi'

export const loadLocations = (payload) => ({
  type: types.LOAD_LOCATIONS,
  payload
})

export const fetchLocations = () => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user
  return (
       api.fetchLocations(access_token).then(
         response => {
           const { serverResponse } = response.data[0]
           return dispatch(loadLocations(serverResponse))
         })
     )}
  )

export const updateLocation = (values, callbackSuccess, callbackFailure) => ( (dispatch, getState) => {
  const
    state               = getState(),
    {access_token}      = state.session.user,
    branchPhoto     = state.files.location

  return (
     api.updateLocation(values, access_token)
     .then(response => {
       const
         { branchId } = response.data,
         logoPayload = {
           id: branchId,
           branchPhoto: branchPhoto && branchPhoto[0]
         }
         branchPhoto && api.uploadBranchPhoto(logoPayload, access_token)
         .then(data => console.log(data))
        if(callbackSuccess) callbackSuccess()
     }
   ))
 }
)

export const findAddress = (postalOrZipCode, callbackSuccess, callbackFailure) => (
     api.findAddress(postalOrZipCode)
     .then(response => {
       const
         data = response.data && response.data.results[0],
         {
           geometry: { location : { lat, lng } },
           formatted_address,
           address_components
         } = data,
         findComponent = serachKeys => address_components.find( component => component.types.some(r => serachKeys.includes(r)) ) || null,
         addressLine1 = findComponent(['route', 'locality']),
         addressLine2 = findComponent(['sublocality',]),
         cityOrTown = findComponent(['postal_town','administrative_area_level_1']),
         countyOrState = findComponent(['administrative_area_level_1']),
         postalCode = findComponent(['postal_code']),
         country = findComponent(['country']),
         address = {
           branch_location: formatted_address,
           branch_lati: lat,
           branch_lang: lng,
           addressLine1: addressLine1 && addressLine1.long_name,
           addressLine2: addressLine2 && addressLine2.long_name,
           cityOrTown: cityOrTown && cityOrTown.long_name,
           countyOrState: countyOrState && countyOrState.long_name,
           postalCode: postalCode && postalCode.long_name,
           country: country && country.short_name,
         }
        if(callbackSuccess) callbackSuccess(address);
     }
   )
 )
