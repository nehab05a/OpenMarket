package verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;

public class Server extends AbstractVerticle {

	@Override
	public void start(Future<Void> future) {

		System.out.println("Deploying App Verticle");
		vertx.deployVerticle(new AppVerticle(),
				new DeploymentOptions().setConfig(config()));

		future.complete();
	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception {
		System.out.println("Stopping Server Verticle now...");
		super.stop(stopFuture);
	}
}
