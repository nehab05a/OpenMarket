package verticles;

import java.nio.file.Paths;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

public class AppVerticle extends AbstractVerticle {

	private Future<Void> createHttpServer(Router router, int port) {

		System.out.println("Provisining HTTP Server....");

		Future<HttpServer> httpServerFuture = Future.future();
		vertx.createHttpServer().requestHandler(router::accept)
				.listen(port, httpServerFuture.completer());

		System.out.println("Provisioned HTTP Server on Port " + port);

		return httpServerFuture.map(r -> null);
	}

	@Override
	public void start(Future<Void> future) {
		Router router = Router.router(vertx);

		// Retrieve the app location from the configuration,
		// default to /app.
		String appRootFolder = config().getString("app.root", "/app");

		System.out.println("Serving contents from "
				+ Paths.get(appRootFolder).toString());

		StaticHandler appHandler = StaticHandler.create(appRootFolder);

		appHandler.setDirectoryListing(false);
		Route rootRoute = router.route("/*").handler(appHandler);

		rootRoute
				.failureHandler(frc -> {

					System.out.println("Current Path: "
							+ frc.currentRoute().getPath());
					System.out.println("Request Path: " + frc.request().path());
					System.out.println("Received "
							+ frc.request().absoluteURI()
							+ ", client route found!");
					System.out.println("Rendering from server "
							+ frc.request().absoluteURI());
					if (frc.request().path() != "/") {
						frc.reroute(HttpMethod.GET, "/");
					}
				});

		// Retrieve the port from the configuration,
		// default to 8080.
		Integer appPort = config().getInteger("http.port", 8080);
		createHttpServer(router, appPort).setHandler(future.completer());
	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception {
		System.out.println("Stopping App Verticle now...");
		super.stop(stopFuture);
	}
}
